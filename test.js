// Import everything:
const mongoose = require("mongoose");
const db_config = require('./configs/database');
// Plugins:
const encrypter = require("./plugins/encrypter");
const logs = require("./plugins/log_manager");
const iam = require("./plugins/IAM");
const testModels = require('./configs/test_models');

// Models:
const User = require("./Models/User");
const Project = require("./Models/Project");
const Asset = require("./Models/Asset");
const Information = require("./Models/Information");

// Routes
const user_route = require("./Routes/user-management");
const project_route = require("./Routes/projects");


mongoose.connect(db_config.database);
// On Connection
mongoose.connection.on('connected', () => {
    console.log(('Connected to Database ' + db_config.database).green);
});
// If Connection Failed
mongoose.connection.on('error', (err) => {
    console.log(('Failed during the Connection to the Database : ' + err).red);
    startup_error = true;
});


// test area