import os, time, json, io, base64, random
from datetime import date
from docx import Document
from docx.shared import Pt, Inches, Cm
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import RGBColor
from docx.oxml.ns import nsdecls
from docx.oxml import parse_xml

class Report_Pre_Sales:
    document = ''
    name = ''
    lang = ''
    path = ''
    target = ''
    client = {
        "name": "",
        "company": "",
        "logo": ""
    }
    results = {}
    texts = ''
    email_in_annex = False

    def __init__(self):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Report.py').strip('Report.pyc')
        self.document = Document(ressources_path + '/template.docx')

    ##########
    # Document manipulation functions
    ##########
    def save(self):
        # TODO: Test it :
        # self.set_correct_margin()
        self.document.save(self.path)

    def get_page_content(self):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Report.py')
        if (ressources_path == ''):
            ressources_path = '.'
        # Text loading
        with open(ressources_path + 'Ressources/langs/' + self.lang + '/content.json', encoding='utf-8') as json_file:  
            base_texts = json.load(json_file)
        self.texts = base_texts

    def add_spacing(self, number):
        for i in range(number):
            self.document.add_paragraph()

    def page_break(self):
        self.document.add_page_break()

    def set_correct_margin(self):
        sections = self.document.sections
        for section in sections:
            section.top_margin = Cm(2.5)
            section.bottom_margin = Cm(2.5)
            section.left_margin = Cm(2.5)
            section.right_margin = Cm(2.5)

    ##############
    # Document writing functions
    ##############
    def write_document_title(self):
        self.document.add_paragraph(style="Title").add_run(self.name)
        table = self.document.add_table(rows=1,cols=1)
        table.style = 'Title Underline'
        self.add_spacing(3)

    def write_multiple_texts(self, para_texts, base_style="Normal"):

        def style_run(text, style=base_style):
            if ('++ITA++' in text):
                splitted = text.split('++ITA++')
                para = self.document.add_paragraph(style=style)
                for idx, textchunk in enumerate(splitted):
                    if ((idx-1) % 3 == 0):
                        para.add_run(textchunk).italic = True
                    else:
                        para.add_run(textchunk)
            elif ('++BOLD++' in text):
                splitted = text.split('++BOLD++')
                para = self.document.add_paragraph(style=style)
                for idx, textchunk in enumerate(splitted):
                    if ((idx-1) % 3 == 0):
                        para.add_run(textchunk).bold = True
                    else:
                        para.add_run(textchunk)
            elif ('Spoofing possible for' in text):
                self.document.add_paragraph(style='Redding').add_run(text)
            else:
                self.document.add_paragraph(style=style).add_run(text)

        is_list = False
        is_numbered = False
        for text in para_texts:
            if (text == '++LIST++'):
                is_list = not is_list
            elif (text == '++NLIST++'):
                is_numbered = not is_numbered
            else:
                if (not is_list and not is_numbered):
                    style_run(text)
                elif (is_numbered):
                    style_run(text, 'Number 1')
                else:
                    style_run(text, 'Bullet 2')

    #############
    # Document pages writing functions
    #############

    def write_front_page(self):
        self.write_document_title()
        self.add_spacing(9)
        self.document.add_paragraph(style="Present to").add_run(self.texts["in_attention_of"])
        image_link = self.manage_client_logo(self.client["logo"])
        self.document.add_picture(image_link, height=Cm(8))
        self.remove_picture(image_link)
        self.page_break()

    def write_introduction(self):
        self.document.add_paragraph(style="Heading 1").add_run(self.texts["introduction"])
        self.write_multiple_texts(self.texts["intro_texts"])
        self.page_break()

    def write_observation(self):
        self.document.add_paragraph(style="Heading 1").add_run(self.texts["observation_results"])

        #### Emails Falsification"
        if self.results[self.target]['Spoofing']["Spoofable"]:
            # Spoofable emails
            self.document.add_paragraph(style="Heading 2").add_run(self.texts["emails_spoofable_title"])
        else:
            # No spoofable
            self.document.add_paragraph(style="Heading 2").add_run(self.texts["emails_unspoofable_title"])
        self.write_multiple_texts(self.texts["spoofable_text_1"])
        if self.results[self.target]['Spoofing']["Spoofable"]:
            # Spoofable emails
            self.document.add_paragraph().add_run(self.texts["spoof_case"])
        else:
            # No spoofable
            self.document.add_paragraph().add_run(self.texts["unspoof_case"])
        # Add Spoof table
        table = self.document.add_table(rows=2, cols=3)
        table.style = 'table phishing rrr'
        widths = [5.33,5.33,5.33]
        for row in table.rows:
            for idx, width in enumerate(widths):
                row.cells[idx].width = Cm(width)
        # Format of header rows:
        sec_present = parse_xml(r'<w:shd {} w:fill="#92D050"/>'.format(nsdecls('w')))
        table.rows[0].cells[0].text = self.texts["SPF"]
        table.rows[0].cells[1].text = self.texts["DMARC"]
        table.rows[0].cells[2].text = self.texts["DKIM"]
        if self.results[self.target]['Spoofing']["SPF"]:
            table.rows[0].cells[0]._tc.get_or_add_tcPr().append(sec_present)
        if self.results[self.target]['Spoofing']["DMARC"]:
            table.rows[0].cells[1]._tc.get_or_add_tcPr().append(sec_present)
        table.rows[1].cells[0].text = self.texts["spf_desc"]
        table.rows[1].cells[1].text = self.texts["dmarc_desc"]
        table.rows[1].cells[2].text = self.texts["dkim_desc"]
        # Tool output
        self.add_spacing(1)
        self.document.add_paragraph().add_run(self.texts["tool_output"]).italic = True
        self.write_multiple_texts(self.results[self.target]['Spoofing']["Output"])
        self.page_break()

        #### Leaked e-mails
        if (len(self.results["Mails"]) > 0):
            if (len(self.results["Mails"]) > 20):
                self.email_in_annex = True
                # Emails present in DB
                self.document.add_paragraph(style="Heading 2").add_run(self.texts["emails_present_title"])
                self.write_multiple_texts(self.texts["emails_present_content"])
                table = self.document.add_table(rows=1, cols=2)
                table.style = 'Title Underline'
                widths = [5.33,7.67]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["email"]
                table.rows[0].cells[1].text = self.texts["password"]
                for i in range(20):
                    table.add_row()
                    table.rows[i+1].cells[0].text = self.results["Mails"][i][0]
                    table.rows[i+1].cells[1].text = self.results["Mails"][i][1]
                self.write_multiple_texts(self.texts["emails_present_disclaimer"])
            else:
                # Emails present in DB
                self.document.add_paragraph(style="Heading 2").add_run(self.texts["emails_present_title"])
                self.write_multiple_texts(self.texts["emails_present_content"])
                table = self.document.add_table(rows=1, cols=2)
                table.style = 'Title Underline'
                widths = [5.33,7.67]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["email"]
                table.rows[0].cells[1].text = self.texts["password"]
                for idx, info in enumerate(self.results["Mails"]):
                    table.add_row()
                    table.rows[idx+1].cells[0].text = info[0]
                    table.rows[idx+1].cells[1].text = info[1]
                self.write_multiple_texts(self.texts["emails_present_disclaimer"])
        else:
            # Emails not present in DB
            self.document.add_paragraph(style="Heading 2").add_run(self.texts["emails_unpresent_title"])
            self.write_multiple_texts(self.texts["emails_unpresent_content"])
            self.write_multiple_texts(self.texts["emails_unpresent_disclaimer"])
        self.page_break()

        #### Add DoS
        self.document.add_paragraph(style='Heading 2').add_run(self.texts["dos_title"])
        self.write_multiple_texts(self.texts["dos_content"])
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Report.py')
        self.document.add_picture(ressources_path + 'Ressources/dos_screen.png', width=Cm(15.84))
        self.page_break()

        #### Tech Security
        self.document.add_paragraph(style="Heading 2").add_run(self.texts["tech_security_title"])
        self.write_multiple_texts(self.texts["tech_security_content"])
        if self.detect_cms():
            # CMS Detected:
            self.add_spacing(1)
            self.write_multiple_texts(self.texts["tech_cms_detected"])
        self.add_spacing(1)
        self.write_multiple_texts(self.texts["tech_table_present"])
        self.document.add_paragraph(style="Lettering").add_run(self.texts["security_headers"])
        # Headers table:
        table = self.document.add_table(rows=2, cols=3)
        table.style = 'Table Headers'
        widths = [5.33,5.33,5.33]
        for row in table.rows:
            for idx, width in enumerate(widths):
                row.cells[idx].width = Cm(width)
        table.rows[0].cells[0].text = self.texts["CSP"]
        table.rows[0].cells[1].text = self.texts["XFO"]
        table.rows[0].cells[2].text = self.texts["XSP"]
        table.rows[1].cells[0].text = self.texts["RefP"]
        table.rows[1].cells[1].text = self.texts["XCT"]
        table.rows[1].cells[2].text = self.texts["HSTS"]
        if (('www.' + self.target) in self.results[self.target]):
            headers = self.results[self.target]["www." + self.target]["Headers"]
        else:
            headers = self.results[self.target][self.target]["Headers"]
        if (headers['content-security-policy']):
            table.rows[0].cells[0]._tc.get_or_add_tcPr().append(sec_present)
        if (headers['x-frame-options']):
            table.rows[0].cells[1]._tc.get_or_add_tcPr().append(sec_present)
        if (headers['x-xss-protection']):
            table.rows[0].cells[2]._tc.get_or_add_tcPr().append(sec_present)
        if (headers['referrer-policy']):
            table.rows[1].cells[0]._tc.get_or_add_tcPr().append(sec_present)
        if (headers['x-content-type-options']):
            table.rows[1].cells[1]._tc.get_or_add_tcPr().append(sec_present)
        if (headers['strict-transport-security']):
            table.rows[1].cells[2]._tc.get_or_add_tcPr().append(sec_present)
        self.document.add_paragraph().add_run(self.texts["headers_disclaimer"]).italic = True

        # Tech information:
        search_domain = self.target
        if (('www.' + self.target) in self.results[self.target]):
            search_domain = 'www.' + self.target
        self.document.add_paragraph(style="Lettering").add_run(self.texts["technologies"])
        table = self.document.add_table(rows=1, cols=3)
        table.style = 'Table Style Pricing'
        widths = [4.5,7.3,4.2]
        for row in table.rows:
            for idx, width in enumerate(widths):
                row.cells[idx].width = Cm(width)
        table.rows[0].cells[0].text = self.texts["technologies"]
        table.rows[0].cells[1].text = self.texts["name"]
        table.rows[0].cells[2].text = self.texts["version"]
        row_count = 0
        # Web server
        if (self.results[self.target][search_domain]["Techno"]["Web-Server"]):
            table.add_row()
            row_count += 1
            table.rows[row_count].cells[0].text = self.texts["web_server"]
            table.rows[row_count].cells[1].text = self.results[self.target][search_domain]["Techno"]["Web-Server"]
        # CMS:
        if (self.results[self.target][search_domain]["Techno"]["CMS"]["Exists"]):
            table.add_row()
            row_count += 1
            table.rows[row_count].cells[0].text = self.texts["CMS"]
            table.rows[row_count].cells[1].text = self.results[self.target][search_domain]["Techno"]["CMS"]["Which"]
        # Programming Languages
        if (len(self.results[self.target][search_domain]["Techno"]["Programming-Languages"]) > 0):
            for pl in self.results[self.target][search_domain]["Techno"]["Programming-Languages"]:
                table.add_row()
                row_count += 1
                table.rows[row_count].cells[0].text = self.texts["program_lang"]
                table.rows[row_count].cells[1].text = pl
        # Scripts
        if (len(self.results[self.target][search_domain]["Techno"]["javascript-frameworks"]) > 0):
            for js in self.results[self.target][search_domain]["Techno"]["javascript-frameworks"]:
                table.add_row()
                row_count += 1
                table.rows[row_count].cells[0].text = self.texts["js_script"]
                table.rows[row_count].cells[1].text = js
        # CDN
        if (len(self.results[self.target][search_domain]["Techno"]["CDN"]) > 0):
            for cdn in self.results[self.target][search_domain]["Techno"]["CDN"]:
                table.add_row()
                row_count += 1
                table.rows[row_count].cells[0].text = self.texts["CDN"]
                table.rows[row_count].cells[1].text = cdn
        
        ## To be continued
        self.page_break()

    def write_domain_list(self):
        self.document.add_paragraph(style="Heading 2").add_run(self.texts["domain_list_title"])
        table = self.document.add_table(rows=1, cols=2)
        table.style = 'Table Style Pricing'
        widths = [7.99,7.99]
        for row in table.rows:
            for idx, width in enumerate(widths):
                row.cells[idx].width = Cm(width)
        table.rows[0].cells[0].text = self.texts["domain_sub"]
        table.rows[0].cells[1].text = self.texts["IP"]
        row = 0
        for sub in self.results[self.target]["SubDomains"]:
            table.add_row()
            row += 1
            table.rows[row].cells[0].text = sub[1]
            table.rows[row].cells[1].text = sub[0]

    def write_annexes(self):
        self.page_break()
        self.document.add_paragraph(style="Heading 1").add_run(self.texts["annex"])
        if (self.email_in_annex):
            self.document.add_paragraph(style="Heading 2").add_run(self.texts["annex_email_title"])
            table = self.document.add_table(rows=1, cols=2)
            table.style = 'Title Underline'
            widths = [5.33,7.67]
            for row in table.rows:
                for idx, width in enumerate(widths):
                    row.cells[idx].width = Cm(width)
            table.rows[0].cells[0].text = self.texts["email"]
            table.rows[0].cells[1].text = self.texts["password"]
            for idx, info in enumerate(self.results["Mails"]):
                table.add_row()
                table.rows[idx+1].cells[0].text = info[0]
                table.rows[idx+1].cells[1].text = info[1]
            self.page_break()
        self.document.add_paragraph(style="Heading 2").add_run(self.texts["annex_other_system"])
        # Search all systems except www or main domain
        keys = list(self.results[self.target].keys())
        www_present = False
        if ('www.' + self.target in keys):
            www_present = True
        for key in keys:
            print(key)
            if ((www_present and not ('www.' in key)) or (not www_present and not self.target == key)) and self.target in key:
                self.document.add_paragraph(style="Heading 3").add_run(key.replace('\\u002E', '.'))
                # Manage domain informations:
                ## 1. Headers:
                self.document.add_paragraph().add_run(self.texts["security_headers"]).italic = True
                sec_present = parse_xml(r'<w:shd {} w:fill="#92D050"/>'.format(nsdecls('w')))
                table = self.document.add_table(rows=2, cols=3)
                table.style = 'Table Headers'
                widths = [5.33,5.33,5.33]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["CSP"]
                table.rows[0].cells[1].text = self.texts["XFO"]
                table.rows[0].cells[2].text = self.texts["XSP"]
                table.rows[1].cells[0].text = self.texts["RefP"]
                table.rows[1].cells[1].text = self.texts["XCT"]
                table.rows[1].cells[2].text = self.texts["HSTS"]
                headers = self.results[self.target][key]["Headers"]
                if (headers['content-security-policy']):
                    table.rows[0].cells[0]._tc.get_or_add_tcPr().append(sec_present)
                if (headers['x-frame-options']):
                    table.rows[0].cells[1]._tc.get_or_add_tcPr().append(sec_present)
                if (headers['x-xss-protection']):
                    table.rows[0].cells[2]._tc.get_or_add_tcPr().append(sec_present)
                if (headers['referrer-policy']):
                    table.rows[1].cells[0]._tc.get_or_add_tcPr().append(sec_present)
                if (headers['x-content-type-options']):
                    table.rows[1].cells[1]._tc.get_or_add_tcPr().append(sec_present)
                if (headers['strict-transport-security']):
                    table.rows[1].cells[2]._tc.get_or_add_tcPr().append(sec_present)

                ## 2. Technos:
                self.document.add_paragraph().add_run(self.texts["technologies"]).italic = True
                search_domain = key
                table = self.document.add_table(rows=1, cols=3)
                table.style = 'Table Style Pricing'
                widths = [4.5,7.3,4.2]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["technologies"]
                table.rows[0].cells[1].text = self.texts["name"]
                table.rows[0].cells[2].text = self.texts["version"]
                row_count = 0
                # Web server
                if (self.results[self.target][search_domain]["Techno"]["Web-Server"]):
                    table.add_row()
                    row_count += 1
                    table.rows[row_count].cells[0].text = self.texts["web_server"]
                    table.rows[row_count].cells[1].text = self.results[self.target][search_domain]["Techno"]["Web-Server"]
                # CMS:
                if (self.results[self.target][search_domain]["Techno"]["CMS"]["Exists"]):
                    table.add_row()
                    row_count += 1
                    table.rows[row_count].cells[0].text = self.texts["CMS"]
                    table.rows[row_count].cells[1].text = self.results[self.target][search_domain]["Techno"]["CMS"]["Which"]
                # Programming Languages
                if (len(self.results[self.target][search_domain]["Techno"]["Programming-Languages"]) > 0):
                    for pl in self.results[self.target][search_domain]["Techno"]["Programming-Languages"]:
                        table.add_row()
                        row_count += 1
                        table.rows[row_count].cells[0].text = self.texts["program_lang"]
                        table.rows[row_count].cells[1].text = pl
                # Scripts
                if (len(self.results[self.target][search_domain]["Techno"]["javascript-frameworks"]) > 0):
                    for js in self.results[self.target][search_domain]["Techno"]["javascript-frameworks"]:
                        table.add_row()
                        row_count += 1
                        table.rows[row_count].cells[0].text = self.texts["js_script"]
                        table.rows[row_count].cells[1].text = js
                # CDN
                if (len(self.results[self.target][search_domain]["Techno"]["CDN"]) > 0):
                    for cdn in self.results[self.target][search_domain]["Techno"]["CDN"]:
                        table.add_row()
                        row_count += 1
                        table.rows[row_count].cells[0].text = self.texts["CDN"]
                        table.rows[row_count].cells[1].text = cdn
          

    #########
    # Other functions
    #########
    def manage_client_logo(self, b64_logo):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Report.py')
        b64_logo = b64_logo.split(',')[1]
        imgdata = base64.b64decode(b64_logo)
        image_temp = ''
        for x in range(10):
            image_temp += str(random.randint(0,9))
        filename = ressources_path + '/Ressources/' + image_temp + '.jpg'
        with open(filename, 'wb') as f:
            f.write(imgdata)
        return ressources_path + '/Ressources/' + image_temp + '.jpg'
    def remove_picture(self, file_path):
        os.remove(file_path)

    ##########
    # Gestion des TLD:
    ##########
    def detect_cms(self):
        keys = list(self.results[self.target].keys())
        cms_present = False
        for key in keys:
            try:
                if self.results[self.target][key]['Techno']['CMS']["Exists"]:
                    cms_present = True
            except:
                pass   
        return cms_present
            