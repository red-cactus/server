import os, time, pymongo, sys
from bson.objectid import ObjectId
from Report import Report_Pre_Sales

def retrieve_presale_infos(presale_id):
    print('Result ID: ' + presale_id)
    sys.stdout.flush()
    db_client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = db_client["Report-Helper"]
    db_collection = db["pre-sales-reports"]
    offerect = db_collection.find_one({"_id": ObjectId(presale_id)})
    return offerect

def retrieve_report_result(result_id):
    print('Result ID: ' + result_id)
    sys.stdout.flush()
    db_client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = db_client["Report-Helper"]
    db_collection = db["Presales-Results"]
    offerect = db_collection.find_one({"_id": ObjectId(result_id)})
    return offerect

def create_report(report_document, results_doc):
    r = Report_Pre_Sales()

    r.name = report_document["Name"]
    r.path = report_document["Path"]
    r.target = report_document["Target"].replace('.', '\\u002E').replace('https://', '').replace('http://', '')
    r.results = results_doc
    r.client["logo"] = report_document["Logo"]
    r.lang = report_document["Lang"]

    r.get_page_content()
    r.write_front_page()
    r.write_introduction()
    r.write_observation()
    r.write_domain_list()

    r.write_annexes()

    r.save()
    print('Pre-Sales Written')
    sys.stdout.flush()


if __name__ == '__main__':
    report_document = retrieve_presale_infos(sys.argv[1])
    results_document = retrieve_report_result(report_document["Results_id"])
    create_report(report_document, results_document)