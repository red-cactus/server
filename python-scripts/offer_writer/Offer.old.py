import os, time, json
from datetime import date
from docx import Document
from docx.shared import Pt, Inches, Cm
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import RGBColor

class Offer:
    document = ''
    name = ''
    lang = ''
    offer_type = ''
    path = ''
    content = []
    texts: {
        "base": '',
        "content": ''
    }
    heading_lvl1 = 0
    heading_lvl2 = 0

    def __init__(self):
        self.document = Document('template.docx')

    # Create document management functions:
    def save(self):
        # TODO: Test it :
        # self.set_correct_margin()
        self.document.save(self.path)

    def get_page_content(self):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        if (ressources_path == ''):
            ressources_path = '.'
        # Base text loading
        with open(ressources_path + 'Ressources/langs/' + self.lang + '/base.json', encoding='utf-8') as json_file:  
            base_texts = json.load(json_file)
        # Content text loading
        with open(ressources_path + 'Ressources/langs/' + self.lang + '/content.json', encoding='utf-8') as json_file:  
            content_texts = json.load(json_file)
        self.texts = {"base": base_texts, "content": content_texts}

    def add_spacing(self, number):
        for i in range(number):
            self.document.add_paragraph()

    def page_break(self):
        self.document.add_page_break()

    def set_correct_margin(self):
        sections = self.document.sections
        for section in sections:
            section.top_margin = Cm(2.5)
            section.bottom_margin = Cm(2.5)
            section.left_margin = Cm(2.5)
            section.right_margin = Cm(2.5)

    # Writing page functions:
    def write_first_page(self, reference, client_name, bm_name):
        print('Creating first page...')
        self.document.add_paragraph(style='Doc Title').add_run(self.name)
        self.document.add_paragraph().add_run('')
        self.document.add_paragraph(style='PC Title').add_run(self.texts["base"]["commercial_proposition"])
        self.document.add_paragraph().add_run('')
        document_para = self.document.add_paragraph()
        # Get date:
        today = date.today()
        date_infos = today.strftime("%Y" + '/' + "%m" + "/" + "%d")
        self.document.add_paragraph().add_run(date_infos + ' | ' + str(self.texts["base"]["reference"]) + reference)
        self.document.add_paragraph().add_run('')
        self.document.add_paragraph().add_run('')
        self.document.add_paragraph(style='Pres Title').add_run(self.texts["base"]["presented_to"])
        # TODO: Replace with real client logo
        self.document.add_picture('./Ressources/Base Logo.jpg', width=Inches(1.7))
        document_para = self.document.add_paragraph()
        document_para.add_run(self.texts["base"]["in_the_attention_of"] + client_name + '\r')
        document_para.add_run(self.texts["base"]["commercial_contact"] + bm_name + '')
        # TODO: Test it:
        self.add_spacing(7)
        self.document.add_picture('./Ressources/red_cactus_logo.png', width=Inches(4.2))

    def write_financial_offer(self):
        print('Writing financial offer...')
        # Writing sells conditions
        self.heading_lvl1 += 1
        self.heading_lvl2 = 0
        self.document.add_paragraph(style="heading1").add_run(str(self.heading_lvl1) + self.texts["base"]["financial_offer"])
        self.heading_lvl2 +=1
        self.document.add_paragraph(style="heading2").add_run('\t' + str(self.heading_lvl1) + '.' + str(self.heading_lvl2) + '. ' + self.texts["base"]["sale_condition"])
        for text in self.texts["base"]["sale_condition_content"]:
            self.document.add_paragraph().add_run(text)
        self.add_spacing(1)

        # Sells part:
        self.heading_lvl2 +=1
        self.document.add_paragraph(style="heading2").add_run('\t' + str(self.heading_lvl1) + '.' + str(self.heading_lvl2) + '. ' + self.texts["base"]["offer_description"])
        """
        indent = 0
        for number, content in enumerate(self.content, start=1):
            if (content == 'smart_ciso' or content == 'devsecops_formation' or content == 'awareness_formation'):
                indent = indent + 1
                self.write_content_description(indent, content)
        self.add_spacing(1)
        self.document.add_paragraph(style="heading3").add_run("\t" + "       " + "1.2." + str(indent+1) + " " + self.texts["base"]["delivered_reports"])
        self.document.add_paragraph().add_run()
        self.document.paragraphs[-1].add_run(self.texts["base"]["managerial_report"]).font.underline = True
        self.document.add_paragraph()
        # Manager report
        for info in self.texts["base"]["manager_report_texts"]:
            self.document.paragraphs[-1].add_run('\t - ' + info + '\r')
            # OLD - self.document.add_paragraph().add_run("\t - " + info)
        self.document.add_paragraph().add_run("\r")
        # Tech report
        self.document.paragraphs[-1].add_run(self.texts["base"]["technical_report"]).font.underline = True
        self.document.add_paragraph()
        for info in self.texts["base"]["tech_report_texts"]:
            self.document.paragraphs[-1].add_run('\t - ' + info + '\r')
            # OLD - self.document.add_paragraph().add_run("\t - " + info)
        """
        # Costs description:
        self.page_break()
        self.heading_lvl2 +=1
        self.document.add_paragraph(style="heading2").add_run('\t' + str(self.heading_lvl1) + '.' + str(self.heading_lvl2) + '. ' + self.texts["base"]["costs"])
        self.write_costs()

    def write_content_description(self, number, content):
        texts = self.texts["content"]["description"][content]
        title = '\t' + str(self.heading_lvl1) + '.' + str(self.heading_lvl2) + '.' + str(number) + ' ' + texts["title"]
        self.document.add_paragraph(style="heading3").add_run(title)
        is_list = False
        for text in texts["paragraphs"]:
            if (text == '++LIST++' and not is_list):
                is_list = True
                self.document.add_paragraph()
            elif (text == '++LIST++'):
                is_list = False
                self.document.paragraphs[-1].paragraph_format.tab_stops
            else:
                if (not is_list):
                    self.document.add_paragraph().add_run("\t" + text)
                else:
                    self.document.paragraphs[-1].add_run("- " + text + "\r")
                    # OLD self.document.add_paragraph(style="list1").add_run("\t     - " + text)

    def write_costs(self):
        self.document.add_paragraph().add_run(self.texts["base"]["different_costs"])
        # Create undertitle:
        self.document.add_paragraph(style="heading4").add_run(self.texts["base"]["prestations"])

        for content in self.content:
            table = self.document.add_table(rows=(len(self.texts["content"]["pricing"][content]["table"])+3), cols=4)
            table.style = 'Plain Table 4'
            # Setup cell size:
            widths = [8.88,1.81,1.97,3.93]
            for row in table.rows:
                for idx, width in enumerate(widths):
                    row.cells[idx].width = Cm(width)
            # Setup table base text
            table.rows[0].cells[0].text = self.texts["content"]["pricing"][content]["title"]
            table.rows[0].cells[1].text = self.texts["base"]["consutlants_nbre"]
            table.rows[0].cells[2].text = self.texts["base"]["hours"]
            table.rows[0].cells[3].text = self.texts["base"]["eur_ammount"]
            table.rows[len(table.rows)-1].cells[0].text = self.texts["base"]["total"]
            # Enter all information:
            for idx, information in enumerate(self.texts["content"]["pricing"][content]["table"]):
                table.rows[idx+1].cells[0].paragraphs[0].add_run(information).bold = False
                table.rows[idx+1].cells[0].paragraphs[0].style = 'Normal'
            self.document.add_paragraph().add_run('\r\r')

    def write_validity(self, company_name):
        self.heading_lvl2 += 1
        self.document.add_paragraph(style="heading2").add_run('\t' + str(self.heading_lvl1) + '.' + str(self.heading_lvl2) + '. ' + self.texts["base"]["validity"])
        self.document.add_paragraph().add_run(self.texts["base"]["offer_validity"])
        self.document.add_paragraph().add_run(self.texts["base"]["acceptation"])
        self.add_spacing(1)
        table = self.document.add_table(rows=5, cols=2)
        table.rows[0].cells[0].text = "REDCACTUS"
        table.rows[0].cells[1].text = company_name
        table.rows[1].cells[0].text = self.texts["base"]["place_date"]
        table.rows[1].cells[1].text = self.texts["base"]["place_date"]
        table.rows[2].cells[0].text = self.texts["base"]["name"]
        table.rows[2].cells[1].text = self.texts["base"]["name"]
        table.rows[3].cells[0].text = self.texts["base"]["position"]
        table.rows[3].cells[1].text = self.texts["base"]["position"]
        table.rows[4].cells[0].text = self.texts["base"]["sign_stamp"]
        table.rows[4].cells[1].text = self.texts["base"]["sign_stamp"]

    def write_last_page(self, bm_email):
        self.add_spacing(9)
        self.document.add_picture('./Ressources/red_cactus_logo2.png', width=Inches(1.5))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.add_spacing(7)
        self.document.add_paragraph(style="redc").add_run('REDCACTUS')
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        address = self.document.add_paragraph()
        for text in self.texts["base"]["address"]:
            address.add_run(text + '\r')
        address.add_run(self.texts["base"]["contact"] + bm_email)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER

    def write_group_presentation():
        # Red cactus & document informations
        self.heading_lvl1 += 1
        self.heading_lvl2 = 0
        self.document.add_paragraph(style="heading1").add_run(str(self.heading_lvl1) + self.texts["base"]["financial_offer"])
 

    # Add styling functions:
    def style(self):
        styles = self.document.styles
        #####
        # Style of front page:
        #####
        # Document Heading style
        doc_title_style = styles.add_style('Doc Title', WD_STYLE_TYPE.PARAGRAPH)
        doc_title_style.font.name = 'Montserrat'
        doc_title_style.font.size = Pt(26)
        # Presented to style
        PC_title_style = styles.add_style('PC Title', WD_STYLE_TYPE.PARAGRAPH)
        PC_title_style.font.name = 'Montserrat'
        PC_title_style.font.size = Pt(24)
        PC_title_style.font.color.rgb = RGBColor(244,23,56)
        # Presentation title style
        Presentation_title_style = styles.add_style('Pres Title', WD_STYLE_TYPE.PARAGRAPH)
        Presentation_title_style.font.name = 'Montserrat'
        Presentation_title_style.font.size = Pt(18)
        #####
        # Base style all document:
        #####
        # Normal Paragraph style
        normal_style = self.document.styles['Normal']
        normal_style.font.name = 'Source Sans Pro'
        normal_style.font.size = Pt(10)
        # List style
        list_style = styles.add_style('list1', WD_STYLE_TYPE.PARAGRAPH)
        list_style.font.name = 'Source Sans Pro'
        list_style.font.size = Pt(10)
        list_style.paragraph_format.space_before = Pt(2)
        list_style.paragraph_format.line_spacing = Pt(13)
        # Red style
        redc_style = styles.add_style('redc', WD_STYLE_TYPE.PARAGRAPH)
        redc_style.font.name = 'Source Sans Pro'
        redc_style.font.size = Pt(12)
        redc_style.font.color.rgb = RGBColor(244,23,56)
        redc_style.font.bold = True

        # Heading 1 style
        heading1_style = styles.add_style('heading1', WD_STYLE_TYPE.PARAGRAPH)
        heading1_style.font.name = 'Montserrat'
        heading1_style.font.size = Pt(18)
        heading1_style.font.color.rgb = RGBColor(244,23,56)
        # Heading 2 style
        heading2_style = styles.add_style('heading2', WD_STYLE_TYPE.PARAGRAPH)
        heading2_style.font.name = 'Montserrat'
        heading2_style.font.size = Pt(14)
        heading2_style.font.color.rgb = RGBColor(146,25,24)
        # Heading 3 style
        heading3_style = styles.add_style('heading3', WD_STYLE_TYPE.PARAGRAPH)
        heading3_style.font.name = 'Montserrat'
        heading3_style.font.size = Pt(12)
        # Heading 4 style
        heading4_style = styles.add_style('heading4', WD_STYLE_TYPE.PARAGRAPH)
        heading4_style.font.name = 'Source Sans Pro'
        heading4_style.font.size = Pt(10)
        heading4_style.font.bold = True
        heading4_style.font.color.rgb = RGBColor(59,56,56)

        # End table style
        endTable_style = styles.add_style('endTable', WD_STYLE_TYPE.PARAGRAPH)
        endTable_style.font.color.rgb = RGBColor(146,25,24)
