import os, time, json, io, base64, random
from datetime import date
from docx import Document
from docx.shared import Pt, Inches, Cm
from docx.enum.style import WD_STYLE_TYPE
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.shared import RGBColor

class Offer:
    document = ''
    name = ''
    lang = ''
    offer_type = ''
    path = ''
    client = {
        "name": "",
        "company": "",
        "logo": ""
    }
    content = []
    texts = {
        "base": '',
        "content": ''
    }

    def __init__(self):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        self.document = Document(ressources_path + '/template.docx')

    ##########
    # Document manipulation functions
    ##########
    def save(self):
        # TODO: Test it :
        # self.set_correct_margin()
        self.document.save(self.path)

    def get_page_content(self):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        if (ressources_path == ''):
            ressources_path = '.'
        # Base text loading
        with open(ressources_path + 'Ressources/langs/' + self.lang + '/base.json', encoding='utf-8') as json_file:  
            base_texts = json.load(json_file)
        # Content text loading
        with open(ressources_path + 'Ressources/langs/' + self.lang + '/content.json', encoding='utf-8') as json_file:  
            content_texts = json.load(json_file)
        self.texts = {"base": base_texts, "content": content_texts}

    def add_spacing(self, number):
        for i in range(number):
            self.document.add_paragraph()

    def page_break(self):
        self.document.add_page_break()

    def set_correct_margin(self):
        sections = self.document.sections
        for section in sections:
            section.top_margin = Cm(2.5)
            section.bottom_margin = Cm(2.5)
            section.left_margin = Cm(2.5)
            section.right_margin = Cm(2.5)

    ##############
    # Document writing functions
    ##############
    def write_document_title(self):
        self.document.add_paragraph(style="Title").add_run(self.name)
        table = self.document.add_table(rows=1,cols=1)
        table.style = 'Title Underline'
        self.add_spacing(3)

    def write_title_1(self, title):
        self.document.add_paragraph(style="Heading 1").add_run("Banana !")

    def write_multiple_texts(self, para_texts, base_style="Normal"):

        def style_run(text, style=base_style):
            if ('++ITA++' in text):
                splitted = text.split('++ITA++')
                para = self.document.add_paragraph(style=style)
                for idx, textchunk in enumerate(splitted):
                    if ((idx-1) % 3 == 0):
                        para.add_run(textchunk).italic = True
                    else:
                        para.add_run(textchunk)
            elif ('++BOLD++' in text):
                splitted = text.split('++BOLD++')
                para = self.document.add_paragraph(style=style)
                for idx, textchunk in enumerate(splitted):
                    if ((idx-1) % 3 == 0):
                        para.add_run(textchunk).bold = True
                    else:
                        para.add_run(textchunk)
            else:
                self.document.add_paragraph(style=style).add_run(text)

        is_list = False
        is_numbered = False
        for text in para_texts:
            if (text == '++LIST++'):
                is_list = not is_list
            elif (text == '++NLIST++'):
                is_numbered = not is_numbered
            else:
                if (not is_list and not is_numbered):
                    style_run(text)
                elif (is_numbered):
                    style_run(text, 'Number 1')
                else:
                    style_run(text, 'Bullet 2')

    def select_content(self, part):

        def sort_content():
            audit = []
            human_audit = []
            training = []
            placement = []

            for content in self.content:
                if (content in self.texts["content"]["_utils"]["online"] or content in self.texts["content"]["_utils"]["local"]):
                    audit.append(content)
                elif (content in self.texts["content"]["_utils"]["human"]):
                    human_audit.append(content)
                elif (content in self.texts["content"]["_utils"]["training"]):
                    training.append(content)
                elif (content in self.texts["content"]["_utils"]["placement"]):
                    placement.append(content)

            return audit, human_audit, training, placement

        audit, human, train, placement = sort_content()
        returning_array = []

        if (part == 'introduction'):
            # Base text:
            # Add all audit texts
            if (len(audit) > 1):
                returning_array.append(self.texts["content"]["_texts"]["audits_description"])
                new_array = ['++LIST++']
            for audit_info in audit:
                if (len(audit) > 1):
                    new_array.append(self.texts["content"]["introduction"]["simple"][audit_info])
                else:
                    returning_array.append(self.texts["content"]["introduction"]["simple"][audit_info])
            if (len(audit) > 1):
                new_array.append('++LIST++')
                returning_array.append(new_array)
            
            if ((len(audit) > 0) and (len(placement) > 0)):
                # Add placement texts
                returning_array.append(self.texts["content"]["introduction"]["multiple"]["audit_response"])

            # Human offer management
            # If Full human audit + Awareness training
            if ((len(human) > 0 or len(train) > 0) and len(audit) > 0):
                returning_array.append(self.texts["content"]["_texts"]["audit_to_human"])

            if (len(human) > 1 and 'awareness_training' in train):
                returning_array.append(self.texts["content"]["introduction"]["multiple"]["full_human_train"])
            elif (len(human) > 0 and 'awareness_training' not in train):
                # Full human audit without training
                returning_array.append(self.texts["content"]["introduction"]["multiple"]["full_human_no_train"])
            elif (len(human) == 1 or 'awareness_training' in train):
                if (len(human) == 1):
                    returning_array.append(self.texts["content"]["introduction"]["simple"][train[0]])
                if ('awareness_training' in train):
                    returning_array.append(self.texts["content"]["introduction"]["simple"]['awareness_training'])

            if ('devsecops_training' in train):
                # Add dev sec ops training sessions
                returning_array.append(' ')
                returning_array.append(self.texts["content"]["introduction"]["simple"]['devsecops_training'])

            # If no audit but in-house:
            if (len(audit) == 0 and len(placement) > 0):
                returning_array.append(self.texts["content"]["introduction"]["simple"]['in_house_consultant'])

        elif (part == 'content'):
            # Detect online audit
            for aud in audit:
                if aud in self.texts["content"]["_utils"]["online"]:
                    self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["online_audit"])
                    if ("server_audit" in audit):
                        self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["server_audit"][0])
                        text = self.texts["content"]["offer_content"]["server_audit"]
                        text.pop(0)
                        self.write_multiple_texts(text)
                        self.document.add_paragraph()
                    if ("website_audit" in audit):
                        self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["website_audit"][0])
                        text = self.texts["content"]["offer_content"]["website_audit"]
                        text.pop(0)
                        self.write_multiple_texts(text)
                        self.document.add_paragraph()
                    if ("webapp_audit" in audit):
                        self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["webapp_audit"][0])
                        text = self.texts["content"]["offer_content"]["webapp_audit"]
                        text.pop(0)
                        self.write_multiple_texts(text)
                        self.document.add_paragraph()
                    if ("app_audit" in audit):
                        self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["app_audit"][0])
                        text = self.texts["content"]["offer_content"]["app_audit"]
                        text.pop(0)
                        self.write_multiple_texts(text)
                        self.document.add_paragraph()
                    break
            # Detect offline audit
            for aud in audit:
                if aud in self.texts["content"]["_utils"]["local"]:
                    self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["offline_audit"])
                    if ("local_net_audit" in audit):
                        self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["local_net_audit"][0])
                        text = self.texts["content"]["offer_content"]["local_net_audit"]
                        text.pop(0)
                        self.write_multiple_texts(text)
                        self.document.add_paragraph()
                    if ("local_infra_audit" in audit):
                        self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["local_infra_audit"][0])
                        text = self.texts["content"]["offer_content"]["local_infra_audit"]
                        text.pop(0)
                        self.write_multiple_texts(text)
                        self.document.add_paragraph()
                    break

            # Detect human audit:
            if (len(human) > 0):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["human_audit"])
                if ("phishing_campaign" in human):
                    self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["phishing_campaign"][0])
                    text = self.texts["content"]["offer_content"]["phishing_campaign"]
                    text.pop(0)
                    self.write_multiple_texts(text)
                    self.document.add_paragraph()
                if ("soc_eng_audit" in human):
                    self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["soc_eng_audit"][0])
                    text = self.texts["content"]["offer_content"]["soc_eng_audit"]
                    text.pop(0)
                    self.write_multiple_texts(text)
                    self.document.add_paragraph()

            # Detect Train:
            if (len(train) > 0):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["trainings"])
                if ("awareness_training" in train):
                    self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["awareness_training"][0])
                    text = self.texts["content"]["offer_content"]["awareness_training"]
                    text.pop(0)
                    self.write_multiple_texts(text)
                    self.document.add_paragraph()
                if ("devsecops_training" in train):
                    self.document.add_paragraph(style="heading 4").add_run(self.texts["content"]["offer_content"]["devsecops_training"][0])
                    text = self.texts["content"]["offer_content"]["devsecops_training"]
                    text.pop(0)
                    self.write_multiple_texts(text)
                    self.document.add_paragraph()

            # Detect In-House:
            if (len(placement) > 0):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["consultants"])
                text = self.texts["content"]["offer_content"]["in_house_consultant"]
                text.pop(0)
                self.write_multiple_texts(text)


        if (part == 'financial'):
            # Online Audit Table
            if ('server_audit' in audit or 'website_audit' in audit or 'webapp_audit' in audit or 'app_audit' in audit):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["online_audit"])
                table = self.document.add_table(rows=1, cols=5)
                table.style = 'Table Style Pricing'
                widths = [11,1,2,1,1]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["base"]["description"]
                table.rows[0].cells[1].text = self.texts["base"]["hours"]
                table.rows[0].cells[2].text = self.texts["base"]["consutlants"]
                table.rows[0].cells[3].text = self.texts["base"]["hour_rate"]
                table.rows[0].cells[4].text = self.texts["base"]["eur_ammount"]
                if ('server_audit' in audit):
                    table.add_row()
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["server_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["server_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                if ('website_audit' in audit):
                    table.add_row()
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["website_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["website_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                if ('webapp_audit' in audit):
                    table.add_row()
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["webapp_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["webapp_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                if ('app_audit' in audit):
                    table.add_row()
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["app_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["app_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                table.add_row()
                table.add_row()
                table.rows[-1].cells[0].text = self.texts["base"]["total"]
                self.document.add_paragraph('\r')

            if ('local_net_audit' in audit or 'local_infra_audit' in audit):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["offline_audit"])
                table = self.document.add_table(rows=1, cols=5)
                table.style = 'Table Style Pricing'
                widths = [6.25,2.21,2.54,2.03,2.97]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["base"]["description"]
                table.rows[0].cells[1].text = self.texts["base"]["hours"]
                table.rows[0].cells[2].text = self.texts["base"]["consutlants"]
                table.rows[0].cells[3].text = self.texts["base"]["hour_rate"]
                table.rows[0].cells[4].text = self.texts["base"]["eur_ammount"]
                table.add_row()
                if 'local_net_audit' in audit:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["local_net_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["local_net_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                if 'local_infra_audit' in audit:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["local_infra_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["local_infra_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                table.add_row()
                table.rows[-1].cells[0].text = self.texts["base"]["total"]
                self.document.add_paragraph('\r')

            if ('phishing_campaign' in human or 'soc_eng_audit' in human):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["human_audit"])
                table = self.document.add_table(rows=1, cols=5)
                table.style = 'Table Style Pricing'
                widths = [6.25,2.21,2.54,2.03,2.97]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["base"]["description"]
                table.rows[0].cells[1].text = self.texts["base"]["hours"]
                table.rows[0].cells[2].text = self.texts["base"]["consutlants"]
                table.rows[0].cells[3].text = self.texts["base"]["hour_rate"]
                table.rows[0].cells[4].text = self.texts["base"]["eur_ammount"]
                table.add_row()
                if 'phishing_campaign' in human:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["phishing_campaign"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["phishing_campaign"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                if 'soc_eng_audit' in human:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["soc_eng_audit"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["soc_eng_audit"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                table.add_row()
                table.rows[-1].cells[0].text = self.texts["base"]["total"]
                self.document.add_paragraph('\r')

            if ('awareness_training' in train or 'devsecops_training' in train):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["trainings"])
                table = self.document.add_table(rows=1, cols=5)
                table.style = 'Table Style Pricing'
                widths = [6.25,2.21,2.54,2.03,2.97]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["base"]["description"]
                table.rows[0].cells[1].text = self.texts["base"]["hours"]
                table.rows[0].cells[2].text = self.texts["base"]["consutlants"]
                table.rows[0].cells[3].text = self.texts["base"]["hour_rate"]
                table.rows[0].cells[4].text = self.texts["base"]["eur_ammount"]
                table.add_row()
                if 'awareness_training' in train:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["awareness_training"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["awareness_training"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                if 'devsecops_training' in train:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["devsecops_training"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["devsecops_training"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                table.add_row()
                table.rows[-1].cells[0].text = self.texts["base"]["total"]
                self.document.add_paragraph('\r')

            if ('in_house_consultant' in placement):
                self.document.add_paragraph(style="heading 3").add_run(self.texts["content"]["offer_content"]["consultants"])
                table = self.document.add_table(rows=1, cols=5)
                table.style = 'Table Style Pricing'
                widths = [11,2,2,2,2]
                for row in table.rows:
                    for idx, width in enumerate(widths):
                        row.cells[idx].width = Cm(width)
                table.rows[0].cells[0].text = self.texts["base"]["description"]
                table.rows[0].cells[1].text = self.texts["base"]["hours"]
                table.rows[0].cells[2].text = self.texts["base"]["consutlants"]
                table.rows[0].cells[3].text = self.texts["base"]["hour_rate"]
                table.rows[0].cells[4].text = self.texts["base"]["eur_ammount"]
                table.add_row()
                if 'in_house_consultant' in placement:
                    table.add_row()
                    table.rows[-1].cells[0].text = self.texts["content"]["offer_points"]["in_house_consultant"]["title"]
                    table.rows[-1].cells[0].paragraphs[0].style = 'Table Italic'
                    for info in self.texts["content"]["offer_points"]["in_house_consultant"]["content"]:
                        table.add_row()
                        table.rows[-1].cells[0].text = info
                table.add_row()
                table.rows[-1].cells[0].text = self.texts["base"]["total"]
                self.document.add_paragraph('\r')
            self.document.add_paragraph().add_run(self.texts["base"]["final_total"]).bold = True

        return returning_array


    ############
    # Page writers
    ############
    def write_front_page(self):
        self.write_document_title()
        # Commercial Proposition
        self.document.add_paragraph(style="CM Proposition").add_run(self.texts["base"]["commercial_proposition"])
        # Date + References
        today = date.today()
        date_infos = today.strftime("%Y" + '/' + "%m" + "/" + "%d")
        self.document.add_paragraph().add_run(date_infos)
        self.add_spacing(5)
        # Presented to
        self.document.add_paragraph(style="Present to").add_run(self.texts["base"]["presented_to"])
        # Client Logo
        image_link = self.manage_client_logo(self.client["logo"])
        self.document.add_picture(image_link, height=Cm(5))
        # In the attention + Client name
        self.document.add_paragraph().add_run(self.texts["base"]["in_the_attention_of"] + ' ' + self.client["name"])
        self.add_spacing(5)
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        self.document.add_picture(ressources_path + '/Ressources/ptc_logo.png', width=Inches(3))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.remove_picture(image_link)

    def write_page_2(self, BM_name, BM_phone, BM_email, BM_position):
        self.document.add_paragraph(style="Contact Point").add_run(self.texts["base"]["contact_points"])
        table = self.document.add_table(rows=1,cols=1)
        table.rows[0].cells[0].text = BM_name + '\r' + BM_position + '\r' + BM_phone + ' - ' + BM_email
        self.page_break()
        self.document.add_paragraph('\r')
        self.page_break()

    def write_group_description(self):
        # 1. General Presentation
        self.document.add_paragraph(style="Heading 1").add_run(self.texts["base"]["general_presentation"])
        self.document.add_paragraph(style="Heading 2").add_run(self.texts["base"]["why_ptc"])
        self.document.add_paragraph(style="Heading 3").add_run(self.texts["base"]["collab_perf"])
        self.write_multiple_texts(self.texts["base"]["collab_perf_text"])
        self.document.add_paragraph('\r')
        self.document.add_paragraph(style="Heading 2").add_run(self.texts["base"]["internationnal_group"])
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        self.document.add_picture(ressources_path + '/Ressources/ptc_world.png', width=Cm(17))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.page_break()
        self.write_multiple_texts(self.texts["base"]["internationnal_group_texts"])
        self.document.add_paragraph('\r')
        self.document.add_paragraph(style="Heading 3").add_run(self.texts["base"]["field_expertise"])
        self.document.add_paragraph().add_run(self.texts["base"]["field_expertise_1"])
        self.document.add_picture(ressources_path + '/Ressources/expertise_field.png', width=Cm(17))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_paragraph().add_run(self.texts["base"]["field_expertise_2"])
        self.page_break()
        self.document.add_paragraph(style="Heading 2").add_run(self.texts["base"]["redcactus_partner"])
        self.write_multiple_texts(self.texts["base"]["redcactus_partner_texts"])
        self.document.add_paragraph('\r')
        self.document.add_picture(ressources_path + '/Ressources/expertise_sector.png', width=Cm(16))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.page_break()
        self.document.add_paragraph(style="Heading 3").add_run(self.texts["base"]["offers_approach"])
        self.document.add_picture(ressources_path + '/Ressources/solution_ciso.png', width=Cm(18.68))
        self.document.paragraphs[-1].paragraph_format.left_indent = Pt(-40)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_picture(ressources_path + '/Ressources/smart_ciso.png', width=Cm(18.68))
        self.document.paragraphs[-1].paragraph_format.left_indent = Pt(-40)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_picture(ressources_path + '/Ressources/consulting_approach.png', width=Cm(18.68))
        self.document.paragraphs[-1].paragraph_format.left_indent = Pt(-40)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.page_break()
        self.document.add_paragraph(style="Heading 3").add_run(self.texts["base"]["our_expertise"])
        self.document.add_paragraph('\r')
        self.document.add_picture(ressources_path + '/Ressources/pillars_expertise.png', width=Cm(18.68))
        self.document.paragraphs[-1].paragraph_format.left_indent = Pt(-40)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_paragraph('\r')
        self.write_multiple_texts(self.texts["base"]["our_expertise_texts"])
        self.page_break()
        self.document.add_paragraph(style="Heading 3").add_run(self.texts["base"]["added_val"])
        self.write_multiple_texts(self.texts["base"]["added_val_1"])
        self.document.add_paragraph('\r')
        self.document.add_paragraph(style='Heading 3').add_run(self.texts["base"]["our_partnership"])
        self.document.add_paragraph('\r')
        self.document.add_picture(ressources_path + '/Ressources/partners1.png', width=Cm(18.68))
        self.document.paragraphs[-1].paragraph_format.left_indent = Pt(-40)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_paragraph('\r')
        self.document.add_picture(ressources_path + '/Ressources/partners2.png', width=Cm(18.68))
        self.document.paragraphs[-1].paragraph_format.left_indent = Pt(-40)
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.page_break()

    def write_offer_content(self):
        self.document.add_paragraph(style='Heading 1').add_run(self.texts["base"]["offer_for"] + self.client["company"])
        self.document.add_paragraph('\r')
        # Introduction management
        self.document.add_paragraph(style='Heading 2').add_run(self.texts["base"]["introduction_scope"])
        self.document.add_paragraph().add_run(self.client["company"] + self.texts["base"]["introduction_wants"])
        introduction = self.select_content('introduction')
        for text in introduction:
            if (isinstance(text, str)):
                self.document.add_paragraph().add_run(text)
            else:
                self.write_multiple_texts(text)
        self.document.add_paragraph('\r')
        self.document.add_paragraph(style='Heading 2').add_run(self.texts["base"]["our_offer"])
        # Offer content management
        self.select_content('content')
        # Numbers offer
        self.document.add_paragraph(style='Heading 2').add_run(self.texts["base"]["financial_proposition"])
        self.select_content('financial')
        self.page_break()
        # Prerequisites
        self.document.add_paragraph(style='Heading 1').add_run(self.texts["base"]["prequisites_title"])
        self.document.add_paragraph().add_run(self.texts["base"]["prerequisites"]).bold = True
        for prerequisite in self.texts["base"]["prerequisites_content"]["prerequisites"]:
            self.document.add_paragraph().add_run(prerequisite)
        self.document.add_paragraph().add_run(self.texts["base"]["needs"]).bold = True
        for need in self.texts["base"]["prerequisites_content"]["needs"]:
            self.document.add_paragraph().add_run(need)
        self.document.add_paragraph().add_run(self.texts["base"]["restrictions"]).bold = True
        for restriction in self.texts["base"]["prerequisites_content"]["restrictions"]:
            self.document.add_paragraph().add_run(restriction)
        self.page_break()
    
    def write_offer_agreement(self):
        self.document.add_paragraph(style="Heading 1").add_run(self.texts["base"]["agreement_offer"])
        today = date.today()
        date_infos = today.strftime("%d" + '/' + "%m" + "/" + "%Y")
        self.document.add_paragraph().add_run(self.texts["base"]["offer_validity"] + date_infos + '.')
        self.document.add_paragraph().add_run(self.texts["base"]["acceptation"])
        self.document.add_paragraph('\r')
        self.document.add_paragraph(style="Redding").add_run(self.client["company"]).bold = True
        self.document.add_paragraph().add_run(self.texts["base"]["place_date"])
        self.document.add_paragraph().add_run(self.texts["base"]["name"])
        self.document.add_paragraph().add_run(self.texts["base"]["position"])
        self.document.add_paragraph().add_run(self.texts["base"]["sign_stamp"])
        self.page_break()

    def write_close_page(self):
        self.document.add_paragraph('\r\r\r\r\r')
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        self.document.add_picture(ressources_path + '/Ressources/red_cactus_logo2.png', width=Cm(4.45))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_paragraph('\r\r\r')
        self.document.add_paragraph(style="Redding").add_run('REDCACTUS')
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_paragraph().add_run(self.texts["base"]["rc_informations"])
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER
        self.document.add_paragraph('\r\r\r\r\r\r\r')
        self.document.add_picture(ressources_path + '/Ressources/ptc_logo.png', width=Cm(7.49))
        self.document.paragraphs[-1].alignment = WD_ALIGN_PARAGRAPH.CENTER

    ################
    # Styling of document
    ################

    def style(self):
        styles = self.document.styles
        
    # TODO: Remove entire code below
    ################
    # Test functions
    ################
    def print_styles(self):
        styles = self.document.styles
        for style in styles:
            print(style.name)

    #########
    # Other functions
    #########
    def manage_client_logo(self, b64_logo):
        ressources_path = os.path.realpath(__file__)
        ressources_path = ressources_path.strip('Offer.py')
        b64_logo = b64_logo.split(',')[1]
        imgdata = base64.b64decode(b64_logo)
        image_temp = ''
        for x in range(10):
            image_temp += str(random.randint(0,9))
        filename = ressources_path + '/Ressources/' + image_temp + '.jpg'
        with open(filename, 'wb') as f:
            f.write(imgdata)
        return ressources_path + '/Ressources/' + image_temp + '.jpg'
    def remove_picture(self, file_path):
        os.remove(file_path)