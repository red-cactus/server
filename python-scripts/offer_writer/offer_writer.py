import os, time, pymongo, sys
from bson.objectid import ObjectId
from Offer import Offer


def retrieve_offer_infos(offer_id):
    db_client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = db_client["Report-Helper"]
    db_collection = db["offers"]
    offerect = db_collection.find_one({"_id": ObjectId(offer_id)})
    return offerect

def retrieve_bm_infos(bm_id):
    db_client = pymongo.MongoClient("mongodb://localhost:27017/")
    db = db_client["Report-Helper"]
    db_collection = db["users"]
    user_return = db_collection.find_one({"_id": ObjectId(bm_id)})
    return user_return

def short_offer(offer_document, bm_document):
    offer = Offer()
    offer.name = offer_document["Name"]
    offer.lang = offer_document["Lang"]
    offer.path = offer_document["Path"]
    offer.content = offer_document["Content"]
    offer.get_page_content()

    offer.client["logo"] = offer_document["Client_logo"]
    offer.client["name"] = offer_document["Client_name"]
    offer.client["company"] = offer_document["Client_company"]
    offer.write_front_page()
    offer.write_page_2(bm_document["Name"], bm_document["Phone"], bm_document["Email"], bm_document["Post"])
    offer.write_offer_content()
    offer.write_offer_agreement()
    offer.write_close_page()
    offer.set_correct_margin()
    offer.save()
    print('Ending Writing')

def long_offer(offer_document, bm_document):
    offer = Offer()
    offer.name = offer_document["Name"]
    offer.lang = offer_document["Lang"]
    offer.path = offer_document["Path"]
    offer.content = offer_document["Content"]
    offer.get_page_content()

    offer.client["logo"] = offer_document["Client_logo"]
    offer.client["name"] = offer_document["Client_name"]
    offer.client["company"] = offer_document["Client_company"]
    offer.write_front_page()
    offer.write_page_2(bm_document["Name"], bm_document["Phone"], bm_document["Email"], bm_document["Post"])
    offer.write_group_description()
    offer.write_offer_content()
    offer.write_offer_agreement()
    offer.write_close_page()
    offer.set_correct_margin()
    offer.save()
    print('Ending Writing')


if __name__ == '__main__':
    offer_id = sys.argv[1]
    bm_id = sys.argv[2]

    offer = retrieve_offer_infos(offer_id)
    user = retrieve_bm_infos(bm_id)

    if (offer["Offer_type"] == 1):
        long_offer(offer, user)
    else:
        short_offer(offer, user)

    sys.stdout.flush()