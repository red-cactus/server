const mongoose = require("mongoose");
const db_config = require("../configs/database");
const encrypter = require("../plugins/encrypter");
const logs = require('../plugins/log_manager');

// Models
const User = require("../Models/User");
const Project = require("../Models/Project");
const Asset = require("../Models/Asset");
const Information = require("../Models/Information");

/*
    * Return the model of a user that can be used.
*/
module.exports.get_new_user = function(callback) {
    const user_key = generatedRandomKey()
    let new_user = new User({
        Username: 'test-username-' + user_key,
        Password: 'test'  + user_key,
        Email: 'test-email-' + user_key,
        Name: 'test-name-' + user_key,
        Phone: 'test-phone-' + user_key,
        Post: 'test-post-' + user_key
    });
    callback(new_user)
}
/*
    * Return the saved document of a User.
*/
module.exports.create_test_user = function(callback) {
    const user_key = generatedRandomKey();
    let new_user = new User({
        Username: 'test-username-' + user_key,
        Password: 'test'  + user_key,
        Email: 'test-email-' + user_key,
        Name: 'test-name-' + user_key,
        Phone: 'test-phone-' + user_key,
        Post: 'test-post-' + user_key
    });

    User.addUser(new_user, (err, saved) => {
        if (err) {
            logs.server.error('test_models > create_test_user > User.assUser', [String(JSON.stringify(new_user))], err);
        } else {
            callback(saved);
        }
    });
}
/*
    * Return the model of a project that can be used.
*/
module.exports.get_new_project = function(leader_id, callback) {
    const pj_key = generatedRandomKey();
    let new_project = new Project({
        team: {leader: leader_id, members: []},
        name: 'test-project-' + pj_key,
        description: 'test-description-' + pj_key,
        asset: [],
        linked_user: leader_id,
        creation: Date.now()
    });
    callback(new_project);
}
/*
    * Return the saved document of a Project.
*/
module.exports.create_test_project = function(leader_id, callback) {
    const pj_key = generatedRandomKey()
    let new_project = new Project({
        team: {leader: leader_id, members: []},
        name: 'test-project-' + pj_key,
        description: 'test-description-' + pj_key,
        asset: [],
        linked_user: leader_id,
        creation: Date.now()
    });

    Project.add(new_project, (err, saved) => {
        if (err) {
            logs.server.error('test_models > create_test_project > Project.add', [String(JSON.stringify(new_project))], err);
        } else {
            User.findById(leader_id, (err, found_user) => {
                if (err) {
                    logs.server.error('test_models > create_test_project > User.findById', [String(leader_id)], err);
                } else {
                    found_user.RelatedProjects.push(saved._id);
                    found_user.save((err, saved2) => {
                        if (err) {
                            logs.server.error('test_models > create_test_project > found_user.save', [], err);
                        } else {
                            callback(saved);
                        }
                    })
                }
            });
        }
    });
}
/*
    * Return the model of an asset that can be used.
*/
module.exports.get_test_asset = function(pj_id, type, linked_user, callback) {
    const asset_key = generatedRandomKey();
    let new_asset = new Asset({
        name: 'test-asset-' + asset_key,
        project_id: pj_id,
        type: type, // 1-Server, 2-Website, 3-Infrastructure, etc...
        asset_description: 'test-descri-' + asset_key,
        informations_linked: [],
        linked_asset: [],
        linked_user: linked_user,
        creation: Date.now()
    });
    callback(new_asset);
}
/*
    * Return the saved document of an asset.
*/
module.exports.create_test_asset = function(pj_id, type, linked_user, callback) {
    const asset_key = generatedRandomKey();
    let new_asset = new Asset({
        name: 'test-asset-' + asset_key,
        project_id: pj_id,
        type: type, // 1-Server, 2-Website, 3-Infrastructure, etc...
        asset_description: 'test-descri-' + asset_key,
        informations_linked: [],
        linked_asset: [],
        linked_user: linked_user,
        creation: Date.now()
    });
    
    Asset.add(new_asset, (err, saved) => {
        if (err) {
            logs.server.error('test_models > create_test_asset > Asset.add', [String(JSON.stringify(new_asset))], err);
        } else {
            Project.findById(pj_id, (err, found_pj) => {
                found_pj.assets.push(new_asset._id);
                found_pj.save(() => {
                    callback(saved);
                });
            });
        }
    });
}
/*
    * Return the model of an information can be used.
*/
module.exports.get_new_information = function(asset_id, type, linked_user, cvss=99, callback) {
    const info_key = generatedRandomKey();
    if (cvss > 10) {
        cvss = randomIntInc(0,10)
    }
    let new_info = new Information({
        title: 'test-info-' + info_key,
        type: type, // 1-Vulnerability, 2-Security, 3-Info
        cvss: cvss,
        linked_informations: [],
        description: 'test-descri-' + info_key,
        tool_output: 'test-output-' + info_key,
        poc: [],
        linked_asset: asset_id,
        linked_user: linked_user,
        creation: Date.now()
    });
    callback(new_info);
}
/*
    * Return the saved document of an information.
*/
module.exports.create_test_information = function(asset_id, type, linked_user, cvss=99, callback) {
    const info_key = generatedRandomKey();
    if (cvss > 10) {
        cvss = randomIntInc(0,10)
    }
    let new_info = new Information({
        title: 'test-info-' + info_key,
        type: type, // 1-Vulnerability, 2-Security, 3-Info
        cvss: cvss,
        linked_informations: [],
        description: 'test-descri-' + info_key,
        tool_output: 'test-output-' + info_key,
        poc: [],
        linked_asset: asset_id,
        linked_user: linked_user,
        creation: Date.now()
    });
    Information.add(new_info, (err, saved) => {
        if (err) {
            logs.server.error('test_models > create_test_information > Information.add', [String(JSON.stringify(new_info))], err);
        } else {
            Asset.findById(asset_id, (err, found_asset) => {
                found_asset.informations_linked.push(saved._id);
                found_asset.save(() => {
                    callback(saved);
                });
            });
        }
    });
}

function generatedRandomKey() {
    length = 4;
    key = '';
    for (let i=0; i<length; i++) {
        key += String(randomIntInc(0,9));
    }

    return key
}

function randomIntInc(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}