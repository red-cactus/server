const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../Models/User');
const config = require ('../configs/jwt');
const logs = require('../plugins/log_manager');
// const encrypter = require('../plugins/encrypter');

module.exports = function(passport){
    let opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.secret;
    passport.use(new JwtStrategy(opts, (jwt_payload, done) => {
      // Verify Session ID:
      User.findByUsername(jwt_payload.username, (err, user) => {
        if (err) {
          logs.server.error('Passport Module > User.findByUsername', [String(jwt_payload.username)], err);
        } else if (!user) {
          logs.server.info('Passport Module', 'Cannot find user with {' + String(jwt_payload.username) + '}');
          return done(null, false);
        } else {
          let isIdPresent = false;
          for(let i = 0; i<user.ActiveSessions.length; i++) {
            if (jwt_payload.session === user.ActiveSessions[i].id) {
              isIdPresent = true;
              if (user.ActiveSessions[i].timestamp + 345600 > (new Date().getTime() / 1000)) {
                if ((user.Username === jwt_payload.username)) {
                  return done(null, true);
                }
              } else {
                User.deleteSession(user, i, (err) => {
                  if (err) {
                    logs.server.error('Passport Module > User.deleteSession', [String(jwt_payload.username), String(i)], err);
                  } else {
                    logs.server.info('Passport Module', 'Session Outdated - {' + String(jwt_payload.username) + '}');
                    return done(null, false);
                  }
                });
              }
            }
            if ((i+1 === user.ActiveSessions.length) && !isIdPresent) {
              logs.server.info('Passport Module', 'Cannot find valid sessionId in user schema - {' + String(jwt_payload.username) + '}');
              return done(null, false);
            }
          }

        }
      })
    }));
};
