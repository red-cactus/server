const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const passport = require("passport");
const mongoose = require("mongoose");
const http = require("http");
const https = require("https");
const fs = require("fs");

// Import add-ons
const db_config = require("./configs/database");
const logs = require("./plugins/log_manager");

// Import routes
const user_route = require("./Routes/user-management");
const project_route = require("./Routes/projects");
const offer_route = require("./Routes/offers-management");
const pre_sales_route = require("./Routes/pre-sales-management");
const admin_route = require('./Routes/admin-management');

const app = express();
const port = 7535;

let startup_error = false;

// app.use(cors({origin: 'https://10.10.9.232'}));
app.use(cors({}));

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

try {
    app.use(bodyParser.json({ limit: '10mb' }));
    app.use(bodyParser.urlencoded({ extended: true, limit: '10mb' }));
} catch (e) {
    logs.server.error('main-module', [], 'Error with Body-Parser');
    startup_error = true;
}
try {
    app.use(passport.initialize());
    app.use(passport.session());
    require('./configs/passport')(passport);
} catch (e) {
    logs.server.error('main-module', [], 'Error with Passport');
    startup_error = true;
    throw e;
}

mongoose.connect(db_config.database);
// On Connection
mongoose.connection.on('connected', () => {
    logs.server.info('main-module', 'server connected to database');
});
// If Connection Failed
mongoose.connection.on('error', (err) => {
    logs.server.error('main-module > mongoose.connection.error', [], err)
    startup_error = true;
});

// Initialize Routes
app.use('/users', user_route);
app.use('/project', project_route);
app.use('/offer', offer_route);
app.use('/pre-sales', pre_sales_route);
app.use('/admin', admin_route);

app.get('/', (req, res) => {
    res.send("<h1>You are not supposed to be here !</h1>");
});


logs.server.alert('main-module', 'HTTPS unvaible');
let httpServer = http.createServer(app);
    
httpServer.listen(port, () => {
    logs.server.alert('main-module', 'Server serving on HTTP on port ' + port);
});

module.exports = httpServer;

/*
if (startup_error) {
    logs.server.error('main-module > CRITICAL', [], 'Critical error during startup, please verify main.js');
    throw('Critical error during startup...');
} else {
    try {
    
        if (__dirname.includes('C:\\')) {
            logs.server.alert('main-module', 'Server running on Windows');
            throw('HTTPS not working on windows Environment...');
        } else {
            logs.server.info('main-module', 'Server running on Unix');
        }
        
        const https_config = {
            key: fs.readFileSync('./configs/ssl/privateKey.key'),
            cert: fs.readFileSync('./configs/ssl/certificate.crt')
        }
        
        let httpsServer = https.createServer(https_config, app).listen(7535, () => {
            logs.server.info('main-module', 'Server serving via HTTPS on port ' + port);
        });

        module.exports = httpsServer;
    } catch (e) {
        logs.server.alert('main-module', 'HTTPS unvaible');
        let httpServer = http.createServer(app);
    
        httpServer.listen(port, () => {
            logs.server.alert('main-module', 'Server serving on HTTP on port ' + port);
        });

        module.exports = httpServer;
    }
}
*/
