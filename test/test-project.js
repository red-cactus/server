/* eslint-disable security/detect-object-injection */
const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require("../main");
const should = chai.should();
const jwt = require("jsonwebtoken");
const jwt_config = require("../configs/jwt");

const mongoose = require("mongoose");
const db_config = require("../configs/database");
const encrypter = require("../plugins/encrypter");
const test_models = require("../configs/test_models");

mongoose.connect(db_config.database);
// On Connection
mongoose.connection.on('connected', () => {});

chai.use(chaiHttp);

// Models
const User = require("../Models/User");
const Project = require("../Models/Project");
const Asset = require("../Models/Asset");
const Information = require('../Models/Information');

// test user
const test_username = 'testuserpj01';
const test_password = 'passtestpj01';
const test_email = 'testpj01@test.com';
const test_name = 'namepj01';
const test_phone = 'phonepj01';
const test_post = 'test postpj';

// Test project
const test_pj_name = 'TestProject01';
const test_pj_description = 'TestPJDesc01';

purge_db();

describe('Verify project route', () => {

    it ('Should return status code 200', function(done) {
        chai.request(server)
        .post('/project/test-route')
        .end(function(err, res) {
            res.should.have.status(200);
            done();
        });
    });
    
});

// Create test user :

describe('Test Project Managment', () => {

    it('Should create a new project', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.get_new_project(t_user._id, (n_project) => {
                    chai.request(server)
                    .post('/project/create')
                    .set('Authorization', 'bearer ' + t_token)
                    .send({'name': n_project.name, 'description': n_project.description, 'token': t_token})
                    .end(function(err, res) {
                        // Verify res
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.success.should.equal(true);
                        // Verify if project is created
                        Project.findOne({'name': n_project.name}, (err, created_project) => {
                            created_project.should.be.a('object');
                            created_project.name.should.equal(n_project.name);
                            created_project.description.should.equal(n_project.description);
                            done();
                        });
                    });
                });
            });
        });
    });

    it('Should get all created projects', function(done) {
        create_test_batch_information((t_user, t_token, t_project) => {
            chai.request(server)
            .post('/project/get-user-projects')
            .set('Authorization', 'bearer ' + t_token)
            .send({'token': t_token})
            .end(function(err, res) {
                // Verify res
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('projects');
                res.body.projects.should.be.a('array');
                res.body.projects[0].name.should.equal(t_project.name);
                done();
            });
        });
    });

    it('Should get project details', function(done) {
        create_test_batch_information((t_user, t_token, t_project) => {
            chai.request(server)
            .post('/project/get-project-details')
            .set('Authorization', 'bearer ' + t_token)
            .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'token': t_token})
            .end(function(err, res) {
                // Verify res
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                done();
            });
        });
    });

    it('Should edit the project', function(done) {
        create_test_batch_information((t_user, t_token, t_project) => {
            test_models.get_new_project(t_user._id, (n_project) => {
                chai.request(server)
                .post('/project/edit-project')
                .set('Authorization', 'bearer ' + t_token)
                .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'name': n_project.name, 'description': n_project.description, 'token': t_token})
                .end(function(err, res) {
                    // Verify res
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    Project.findOne({'name': n_project.name}, (err, found_pj) => {
                        found_pj.name.should.equal(n_project.name);
                        found_pj.description.should.equal(n_project.description);
                        done();
                    });
                });
            });
        });
    });

    it('Should delete the project', function(done) {
        create_test_batch_information((t_user, t_token, t_project) => {
            chai.request(server)
            .post('/project/delete-project')
            .set('Authorization', 'bearer ' + t_token)
            .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'token': t_token})
            .end(function(err, res) {
                // Verify res
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                Project.findById(t_project._id, (err, found_pj) => {
                    should.not.exist(found_pj);
                    done();
                });
            });
        });
    });

    it('Should return project informations', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            chai.request(server)
            .post('/project/get-project-informations')
            .set('Authorization', 'bearer ' + t_token)
            .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'token': t_token})
            .end(function(err, res) {
                // Verify res
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.assets[0].asset_name.should.equal(t_asset.name);
                res.body.assets[0].informations[0].title.should.equal(t_info.title);
                done();
            });
        });
    });

    it('Should link a new user to the project', function(done) {
        create_test_batch_information((t_user, t_token, t_project) => {
            test_models.create_test_user((t_user2) => {
                chai.request(server)
                .post('/project/link-user-with-project')
                .set('Authorization', 'bearer ' + t_token)
                .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'username': t_user2.Username, 'token': t_token})
                .end(function(err, res) {
                    // Verify res
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    User.findById(t_user2._id, (err, found_user) => {
                        compare_bson(found_user.RelatedProjects[0], t_project._id, (is_bson_equal) => {
                            is_bson_equal.should.equal(true);
                            done();
                        });
                    });
                });
            })
        });
    });

    it('Should unlink a user from the project', function(done) {
        create_test_batch_information((t_user, t_token, t_project) => {
            test_models.create_test_user((t_user2) => {
                chai.request(server)
                .post('/project/link-user-with-project')
                .set('Authorization', 'bearer ' + t_token)
                .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'username': t_user2.Username, 'token': t_token})
                .end(function(err, res) {
                    // Unlink user
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    create_test_batch_information((t_user, t_token, t_project) => {
                        test_models.create_test_user((t_user2) => {
                            chai.request(server)
                            .post('/project/unlink-user-project')
                            .set('Authorization', 'bearer ' + t_token)
                            .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'username': t_user2.Username, 'token': t_token})
                            .end(function(err, res) {
                                // Verify res
                                res.should.have.status(200);
                                res.should.be.json;
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.success.should.equal(true);
                                User.findById(t_user2._id, (err, found_user) => {
                                    found_user.RelatedProjects.should.be.empty;
                                    done();
                                });
                            });
                        })
                    });
                });
            })
        });
    });
});

describe('Asset Management tests', () => {

    it('Should create an asset in test project', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.get_test_asset(t_project._id, 1, t_user._id, (asset_infos) => {
                        chai.request(server)
                        .post('/project/create-asset')
                        .set({'Authorization': 'bearer ' + t_token})
                        .send({'token': t_token, 'a_name': asset_infos.name, 'asset_desc': asset_infos.asset_description, 'type': asset_infos.type, 'project_id': encrypter.crypt(JSON.stringify(t_project._id))})
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a('object');
                            res.body.should.have.property('success');
                            res.body.success.should.equal(true);
                            Asset.findOne({'name': asset_infos.name}, (err, found_asset) => {
                                found_asset.should.be.a('object');
                                found_asset.name.should.equal(asset_infos.name);
                                found_asset.asset_description.should.equal(asset_infos.asset_description);
                                found_asset.type.should.equal(asset_infos.type);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should gather all created assets information (3 differents assets)', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    let asset_list = [];
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset1) => {
                        asset_list.push(t_asset1);
                        test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset2) => {
                            asset_list.push(t_asset2);
                            test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset3) => {
                                asset_list.push(t_asset3);
                                chai.request(server)
                                .post('/project/get-project-assets')
                                .set({'Authorization': 'bearer ' + t_token})
                                .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id))})
                                .end(function(err, res) {
                                    res.should.have.status(200);
                                    res.should.be.json;
                                    res.body.should.be.a('object');
                                    res.body.should.have.property('success');
                                    res.body.success.should.equal(true);
                                    res.body.should.have.property('assets');
                                    for (let i=0; i<asset_list.length; i++) {
                                        Asset.findOne({'name': asset_list[i].name}, (err, found_asset) => {
                                            found_asset.should.be.a('object');
                                            found_asset.name.should.equal(asset_list[i].name);
                                            found_asset.asset_description.should.equal(asset_list[i].asset_description);
                                            found_asset.type.should.equal(asset_list[i].type);
                                        });
                                        if (i==asset_list.length-1) {
                                            done();
                                        }
                                    }
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should link two assets', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset_1) => {
                        test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset_2) => {
                            chai.request(server)
                            .post('/project/link-asset-to-asset')
                            .set({'Authorization': 'bearer ' + t_token})
                            .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'link_asset_id': encrypter.crypt(JSON.stringify(t_asset_1._id)), 'link_to_asset_id': encrypter.crypt(JSON.stringify(t_asset_2._id))})
                            .end(function(err, res) {
                                res.should.have.status(200);
                                res.should.be.json;
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.success.should.equal(true);
                                Asset.findOne({'name': t_asset_1.name}, (err, found_asset) => {
                                    found_asset.should.be.a('object');
                                    found_asset.should.have.property('linked_asset');
                                    let sanitized = JSON.stringify(found_asset.linked_asset[0]);
                                    sanitized.should.equal(JSON.stringify(t_asset_2._id));
                                    Asset.findOne({'name': t_asset_2.name}, (err, found_asset_2) => {
                                        found_asset_2.should.be.a('object');
                                        found_asset_2.should.have.property('linked_asset');
                                        let sanitized = JSON.stringify(found_asset_2.linked_asset[0]);
                                        sanitized.should.equal(JSON.stringify(t_asset_1._id));
                                        done();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should unlink assets', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset_1) => {
                        test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset_2) => {
                            t_asset_1.linked_asset = [t_asset_2._id];
                            t_asset_1.save((err, saved) => {
                                t_asset_2.linked_asset = [t_asset_1._id];
                                t_asset_2.save((err, saved) => {
                                    chai.request(server)
                                    .post('/project/unlink-assets')
                                    .set({'Authorization': 'bearer ' + t_token})
                                    .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'unlink_asset_1': encrypter.crypt(JSON.stringify(t_asset_1._id)), 'unlink_asset_2': encrypter.crypt(JSON.stringify(t_asset_2._id))})
                                    .end(function(err, res) {
                                        res.should.have.status(200);
                                        res.should.be.json;
                                        res.body.should.be.a('object');
                                        res.body.should.have.property('success');
                                        res.body.success.should.equal(true);
                                        Asset.findOne({'name': t_asset_1.name}, (err, found_asset) => {
                                            found_asset.should.be.a('object');
                                            found_asset.should.have.property('linked_asset');
                                            found_asset.linked_asset.should.be.empty;
                                            Asset.findOne({'name': t_asset_2.name}, (err, found_asset_2) => {
                                                found_asset_2.should.be.a('object');
                                                found_asset_2.should.have.property('linked_asset');
                                                found_asset_2.linked_asset.should.be.empty;
                                                done();
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should delete an asset from the project', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset) => {
                        chai.request(server)
                        .post('/project/delete-asset')
                        .set({'Authorization': 'bearer ' + t_token})
                        .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'asset_id': encrypter.crypt(JSON.stringify(t_asset._id))})
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a('object');
                            res.body.should.have.property('success');
                            res.body.success.should.equal(true);
                            Asset.findOne({'name': t_asset.name}, (err, found_asset) => {
                                should.not.exist(found_asset);
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should return asset details', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset) => {
                        test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset_2) => {
                            test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset_3) => {
                                t_asset.linked_asset = [t_asset_2._id, t_asset_3._id];
                                t_asset.save(() => {
                                    chai.request(server)
                                    .post('/project/get-asset-details')
                                    .set({'Authorization': 'bearer ' + t_token})
                                    .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'asset_id': encrypter.crypt(JSON.stringify(t_asset._id))})
                                    .end(function(err, res) {
                                        res.should.have.status(200);
                                        res.should.be.json;
                                        res.body.should.be.a('object');
                                        res.body.should.have.property('success');
                                        res.body.success.should.equal(true);
                                        res.body.should.have.property('asset');
                                        res.body.asset.linked_asset[0].name.should.equal(t_asset_2.name);
                                        res.body.asset.linked_asset[1].name.should.equal(t_asset_3.name);
                                        done();
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should edit the project details', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset) => {
                        chai.request(server)
                        .post('/project/edit-asset')
                        .set({'Authorization': 'bearer ' + t_token})
                        .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'asset_id': encrypter.crypt(JSON.stringify(t_asset._id)), 'name': 'modified', 'description': 'modified_desc', 'type': 2})
                        .end(function(err, res) {
                            res.should.have.status(200);
                            res.should.be.json;
                            res.body.should.be.a('object');
                            res.body.should.have.property('success');
                            res.body.success.should.equal(true);
                            Asset.findById(t_asset._id, (err, found_asset) => {
                                found_asset.name.should.equal('modified');
                                found_asset.asset_description.should.equal('modified_desc');
                                found_asset.type.should.equal('2');
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should return the asset informations', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset) => {
                        test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info) => {
                            test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info_2) => {
                                test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info_3) => {
                                    let info_array = [t_info, t_info_2, t_info_3];
                                    t_asset.informations_linked = info_array;
                                    t_asset.save((err, saved) => {
                                        chai.request(server)
                                        .post('/project/get-asset-informations')
                                        .set({'Authorization': 'bearer ' + t_token})
                                        .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'asset_id': encrypter.crypt(JSON.stringify(t_asset._id))})
                                        .end(function(err, res) {
                                            res.should.have.status(200);
                                            res.should.be.json;
                                            res.body.should.be.a('object');
                                            res.body.should.have.property('success');
                                            res.body.success.should.equal(true);
                                            res.body.should.have.property('infos');
                                            for (let i=0; i<info_array.length; i++) {
                                                res.body.infos[i].title.should.equal(info_array[i].title);
                                                if (i==2) {
                                                    done();
                                                }
                                            }
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});

describe('Information management', () => {

    it('Should create an information', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset) => {
                        test_models.get_new_information(t_asset._id, 1, t_user._id, 5, (tn_info) => {
                            chai.request(server)
                            .post('/project/add-information')
                            .set({'Authorization': 'bearer ' + t_token})
                            .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'asset_id': encrypter.crypt(JSON.stringify(t_asset._id)), 'title': tn_info.title, 'description': tn_info.description, 'type': tn_info.type})
                            .end(function(err, res) {
                                res.should.have.status(200);
                                res.should.be.json;
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.success.should.equal(true);
                                Information.findOne({'title': tn_info.title}, (err, found_info) => {
                                    found_info.description.should.equal(tn_info.description);
                                    found_info.type.should.equal(tn_info.type);
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });

    it('Should link two informations with correct relation', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info2) => {
                test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info3) => {
                    test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info4) => {
                        let info_array = [t_info2, t_info3, t_info4];
                        for (let i=0; i<info_array.length; i++) {
                            let relation = (i+1)*2
                            if (relation > 5) {
                                relation = 5
                            }
                            chai.request(server)
                                .post('/project/link-informations')
                                .set('Authorization', 'bearer ' + t_token)
                                .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info1_id': encrypter.crypt(JSON.stringify(t_info._id)), 'info2_id': encrypter.crypt(JSON.stringify(info_array[i]._id)), 'relation': relation, 'token': t_token})
                                .end(function(err, res) {
                                // Verify res
                                res.should.have.status(200);
                                res.should.be.json;
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.success.should.equal(true);
                                Information.findById(info_array[i]._id, (err, found_info) => {
                                    let str_id = JSON.stringify(found_info.linked_informations[0].id);
                                    str_id.should.equal(JSON.stringify(t_info._id));
                                });
                            });
                            if (i == info_array.length-1) {
                                done();
                            }
                        }
                    });
                });
            });
        });
    });

    it('Should unlink informations', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            test_models.create_test_information(t_asset._id, 1, t_user._id, 4, (t_info2) => {
                Information.link_information(t_info._id, t_info2._id, () => {
                    chai.request(server)
                    .post('/project/unlink-information')
                    .set({'Authorization': 'bearer ' + t_token})
                    .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info1_id': encrypter.crypt(JSON.stringify(t_info._id)), 'info2_id': encrypter.crypt(JSON.stringify(t_info2._id))})
                    .end(function(err, res) {
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.success.should.equal(true);
                        Information.findOne({'title': t_info.title}, (err, found_info) => {
                            found_info.linked_informations.should.be.empty;
                            done();
                        });
                    });
                });
            });
        });
    });

    it('Should delete the information', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            chai.request(server)
            .post('/project/delete-information')
            .set({'Authorization': 'bearer ' + t_token})
            .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info_id': encrypter.crypt(JSON.stringify(t_info._id))})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                Information.findOne({'title': t_info.title}, (err, found_info) => {
                    if (!found_info) {
                        done();
                    }
                });
            });
        });
    });

    it('Should edit the information', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            test_models.get_new_information(t_asset, 2, t_user._id, 5, (n_info) => {
                chai.request(server)
                .post('/project/edit-information')
                .set({'Authorization': 'bearer ' + t_token})
                .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info_id': encrypter.crypt(JSON.stringify(t_info._id)), 'title': n_info.title, 'type': n_info.type, 'cvss': n_info.cvss, 'description': n_info.description, 'tool_output': n_info.tool_output})
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    Information.findById(t_info._id, (err, found_info) => {
                        found_info.title.should.equal(n_info.title);
                        found_info.description.should.equal(n_info.description);
                        found_info.cvss.should.equal(n_info.cvss);
                        found_info.tool_output.should.equal(n_info.tool_output);
                        done();
                    });
                });
            });
        });
    });

    it('Should get information image back', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            t_info.poc.push('123456');
            t_info.save((err, t_info) => {
                chai.request(server)
                .post('/project/get-information-images')
                .set({'Authorization': 'bearer ' + t_token})
                .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info_id': encrypter.crypt(JSON.stringify(t_info._id))})
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    res.body.should.have.property('poc');
                    res.body.poc[0].should.equal('123456');
                    done();
                });
            });
        });
    });

    it('Should delete one of the project POC', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            t_info.poc = ['123', '456', '789'];
            t_info.save((err, t_info) => {
                chai.request(server)
                .post('/project/delete-information-image')
                .set({'Authorization': 'bearer ' + t_token})
                .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info_id': encrypter.crypt(JSON.stringify(t_info._id)), 'image_position': 1})
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    Information.findById(t_info._id, (err, found_info) => {
                        found_info.poc[0].should.equal('123');
                        found_info.poc[1].should.equal('789');
                        done();
                    });
                });
            });
        });
    });

    it('Should add an image to the information POC', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            chai.request(server)
            .post('/project/add-information-image')
            .set({'Authorization': 'bearer ' + t_token})
            .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info_id': encrypter.crypt(JSON.stringify(t_info._id)), 'image': '123456789'})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                Information.findById(t_info._id, (err, found_info) => {
                    found_info.poc[0].should.equal('123456789');
                    done();
                });
            });
        });
    });

    it('Should get information details', function(done) {
        create_test_batch_information((t_user, t_token, t_project, t_asset, t_info) => {
            t_info.poc = ['123456789'];
            t_info.save((err, t_info) => {
                chai.request(server)
                .post('/project/get-information-details')
                .set({'Authorization': 'bearer ' + t_token})
                .send({'token': t_token, 'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'info_id': encrypter.crypt(JSON.stringify(t_info._id))})
                .end(function(err, res) {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    res.body.should.have.property('success');
                    res.body.success.should.equal(true);
                    Information.findById(t_info._id, (err, found_info) => {
                        found_info.poc[0].should.equal('123456789');
                        done();
                    });
                });
            });
        });
    });
});

/*
describe('Manipulation Assets Tests', () => {

    it('Should link assets', function(done) {
        test_models.create_test_user((t_user) => {
            User.create_token(t_user, t_user.Username, null, (t_token) => {
                test_models.create_test_project(t_user._id, (t_project) => {
                    test_models.create_new_asset(t_project._id, 1, t_user._id, (t1_asset) => {
                        test_models.create_new_asset(t_project._id, 1, t_user._id, (t2_asset) => {
                            chai.request(server)
                            .post('/project/link-asset-to-asset')
                            .set('Authorization', 'bearer ' + t_token)
                            .send({'project_id': encrypter.crypt(JSON.stringify(t_project._id)), 'link_asset_id': encrypter.crypt(JSON.stringify(t1_asset._id)), 'link_to_asset_id': encrypter.crypt(JSON.stringify(t2_asset._id)), 'token': t_token})
                            .end(function(err, res) {
                                // Verify res
                                res.should.have.status(200);
                                res.should.be.json;
                                res.body.should.be.a('object');
                                res.body.should.have.property('success');
                                res.body.success.should.equal(true);
                                Project.findById(t_project_id, (err, found_pj) => {
                                    should.not.exist(found_pj);
                                    done();
                                });
                            });
                        });
                    });
                });
            });
        });
    });
});
*/

// Asset management tests
/*
describe('Asset management tests', () => {

    // 1st creating the user and the project used for test:
    let t_user;
    let t_token;
    let t_project_encrypted_id;

    user_creation((test_user, auth_token) => {
        console.log('DEBUG');
        console.log(test_user);
        t_user = test_user;
        t_token = auth_token;

        create_project(t_user._id, (t_prj) => {
            t_project_encrypted_id = encrypter.crypt(JSON.stringify(t_prj._id))
        });
    });

    // Assets names:
    let test_assets = [
        {name: 'asset_test_01_server', type: 1, description: 'asset_desc_test_01'},
        {name: 'asset_test_02_website', type: 2, description: 'asset_desc_test_02'},
        {name: 'asset_test_03_infra', type: 3, description: 'asset_desc_test_03'},
        {name: 'asset_test_04', type: 1, description: 'asset_desc_test_04'},
        {name: 'asset_test_05', type: 1, description: 'asset_desc_test_05'}
    ]

    // Normal asset functions
    it('Should create 3 new assets in the project', function(done) {
        for (let i=0; i<3; i++) {
            chai.request(server)
            .post('/project/create-asset')
            .set('Authorization', 'bearer ' + t_token)
            // eslint-disable-next-line security/detect-object-injection
            .send({'project_id': t_project_encrypted_id, 'a_name': test_assets[i].name, 'asset_desc': test_assets[i].description, 'type': test_assets[i].type, 'token': t_token})
            .end(function(err, res) {
                // Verify res
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
            });
        }
    });

});
*/
// purge_db();

function create_test_batch_information(callback) {
    test_models.create_test_user((t_user) => {
        User.create_token(t_user, t_user.Username, null, (t_token) => {
            test_models.create_test_project(t_user._id, (t_project) => {
                test_models.create_test_asset(t_project._id, 1, t_user._id, (t_asset) => {
                    test_models.create_test_information(t_asset._id, 1, t_user._id, 5, (t_info) => {
                        callback(t_user, t_token, t_project, t_asset, t_info);
                    });
                });
            });
        });
    });
}

function compare_bson(bson1, bson2, callback) {
    let compare1 = JSON.stringify(bson1);
    let compare2 = JSON.stringify(bson2);
    callback(compare1==compare2);
}

function purge_db() {
    User.deleteMany({}, () => {});
    Project.deleteMany({}, () => {});
    Asset.deleteMany({}, () => {});
    Information.deleteMany({}, () => {});
}