const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require("../main");
const should = chai.should();

const mongoose = require("mongoose");
const db_config = require("../configs/database");

mongoose.connect(db_config.database);
// On Connection
mongoose.connection.on('connected', () => {});

chai.use(chaiHttp);
const User = require("../Models/User");

/*
User.deleteMany({}, () => {
    console.log();
});
*/
// Unit Tests
describe('Verify user route', () => {

    it ('Should return status code 200', function(done) {
        chai.request(server)
        .post('/users/test-route')
        .end(function(err, res) {
            res.should.have.status(200);
            done();
        });
    });
    
});

describe('User creation process', () => {
    const test_username = 'test01';
    const test_password = 'passtest01';
    const test_email = 'test01@test.com';
    const test_name = 'name01';
    const test_phone = 'phone01';
    const test_post = 'test post';

    it ('Should create a user with all correct informations', function(done) {
        chai.request(server)
        .post('/users/new-user')
        .send({'username': test_username, 'password': test_password, 'email': test_email, 'name': test_name, 'phone': test_phone, 'post': test_post})
        .end(function(err, res) {
            // Verify res
            res.should.have.status(200);
            res.should.be.json;
            res.body.should.be.a('object');
            res.body.should.have.property('success');
            res.body.success.should.equal(true);
            // Verify if user is created
            User.findByEmail(test_email, (err, found_user) => {
                found_user.should.be.a('object');
                found_user.Username.should.equal(test_username);
                found_user.Email.should.equal(test_email);
                found_user.Name.should.equal(test_name);
                found_user.Phone.should.equal(test_phone);
                found_user.Post.should.equal(test_post);
                done();
            });
        })
    });

    it('Should be denied because the username is already used', function(done) {
        chai.request(server)
            .post('/users/new-user')
            .send({'username': test_username, 'password': test_password, 'email': 'test_email02', 'name': test_name, 'phone': test_phone, 'post': test_post})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.equal('user_taken');
                done();
            });
    });

    it('Should be denied because the email is already used', function(done) {
        chai.request(server)
            .post('/users/new-user')
            .send({'username': 'test_username', 'password': test_password, 'email': test_email, 'name': test_name, 'phone': test_phone, 'post': test_post})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(false);
                res.body.should.have.property('msg');
                res.body.msg.should.equal('email_taken');
                done();
            });
    });

    it('Should login the user and catch the auth token', function(done) {
        chai.request(server)
            .post('/users/login')
            .send({'username': test_username, 'password': test_password})
            .end(function(err, res) {
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('success');
                res.body.success.should.equal(true);
                res.body.should.have.property('token');
                done();
            });
    });

});

User.deleteMany({}, () => {
    console.log();
});


// end user tests