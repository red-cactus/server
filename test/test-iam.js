const chai = require("chai");
const chaiHttp = require('chai-http');
const server = require("../main");
const should = chai.should();

const testModel = require("../configs/test_models");
const encrypter = require("../plugins/encrypter");

const mongoose = require("mongoose");
const db_config = require("../configs/database");

mongoose.connect(db_config.database);
// On Connection
mongoose.connection.on('connected', () => {});

chai.use(chaiHttp);

// Models
const User = require("../Models/User");
const Project = require("../Models/Project");
const Asset = require("../Models/Asset");
const Information = require("../Models/Information");

// purge_db();

// Create Test Models
describe('Basic IAM tests with project', () => {
    it('Should validate the user and create a new asset', function(done) {
        create_test_batch((user1, u1_token, user2, u2_token, user3, u3_token, user4, u4_token) => {
            testModel.create_test_project(user1._id, (project1) => {
                testModel.get_test_asset(project1._id, 1, user2._id, (asset_infos) => {
                    chai.request(server)
                    .post('/project/create-asset')
                    .set('Authorization', 'bearer ' + u1_token)
                    .send({'a_name': asset_infos.name, 'asset_desc': asset_infos.asset_description, 'type': 1, 'project_id': encrypter.crypt(JSON.stringify(project1._id)), 'token': u1_token})
                    .end(function(err, res) {
                        // Verify res
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.be.a('object');
                        res.body.should.have.property('success');
                        res.body.success.should.equal(true);
                        // Verify if asset is created
                        Asset.findOne({'name': asset_infos.name}, (err, created_asset) => {
                            created_asset.should.be.a('object');
                            created_asset.name.should.equal(asset_infos.name);
                            created_asset.type.shoudl.equal(asset_infos.type);
                            created_asset.asset_description.should.equal(asset_infos.asset_description);
                            done();
                        });
                    });
                });
            });
        });
    });
});

// purge_db();

function purge_db() {
    User.deleteMany({}, () => {});
    Project.deleteMany({}, () => {});
    Asset.deleteMany({}, () => {});
    Information.deleteMany({}, () => {});
}

function create_test_batch(callback) {
    testModel.create_test_user((user1) => {
        testModel.create_test_user((user2) => {
            testModel.create_test_user((user3) => {
                testModel.create_test_user((user4) => {
                    User.create_token(user1, user1.Username, null, (u1_token) => {
                        User.create_token(user2, user2.Username, null, (u2_token) => {
                            User.create_token(user3, user3.Username, null, (u3_token) => {
                                User.create_token(user4, user4.Username, null, (u4_token) => {
                                    callback(user1, u1_token, user2, u2_token, user3, u3_token, user4, u4_token);
                                });
                            });
                        });
                    });
                });
            });
        });
    });    
}