const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const encrypter = require("../plugins/encrypter");

// Import Models
const Asset = require('../Models/Asset');

// User Schema
const informationSchema = mongoose.Schema({
  title: {type: String},
  type: {type: Number}, // 1-Vulnerability, 2-Security, 3-Info
  cvss: {type: Number},
  linked_informations: [],
  description: {type: String},
  tool_output: {type: String},
  poc: [],
  linked_asset: {type: String},
  linked_user: {type: String},
  creation: {type: String}
});

const Information = module.exports = mongoose.model('Information', informationSchema);

module.exports.add = function(newInfo, callback) {
  newInfo.save(callback)
}

module.exports.delete = function delete_information(info_id, callback) {
  Information.findById(info_id, (err, found_info) => {
    if (err) {
      callback(err, false);
    } else if (!found_info) {
      callback('No ressource with id', false);
    } else {
      Asset.findById(found_info.linked_asset, (err, found_asset) => {
        if (err) {
          callback(err, false);
        } else if (!found_asset) {
          callback('No ressource with id', false);
        } else {
          let new_arrray = found_asset.informations_linked.filter(function(value, index, arr){
            if (value == info_id) {
              return true;
            } else {
              return false;
            }
        });
        found_asset.informations_linked = new_arrray;
        found_asset.save((err, saved) => {
          Information.deleteOne({'_id': found_info._id}, (err) => {
            if (err) {
              callback(err, false);
            } else {
              callback(null, true);
            }
          });
        });
        }
      });
    }
  });
}

module.exports.link_information = function(info1_id, info2_id, callback) {
  Information.findById(info1_id, (err, found_info1) => {
    if (err) {
      callback(err, false);
    } else if (!found_info1) {
      callback('No ressource with id ' + info1_id, false);
    } else {
      Information.findById(info2_id, (err, found_info2) => {
        if (err) {
          callback(err, false);
        } else if (!found_info2) {
          callback('No ressource with id ' + info2_id, false);
        } else {
          if (!found_info1.linked_informations) {
            found_info1.linked_informations = [];
          }
          if (!found_info2.linked_informations) {
            found_info2.linked_informations = [];
          }
          found_info1.linked_informations.push(info2_id);
          found_info2.linked_informations.push(info1_id);
          found_info1.save((err) => {
            if (err) {
              callback(err, false);
            } else {
              found_info2.save((err) => {
                if (err) {
                  callback(err, false);
                } else {
                  callback(null, true);
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports.unlink_informations = function(info1_id, info2_id, callback) {
  Information.findById(info1_id, (err, found_info1) => {
    if (err) {
      callback(err, false);
    } else if (!found_info1) {
      callback('', false);
    } else {
      let new_arrray = found_info1.linked_informations.filter(function(value){
        if (value.id == info2_id) {
          return true;
        } else {
          return false;
        }
      });
      found_info1.linked_informations = new_arrray;
      found_info1.save(() => {
        callback('', true);
      });
    }
  });
}
