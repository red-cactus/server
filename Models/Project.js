const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const encrypter = require("../plugins/encrypter");

// User Schema
const projectSchema = mongoose.Schema({
  team: {},
  name: {type: String},
  assets: [],
  description: {type: String},
  image: {type: String},
  linked_users: [],
  creation: {type: String}
});

const Project = module.exports = mongoose.model('Project', projectSchema);

module.exports.add = function(newProject, callback) {
  newProject.save(callback)
}

module.exports.add_asset = function(project, asset_id, callback) {
  project.assets.push(asset_id);
  project.save(callback);
}
