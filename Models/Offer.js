const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const encrypter = require("../plugins/encrypter");

// User Schema
const offerSchema = mongoose.Schema({
  Name: {type: String},
  Path: {type: String},
  linked_user: {type: String},
  Lang: {type: String},
  Offer_type: {type: Number}, // 1-minifed, 2-full
  Content: [], // ['pentest', 'phishing', etc]
  Client_logo: {type: String}, // Encrypted base64 logo,
  Client_company: {type: String},
  Client_name: {type: String},
  Written: {type: Boolean}
});

const Offer = module.exports = mongoose.model('Offer', offerSchema);

module.exports.add = function(newOffer, callback) {
  newOffer.save(callback);
}

module.exports.get_all_user_offers = function(user_id, callback) {
  Offer.find({linked_user: user_id}, callback)
}
