const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const encrypter = require("../plugins/encrypter");

const spawn = require('child_process').spawn;

// User Schema
const PreSalesReportSchema = mongoose.Schema({
  Name: {type: String},
  Path: {type: String},
  linked_user: {type: String},
  Lang: {type: String},
  Target: {type: String},
  Results_id: {type: String},
  Logo: {type: String}, // Encrypted base64 logo
  Audited: {type: Boolean},
  Written: {type: Boolean},
  Errored: {type: Boolean},
  Analyse_time: {type: String}
});

const PreSalesReport = module.exports = mongoose.model('pre-sales-reports', PreSalesReportSchema);

module.exports.add = function(newReport, callback) {
  newReport.save(callback);
}

module.exports.get_all_user_reports = function(user_id, callback) {
  PreSalesReport.find({linked_user: user_id}, callback);
}

module.exports.launch_analyses = function(model) {

  console.log('Launching Analysis of ' + model.Target);
  let pyprocess = spawn('python', ["/home/npascale/PreSales/presales.py", model.Target]);
  // let pyprocess = spawn('python', ["/home/acox/Report-Helper/Server/python-scripts/pre-sales_writer/test_information.py", model.Target]);

  pyprocess.stdout.on('data', (data) => {
    data = data.toString();
    console.log(data);
    if (data.includes('id=')) {
      PreSalesReport.findById(model._id, (err, model) => {
        model.Results_id = data.split(' ')[1].split('\n')[0];
        model.Audited = true;
        PreSalesReport.add(model, (err, new_saved) => {
          PreSalesReport.write_report(new_saved);
        });
      });
    }
    if (data.includes('ElapsedTime')) {
      model.Analyse_time = data.split(' ')[1].split('\n')[1];
      PreSalesReport.findById(model._id, (err, model) => {
        PreSalesReport.add(model, (err, new_saved) => {});
      });
    }
  });
  
  pyprocess.stderr.on('data', (data) => {
      console.log('Error: ' + data);
      model.Errored = true;
      PreSalesReport.add(model, (err, new_saved) => {});
  });

}

module.exports.write_report = function(model) {
  console.log('Id = ' + model._id);
  let pyprocess = spawn('python3', ["./python-scripts/pre-sales_writer/main.py", model._id]);
  pyprocess.stdout.on('data', (data) => {
    console.log('Ending Pre-Sales Writing');
    model.Written = true;
    PreSalesReport.add(model, (err, new_saved) => {});
  });
  
  pyprocess.stderr.on('data', (data) => {
      console.log('Error: ' + data);
      model.Errored = true;
      PreSalesReport.add(model, (err, new_saved) => {});
  });
}

module.exports.get_lasts = function(user_id, callback) {
  PreSalesReport.find({linked_user: user_id}, (err, found_reports) => {
    if (err) {
      callback(err, []);
    } else {
      if (found_reports.length < 10) {
        callback('', found_reports)
      } else {
        callback('', found_reports.slice(-10))
      }
    }
  });
}
