const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const encrypter = require("../plugins/encrypter");
const logs = require("../plugins/log_manager")

// User Schema
const assetSchema = mongoose.Schema({
  name: {type: String},
  project_id: {type: String},
  type: {type: String}, // 1-Server, 2-Website, 3-Infrastructure, etc...
  asset_description: {type: String},
  informations_linked: [],
  linked_asset: [],
  linked_user: {type: String},
  creation: {type: String}
});

const Asset = module.exports = mongoose.model('Asset', assetSchema);

module.exports.add = function(newAsset, callback) {
  newAsset.save(callback)
}

module.exports.add_info = function(asset, info_id, callback) {
  asset.informations_linked.push(info_id);
  asset.save(callback);
}

module.exports.unlink_asset = function(asset1_id, asset2_id, callback) {
  Asset.findById(asset1_id, (err, found_asset) => {
    if (err) {
      callback(err, false);
    } else if (!found_asset) {
      callback(null, false);
    } else {
      let new_arrray = found_asset.linked_asset.filter(function(value){
        if (JSON.stringify(value) === JSON.stringify(asset2_id)) {
          return false
        } else {
          return true
        }
      });
      found_asset.linked_asset = new_arrray;
      found_asset.save((err) => {
        if (err) {
          callback(err, false);
        } else {
          callback('', true);
        }
      });
    }
  });
};
