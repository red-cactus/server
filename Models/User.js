const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const encrypter = require("../plugins/encrypter");
const logs = require('../plugins/log_manager.js');

const jwt = require("jsonwebtoken");
const jwt_config = require("../configs/jwt");

// User Schema
const userSchema = mongoose.Schema({
  Username: {type: String},
  Password: {type: String},
  Email: {type: String},
  Name: {type: String},
  Surname: {type: String},
  Phone: {type: String},
  Post: {type: String},
  LastConnection: {type: String},
  Is_admin: {type: Boolean},
  Await_approbation: {type: Boolean},
  Is_Validated: {},
  ActiveSessions: [],
  RelatedProjects: []
});

const User = module.exports = mongoose.model('User', userSchema);

module.exports.addUser = function(newUser, callback) {
  bcrypt.hash(newUser.Password, 10, (err, hashedPass) => {
    if (err) {
      logs.server('User Model > addUser -- Error during bcrypt.hash = ' + err, 'error');
      console.log (err);
    } else {
      newUser.Password = hashedPass;
      newUser.save(callback);
    }
  });
}

module.exports.user_exist = function(user, callback) {
  User.findOne({Username: user.Username}, (err, found_doc) => {
    if (err) {
      logs.server('User Model > user_exist -- Error during User.findOne = ' + err, 'error');
    } else if (found_doc) {
      callback(true, 'username');
    } else {
      User.findOne({Email: user.Email}, (err, found_doc) => {
        if (err) {
          console.log('ERROR');
        } else if (found_doc) {
          callback(true, 'email');
        } else {
          callback(false)
        }
      })
    }
  });
}

module.exports.findByUsername = function(username, callback) {
  User.findOne({Username: username}, callback);
}

module.exports.findByEmail = function(email, callback) {
  User.findOne({Email: email}, callback);
}

module.exports.comparePassword = function(user, candidate_pass, callback) {
  bcrypt.compare(candidate_pass, user.Password, (err, result) => {
    if (err) {
      logs.server('User Model > comparePassword -- Error during bcrypt.compare = ' + err, 'error');
    } else {
      if (result) {
        callback(true)
      } else {
        callback(false)
      }
    }
  })
}

module.exports.addSession = function(user, new_session, callback) {
  const timestamp = new Date().getTime();
  user.ActiveSessions.unshift({id: new_session, timestamp: timestamp});
  user.save(callback);
}

module.exports.deleteSession = function(user, session_postition, callback) {
  user.ActiveSessions.splice(session_postition, 1);
  user.update(callback);
}

module.exports.addProject = function(user, project_id, callback) {
  user.RelatedProjects.unshift(project_id);
  user.save(callback);
}

module.exports.create_token = function(user, username, session="test-session+++", callback) {
  if (session == 'test-session+++') {
    session = encrypter.crypt('8.8.8.8' + '+///+' + 'informations');
  }

  User.addSession(user, session, (err, updated) => {
    if (err) {
      logs.server.error('User Model > User.addSession', [String(user._id), String(session)], err);
    } else {
      const payload = {
        session: session,
        username: username
      };
      const token = jwt.sign(payload, jwt_config.secret, {
        expiresIn: 345600 // Expires in 4 days
      });
      callback(token);
    }
  });
}

module.exports.promote_admin = function(user_to_promote, callback) {
  user_to_promote.is_admin = true;
  user_to_promote.save(callback);
}

module.exports.is_user_admin = function(user, callback) {
  callback(user.Is_admin);
}

module.exports.validate_user = function(admin, user, validated, callback) {
  user.Await_approbation = false;
  user.Is_Validated = {state: validated, by: {username: admin.Username, id: admin._id}}
  user.save(callback);
}

module.exports.get_all_user_to_validate = function(callback) {
  User.find({Await_approbation: true}, callback);
}