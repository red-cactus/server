const crypto = require('crypto');
const algorithm = 'bf-cbc';
const password = 'F^f@$S4eL@8qJLbx6Gjp7A3#Bhp+NrK^hKg+GPA$TGFeTe*=h9k?v=GX*4RwCWqg*ebZ2H+_SCTqFUaECFY^$6msn_-m5LEc2+bAA49!XqvbNUcXa-E?Wb@R6z_9BTMYRv^tUCAy=hy?SESnz^z-&7_vN@TLRmdkgK2cG+8Vcr_pJfnDyyuvg^XFaacLYeYv$-YucYjeHcbKmWYQhx8vHucFMtBGYM@JV2B8PvHbx8sP^R+ESL%wkSwD+-BetyUA';
const aesAlgorithm = 'aes-256-ctr';
//const aesPassword = crypto.randomBytes(32);
const aesPassword = 'mYq3s6v9y$B&E)H@McQfTjWnZr4u7x!z';
const iv = 'hVmYq3t6w9z$B&E)';
module.exports.crypt = function(text) {

    const cipher = crypto.createCipheriv(aesAlgorithm, Buffer.from(aesPassword), iv);
    let crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;

};

module.exports.decrypt = function(text) {

    const decipher = crypto.createDecipheriv(aesAlgorithm, Buffer.from(aesPassword), iv);
    let dec = decipher.update(text,'hex','utf8');
    dec += decipher.final('utf8');
    return dec;

};

module.exports.stringRandomizer = function() {
        let text = "";
        const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789#,.;:";

        for (var i = 0; i < 32; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
};