const fs = require("fs");


const server_file_path = __dirname.split('/plugins')[0].split('\\plugins')[0] + "/logs/server.log";
const user_file_path = __dirname.split('/plugins')[0].split('\\plugins')[0] + "/logs/user.log";
const analyses_file_path = __dirname.split('/plugins')[0].split('\\plugins')[0] + "/logs/analyses.log";

module.exports = {
    server: {
        info: function(position, msg, ip='localhost', user={Username: 'Server', _id: '_server'}) {
            let date = new Date();
            let now = date.getUTCDay() + '/' + (date.getUTCMonth()+1) + '/' + date.getUTCFullYear() + ' - ' + (date.getUTCHours()+2) + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
            let registering = '[INFO]' + '[' + now + '] ' + '[' + ip + ']'  + '[' + user.Username + '-' + user._id + '] ' + position + ' - ' + msg + '\r';
            fs.appendFile(server_file_path, registering, function (err) {
                if (err) {throw err}
            });
        },
        alert: function(position, msg, ip='localhost', user={Username: 'Server', _id: '_server'}) {
            let date = new Date();
            let now = date.getUTCDay() + '/' + (date.getUTCMonth()+1) + '/' + date.getUTCFullYear() + ' - ' + (date.getUTCHours()+2) + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
            let registering = '[ALERT]' + '[' + now + '] ' + '[' + ip + ']'  + '[' + user.Username + '-' + user._id + '] alert in ' + position + ' infos: ' + msg + '\r';
            fs.appendFile(server_file_path, registering, function (err) {
                if (err) {throw err}
            });
        },
        error: function(position, params=[], error, ip='localhost', user={Username: 'Server', _id: '_server'}) {
            let date = new Date();
            let now = date.getUTCDay() + '/' + (date.getUTCMonth()+1) + '/' + date.getUTCFullYear() + ' - ' + (date.getUTCHours()+2) + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
            let registering = '[ERROR]' + '[' + now + ']' + '[' + ip + ']' + '[' + user.Username + '-' + user._id + '] error in ' + position + ' with params: {';
            for (let i=0;i<params.length; i++) {
                // eslint-disable-next-line security/detect-object-injection
                if (i==0) {
                    registering += String(params[i]);
                } else {
                    registering += ', ' + String(params[i]);
                }
            }
            registering += '} errormsg: ' + error + '\r';
            fs.appendFile(server_file_path, registering, function (err) {
                if (err) {throw err}
            });
        }
    }
}

module.exports.analyses = function(msg, type='') {
    date = new Date();
    now = date.getUTCDay() + '/' + (date.getUTCMonth()+1) + '/' + date.getUTCFullYear() + ' - ' + (date.getUTCHours()+2) + ':' + date.getUTCMinutes() + ':' + date.getUTCSeconds();
    info = 'INFO';
    if (type == 'error') {
        info = 'ERROR';
    } else if (type == 'alert') {
        info = 'ALERT';
    }
    fs.appendFile(analyses_file_path, registering, function (err) {
        if (err) throw err;
    });
}

module.exports.get_ip = function(req) {
    return String(req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : null));
}