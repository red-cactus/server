const User = require("../Models/User");
const Project = require("../Models/Project");
const Asset = require("../Models/Asset");
const Information = require("../Models/Information");

const jwt = require("jsonwebtoken");
const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");

const logs = require('../plugins/log_manager');

module.exports.has_rights_on_project = function(user_id, project_id, ressource_type=null, requested_ressource=null, callback) {
    /*
        This function verify if the user has the right on the requested ressource.
        It will return a callback containing:
        -> if the user is linked
        -> His right on the ressource (1-normal, 2-management)
    */

    if (!ressource_type) {
        is_user_in_project(user_id, project_id, (is_linked, rights, error) => {
            if (error) {
                callback(false, null, error);
            }
            if (!is_linked) {
                callback(false, null, null);
            } else {
                callback(true, rights, null);
            }
        });
    } else {
        if (ressource_type == 1) {
            // Verify asset
            is_user_in_project(user_id, project_id, (is_linked, rights, error) => {
                if (error) {
                    callback(false, null, error);
                } else if (!is_linked) {
                    callback(false, null, null);
                } else if (rights == 2) {
                    callback(true, 2, null);
                } else {
                    is_asset_part_of_project(project_id, requested_ressource, (is_linked, err) => {
                        if (err) {
                            callback(false, null, err)
                        } else if (!is_linked) {
                            callback(false, null, null);
                        } else {
                            Asset.findById(requested_ressource, (err, found_asset) => {
                                if (err) {
                                    callback(false, null, err);
                                } else if (!found_asset) {
                                    callback(false, null, 'No ressource with this ID');
                                } else {
                                    // Verify if user is owner or not
                                    if (found_asset.linked_user == user_id) {
                                        callback(true, 2, null);
                                    } else {
                                        callback(true, 1, null);
                                    }
                                }
                            });
                        }
                    });
                }
            });
        } else if (ressource_type == 2) {
            // Verify information
            is_user_in_project(user_id, project_id, (is_linked, rights, err) => {
                if (err) {
                    callback(false, null, err);
                } else if (!is_linked) {
                    callback(false, null, null);
                } else if (rights == 2) {
                    callback(true, 2, null);
                } else {
                    Information.findById(requested_ressource, (err, found_info) => {
                        if (err) {
                            callback(false, null, err);
                        } else if (!found_info) {
                            callback(false, null, 'No ressource with this ID');
                        } else {
                            is_information_part_of_project(project_id, requested_ressource, (is_linked, err) => {
                                if (err) {
                                    callback(false, null, err);
                                } else if (!is_linked) {
                                    callback(false, null, null);
                                } else {
                                    // Get user rights
                                    if (found_info.linked_user == user_id) {
                                        callback(true, 2, null);
                                    } else {
                                        Asset.findById(found_info.linked_asset, (err, found_asset) => {
                                            if (err) {
                                                callback(false, null, err);
                                            } else if (!found_asset) {
                                                callback(false, null, 'No ressource with this ID');
                                            } else {
                                                if (found_asset.linked_user == user_id) {
                                                    callback(true, 2, null);
                                                } else {
                                                    if (found_info.linked_user == user_id) {
                                                        callback(true, 2, null);
                                                    } else {
                                                        callback(true, 1, null);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            })
                        }
                    })
                }
            });
        } else {
            throw 'ressource_type must be 1 (asset) or 2 (information)';
        }
    }
}

/*
 * Get the token and search the user linked to it
 * If found, it returns the user Schemas
 */
module.exports.verify_encrypted_token = function(enc_token, callback) {
    jwt.verify(enc_token, jwt_config.secret, (err, decoded) => {
        if (err) {
            callback(null, err);
        } else {
            User.findByUsername(decoded.username, (err, found_user) => {
                if (err) {
                    callback(null, err);
                } else if (!found_user) {
                    callback(null, null);
                } else {
                    callback(found_user, null);
                }
            });
        }
    });
}

function is_user_in_project(user_id, project_id, callback) {
    User.findById(user_id, (err, candidate_user) => {
        if (err) {
            callback(false, null, err);
        } else if (!candidate_user) {
            callback(false, null, null);
        } else {
            // Get project
            Project.findById(project_id, (err, found_project) => {
                if (err) {
                    callback(false, null, err);
                } else if (!found_project) {
                    callback(false, null, 'No ressource with this ID');
                } else {
                    // Verify if user is in team:
                    if (!(found_project.team.leader > user_id) && !(found_project.team.leader < user_id)) {
                        // User is leader
                        callback(true, 2, null);
                    } else {
                        if (found_project.team.members.includes(user_id)) {
                            // User is in project
                            callback(true, 1, null);
                        } else {
                            // User is not in project
                            callback(false, 0, null);
                        }
                    }
                }
            });
        }
    });
}

function is_asset_part_of_project(project_id, asset_id, callback) {
    Project.findById(project_id, (err, found_project) => {
        if (err) {
            callback(false, err);
        } else if (!found_project) {
            callback(false, 'No ressource with this ID');
        } else {
            if (found_project.assets.includes(asset_id)) {
                callback(true, null);
            } else {
                callback(false, null);
            }
        }
    });
}

function is_information_part_of_project(project_id, info_id, callback) {
    Information.findById(info_id, (err, found_info) => {
        if (err) {
            callback(false, err);
        } else if (!found_info) {
            callback(false, 'No ressource with this ID');
        } else {
            is_asset_part_of_project(project_id, found_info.liked_asset, (is_linked, err) => {
                if (err) {
                    callback(false, err);
                } else if (!is_linked) {
                    callback(false, null);
                }else {
                    callback(true, null);
                }
            });
        }
    })
}