const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

const db_config = require("../configs/database");
const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");

// Mongoose Models loading
const User = require("../Models/User");
const Pre_Sales = require("../Models/Pre-sales");

router.post('/test-route', (req, res, next) => {
    res.json({
      success: true,
      msg: "Pre-Sales Route Online !"
    })
});

router.post('/new', passport.authenticate('jwt', {session: false}), (req, res, next) =>{
    verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                msg: 'Unex Error'
            });
        } else if (!is_user) {
            res.json({
                success: false,
                msg: 'No user found...'
            });
        } else {
            // Verify that all informations are present:
            if (!req.body.name || !req.body.target || !req.body.lang || !req.body.logo) {
                res.json({
                    success: false,
                    msg: 'Missing parameters...'
                })
            } else {
                const today = new Date();
                const temp_name = __dirname.split('/Routes')[0].split('\\Routes')[0] + '/Pre-Sales/' + req.body.name + '-' + today.getUTCFullYear() + '-' + (today.getUTCMonth()+1) + '-' + today.getUTCDate() + '.docx';
                let new_report = new Pre_Sales({
                    Name: req.body.name,
                    Path: temp_name,
                    Results_id: '',
                    linked_user: found_user._id,
                    Lang: req.body.lang,
                    Target: req.body.target.replace('www.', ''),
                    Logo: req.body.logo,
                    Audited: false,
                    Written: false,
                    Errored: false
                });

                // Save new report in DB:
                Pre_Sales.add(new_report, (err, saved) => {
                    if (err) {
                        console.log(err);
                        res.json({
                            success: false,
                            msg: 'Unex error'
                        });
                    } else {
                        res.json({
                            success: true
                        });
                        Pre_Sales.launch_analyses(saved);
                    }
                })
            }
        }
    })
});

router.post('/get-10', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                msg: 'Unex Error'
            });
        } else if (!is_user) {
            res.json({
                success: false,
                msg: 'No user found...'
            });
        } else {
            Pre_Sales.get_lasts(found_user._id, (err, found_10) => {
                if (err) {
                    console.log(err);
                    res.json({
                        success: false,
                        msg: 'Unex error'
                    });
                } else {
                    manage_reports(found_10, (return_array) => {
                        res.json({
                            success: true,
                            reports: return_array.reverse()
                        })
                    })
                }
            });
        }
    });

    function manage_reports(reports_array, callback) {
        let return_array = [];
        advance_array(0, [], (return_array) => {
          callback(return_array);
        });
    
        function advance_array(i, return_array, callback) {
          if (i >= reports_array.length) {
            console.log('End of reports Management');
            callback(return_array);
          } else {
            console.log('Next report');
            register_report(reports_array[i], return_array, (return_array) => {
              advance_array(i+1, return_array, callback);
            });
          }
        }
    
        function register_report(report, return_array, callback) {
          console.log('Registering report');
          // console.log(report);
          let encrypted_id = encrypter.crypt(JSON.stringify(report._id));
          let registering = {name: report.Name, written: report.Written, error: report.Errored, id: encrypted_id}
          return_array.push(registering);
          callback(return_array);
        }
      }
});

router.get('/download', (req, res, next) => {

    if (req.query.token && req.query.id) {
        verify_user_by_encrypted_token(req.query.token, (err, is_ok, found_user) => {
            if (err) {
                console.log(err);
            } else if (is_ok) {
                // Get report
                let decrypted_id = encrypter.decrypt(req.query.id);
                decrypted_id = decrypted_id.replace('"', '').replace('"', '');
                Pre_Sales.findById(decrypted_id, (err, found_report) => {
                    if (err) {
                        console.log(err);
                    } else if (found_report) {
                        // Verify if user has rights on the offer
                        if (found_report.linked_user == found_user._id) {
                            res.download(found_report.Path, found_report.Name + '.docx')
                        }
                    }
                });
            }
        });
    }

    // res.download('./python-scripts/offer_writer/test.docx', 'report.docx');
});

module.exports = router;

function verify_user_by_encrypted_token(encrypted_token, callback) {
    jwt.verify(encrypted_token, jwt_config.secret, (err, decoded) => {
      if (err) {
        console.log(err);
        callback(err, false);
      } else {
        User.findByUsername(decoded.username, (err, found_user) => {
          if (err) {
            console.log(err);
            callback(err, false);
          } else if (!found_user) {
            callback('', false);
          } else {
            callback('', true, found_user);
          }
        });
      }
    });
  }