const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");
const logs = require("../plugins/log_manager");
const iam = require("../plugins/IAM");

const User = require("../Models/User");

router.post('/test-route', (req, res) => {
  res.json({
    success: true,
    msg: "Admin Route Online !"
  });
  logs.server.info('/admin/test-route', 'access to test-route detected', logs.get_ip(req));
});

router.post('/is-admin', (req, res) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/admin/is-admin > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex Error'
      });
    } else if (!found_user) {
      logs.server.info('/admin/is-admin > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
    } else {
      User.is_user_admin(found_user, (is_admin) => {
        logs.server.info('/admin/is-admin', 'User verified priviledges', logs.get_ip(req), found_user);
        res.json({
          success: true,
          is_admin: is_admin
        });
      });
    }
  });
});

router.post('/validate-user', (req, res) => {
    iam.verify_encrypted_token(req.body.token, (found_user, err) => {
        if (err) {
            logs.server.error('/admin/validate-user > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
            res.json({
              success: false,
              msg: 'Unex Error'
            });
          } else if (!found_user) {
            logs.server.info('/admin/validate-user > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
          } else {
            User.is_user_admin(found_user, (is_admin) => {
                if (is_admin) {
                    if (req.body.user_id && (typeof(req.body.validated) == 'boolean')) {
                        let user_id = encrypter.decrypt(req.body.user_id);
                        user_id = user_id.replace('"', '').replace('"', '');
                        User.findById(user_id, (err, found_candidate) => {
                            User.validate_user(found_user, found_candidate, req.body.validated, (err) => {
                                if (err) {
                                    logs.server.error('/admin/validate-user > User.validate_user', [String(req.body.token)], err, logs.get_ip(req));
                                    res.json({
                                    success: false,
                                    msg: 'Unex Error'
                                    });
                                } else {
                                    logs.server.info('/admin/validate-user', 'User {' + found_candidate._id + ' - ' + found_candidate.Username + '} reviewed by admin with state {' + String(req.body.validated) + '}', logs.get_ip(req), found_user);
                                    res.json({
                                        success: true,
                                        msg: 'User reviewed'
                                    });
                                }
                            });
                        });
                    } else {
                        logs.server.info('/admin/validate-user', 'Missing parameters', logs.get_ip(req), found_user);
                    }
                } else {
                    logs.server.alert('/admin/validate-user', 'User tried to validate a user without admin rights', logs.get_ip(req), found_user);
                }
            });
          }
    });
});

router.post('/get-users-to-validate', (req, res) => {
    iam.verify_encrypted_token(req.body.token, (found_user, err) => {
        if (err) {
            logs.server.error('/admin/get-users-to-validate > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
            res.json({
              success: false,
              msg: 'Unex Error'
            });
          } else if (!found_user) {
            logs.server.info('/admin/get-users-to-validate > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
          } else {
            User.is_user_admin(found_user, (is_admin) => {
                if (is_admin) {
                    User.get_all_user_to_validate((err, users_to_validate) => {
                        if (err) {
                            logs.server.error('/admin/get-users-to-validate > User.get_all_user_to_validate', [String(req.body.token)], err, logs.get_ip(req));
                            res.json({
                            success: false,
                            msg: 'Unex Error'
                            });
                        } else {
                            manage_users(users_to_validate, (treated_array) => {
                                logs.server.info('/admin/get-users-to-validate', 'List of users to validate sended to admin', logs.get_ip(req), found_user);
                                res.json({
                                    success: true,
                                    users: treated_array
                                });
                            });
                        }
                    });
                } else {
                    logs.server.alert('/admin/get-users-to-validate', 'User tried access route without admin rights', logs.get_ip(req), found_user);
                }
            });
          }
    });

    function manage_users(users_array, callback) {

        advance_array(users_array, 0, [], callback);

        function advance_array(users_array, i, return_array, callback) {
            if (i >= users_array.length) {
                callback(return_array)
            } else {
                register_user(users_array[i], return_array, (return_array) => {
                    advance_array(users_array, i+1, return_array, callback);
                });
            }
        }

        function register_user(user, return_array, callback) {
            let encrypted_id = encrypter.crypt(JSON.stringify(user._id));
            let registering = {id: encrypted_id, username: user.Username, surname: user.Surname, name: user.Name, email: user.Email, post: user.Post, phone: user.Phone};
            return_array.push(registering);
            callback(return_array);
        }
    }
});


module.exports = router;
