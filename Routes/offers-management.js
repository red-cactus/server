const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

const db_config = require("../configs/database");
const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");

const spawn = require('child_process').spawn;

// Mongoose Models loading
const User = require("../Models/User");
const Offer = require("../Models/Offer");

router.post('/test-route', (req, res, next) => {
    res.json({
      success: true,
      msg: "User Route Online !"
    })
});

router.post('/new', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    // Need: offer_name, client_name, lang, offer_type, content, client_logo
    verify_user_by_encrypted_token(req.body.token, (err, is_present, found_user) => {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                msg: 'Unex error'
            });
        } else if (!is_present) {
            res.json({
                success: false,
                msg: 'Unauthorized'
            });
        } else {
            // Verify if all informations are present:
            if (!req.body.offer_name || !req.body.client_name || !req.body.lang || !req.body.offer_type || !req.body.content || !req.body.client_logo) {
                res.json({
                    success: false,
                    msg: 'Missing informations'
                })
            } else {
                const today = new Date();
                const temp_name = __dirname.split('/Routes')[0].split('\\Routes')[0] + '/Offers/' + req.body.client_name + '-' + today.getUTCFullYear() + '-' + (today.getUTCMonth()+1) + '-' + today.getUTCDate() + '.docx';
                // Create new offer object:
                let new_offer = new Offer({
                    Name: req.body.offer_name, // Client name + date
                    Path: temp_name,
                    linked_user: found_user._id,
                    Lang: req.body.lang,
                    Offer_type: req.body.offer_type, // 1-minifed, 2-full
                    Content: req.body.content, // ['pentest', 'phishing', etc]
                    Client_logo: req.body.client_logo, // Encrypted base64 logo
                    Client_company: req.body.client_company,
                    Client_name: req.body.client_name,
                    Written: false
                });

                // Save new offer:
                Offer.add(new_offer, (err, saved) => {
                    if (err) {
                        console.log(err);
                        res.json({
                            success: false,
                            msg: 'Unex error'
                        });
                    } else {
                        // Create Offer
                        let pyprocess = spawn('python3', ["./python-scripts/offer_writer/offer_writer.py", saved._id, found_user._id])

                        pyprocess.stdout.on('data', (data) => {
                            console.log('Ending Offer Writing');
                            saved.Written = true;
                            Offer.add(saved, (err, new_saved) => {
                                res.json({
                                    success: true,
                                    msg: 'New offer created'
                                });
                            });
                        });

                        pyprocess.stderr.on('data', (data) => {
                            console.log('Error: ' + data)
                        });
                    }
                });
            }
        }
    })
});

router.post('/get-user-offers', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    verify_user_by_encrypted_token(req.body.token, (err, user_ok, found_user) => {
        if (err) {
            res.json({
                success: false,
                msg: 'Unex error'
            });
        } else if (!user_ok) {
            res.json({
                success: false,
                msg: 'Bad user...'
            });
        } else {
            Offer.get_all_user_offers(found_user._id, (err, found_offers) => {
                if (err) {
                    res.json({
                        success: false,
                        msg: 'Unex error'
                    });
                } else {
                    manage_offers(found_offers, (returning_array) => {
                        if (returning_array.length > 10) {
                            returning_array = returning_array.slice(-10)
                        }
                        res.json({
                            success: true,
                            offers: returning_array.reverse()
                        });
                    });
                }
            });
        }
    });

    function manage_offers(offers_array, callback) {
        let return_array = [];
        advance_array(0, [], (return_array) => {
          callback(return_array);
        });
    
        function advance_array(i, return_array, callback) {
          if (i >= offers_array.length) {
            console.log('End of Offers Management');
            callback(return_array);
          } else {
            console.log('Next Offer');
            register_offer(offers_array[i], return_array, (return_array) => {
              advance_array(i+1, return_array, callback);
            });
          }
        }
    
        function register_offer(offer, return_array, callback) {
          console.log('Registering Offer');
          // console.log(offer);
          let encrypted_id = encrypter.crypt(JSON.stringify(offer._id));
          let registering = {name: offer.Name, id: encrypted_id}
          return_array.push(registering);
          callback(return_array);
        }
      }
});

router.get('/download', (req, res, next) => {

    if (req.query.token && req.query.id) {
        verify_user_by_encrypted_token(req.query.token, (err, is_ok, found_user) => {
            if (err) {
                console.log(err);
            } else if (is_ok) {
                // Get offer
                let decrypted_id = encrypter.decrypt(req.query.id);
                decrypted_id = decrypted_id.replace('"', '').replace('"', '');
                Offer.findById(decrypted_id, (err, found_offer) => {
                    if (err) {
                        console.log(err);
                    } else if (found_offer) {
                        // Verify if user has rights on the offer
                        if (found_offer.linked_user == found_user._id) {
                            res.download(found_offer.Path, found_offer.Name + '.docx')
                        }
                    }
                });
            }
        });
    }

    // res.download('./python-scripts/offer_writer/test.docx', 'report.docx');
});


module.exports = router;

function verify_user_by_encrypted_token(encrypted_token, callback) {
    jwt.verify(encrypted_token, jwt_config.secret, (err, decoded) => {
      if (err) {
        console.log(err);
        callback(err, false);
      } else {
        User.findByUsername(decoded.username, (err, found_user) => {
          if (err) {
            console.log(err);
            callback(err, false);
          } else if (!found_user) {
            callback('', false);
          } else {
            callback('', true, found_user);
          }
        });
      }
    });
  }