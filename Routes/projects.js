const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");
const logs = require('../plugins/log_manager');
const iam = require("../plugins/IAM");

const User = require("../Models/User");
const Project = require("../Models/Project");
const Asset = require("../Models/Asset");
const Information = require("../Models/Information");

router.post('/test-route', (req, res) => {
  logs.server.info('/project/test-route', 'ACCESS TO TEST ROUTE DETECTED', logs.get_ip(req))
  res.json({
    success: true,
    msg: "Project Route Online !"
  });
});

router.post('/create', passport.authenticate('jwt', {session: false}), (req, res) => {

  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/create > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex Error'
      });
    } else if (!found_user) {
      logs.server.info('/project/create > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
    } else {
      if (req.body.image && req.body.name && req.body.description) {
        const newProject = new Project({
          team: {
            leader: found_user._id,
            members: []
          },
          name: req.body.name,
          image: req.body.image,
          assets: [],
          description: req.body.description,
          linked_users: [found_user._id],
          creation: Date.now()
        });
  
        Project.add(newProject, (err, saved) => {
          if (err) {
            logs.server.error('/project/create > Project.add', [String(newProject.name), String(newProject.description)], err, logs.get_ip(req), found_user);
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else {
            User.addProject(found_user, saved._id, (err) => {
              if (err) {
                logs.server.error('/project/create > User.addProject', [String(found_user.Username), String(saved._id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                })
              } else {
                logs.server.info('/project/create', 'Project {' + String(saved.name) + '} created', logs.get_ip(req), found_user);
                res.json({
                  success: true,
                  msg: 'Project Created'
                });
              }
            })
          }
        })
      } else {
        logs.server.alert('/project/create', 'Missing Parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/get-user-projects', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-user-projects > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.info('/project/get-user-projects > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
    } else {
      get_projects(found_user.RelatedProjects, (found_projects) => {
        logs.server.info('/project/get-user-projects', 'all projects linked to user sent', logs.get_ip(req), found_user);
        res.json({
          success: true,
          projects: found_projects
        });
      });
    }
  });

  function get_projects(project_array, callback) {
    advance_array(project_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(project_array, i, return_array, callback) {
      if (i >= project_array.length) {
        callback(return_array);
      } else {
        get_project(project_array, i, return_array, (project_array, i, return_array) => {
          advance_array(project_array, i+1, return_array, callback)
        })
      }
    }

    function get_project(project_array, i, return_array, callback) {
      // eslint-disable-next-line security/detect-object-injection
      Project.findById(project_array[i], (err, project_found) => {
        if (err) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.error('/project/get-user-projects > get_projects > get_project', [String(project_array[i])], err, logs.get_ip(req));
        } else if (!project_found) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.info('/project/get-user-projects > get_projects > get_project', 'No project found with id {' + String(project_array[i]) + '}', logs.get_ip(req));
          callback(project_array, i, return_array);
        } else {
          register_project(project_found, return_array, (return_array) => {
            callback(project_array, i, return_array);
          });
        }
      });
    }

    function register_project(project, return_array, callback) {
      let encrypted_id = encrypter.crypt(JSON.stringify(project._id))
      let registering = {name: project.name, description: project.description, id: encrypted_id}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/get-project-details', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-user-projects > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.info('/project/get-project-details > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        decrypted_id = decrypted_id.replace('"', '').replace('"', '')
        iam.has_rights_on_project(found_user._id, decrypted_id, null, null, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-user-projects > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(decrypted_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-user-projects > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-project-details', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                logs.server.error('/project/get-project-details > Project.findById', [String(decrypted_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                logs.server.alert('/project/get-project-details > Project.findById', 'No project found with given ID {' + String(decrypted_id) + '}, the project must have been deleted while processing', logs.get_ip(req), found_user);
              } else {
                let return_project = {name: found_project.name, description: found_project.description};
                logs.server.info('/project/get-project-details', 'Details of project {' + String(decrypted_id) + '} sent', logs.get_ip(req), found_user);
                res.json({
                  success: true,
                  project: return_project
                });
              }
            });
          }
        });
      } else {
        logs.server.alert('/project/get-project-details', 'No project_id parameter entered', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/edit-project', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/edit-project > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.info('/project/edit-project > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req));
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        decrypted_id = decrypted_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, decrypted_id, null, null, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/edit-project > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(decrypted_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/edit-project > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/edit-project', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                logs.server.error('/project/edit-project > Project.findById', [String(decrypted_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                logs.server.alert('/project/edit-project > Project.findById', 'No project found with given ID {' + String(decrypted_id) + '}, the project must have been deleted while processing', logs.get_ip(req), found_user);
              } else {
                if (req.body.name) {
                  found_project.name = req.body.name;
                }
                if (req.body.description) {
                  found_project.description = req.body.description;
                }
                if (req.body.image) {
                  found_project.image = req.body.image;
                }

                found_project.save((err, saved) => {
                  if (err) {
                    logs.server.error('/project/edit-project > found_project.save', [], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else if (saved) {
                    logs.server.info('/project/edit-project', 'user modified project {' + found_project._id + '}', logs.get_ip(req), found_user);
                    res.json({
                      success: true
                    });
                  }
                })
              }
            });
          }
        });
      } else {
        logs.server.info('/project/edit-project', 'No project_id parameter entered', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/delete-project', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/delete-project > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/delete-project > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, String(project_id), null, null, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/edit-project > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/edit-project > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/delete-project', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            if (rights == 2) {
              logs.server.info('/project/delete-project', 'project {' + project_id + '} deleted', logs.get_ip(req), found_user);
              delete_project(found_user._id, project_id, () => {
                res.json({
                  success: true,
                  msg: 'Project deleted'
                });
              });
            } else {
              res.json({
                success: false,
                msg: 'No rights for deleting project'
              });
            }
          }
        });
      } else {
        logs.server.info('/project/delete-project', 'No project_id parameter entered', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/create-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  // Find relative Project:
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/create-asset > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/create-asset > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.a_name && req.body.type && req.body.asset_desc) {
        let id = encrypter.decrypt(req.body.project_id);
        id = id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, id, null, null, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/create-asset > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/create-asset > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/create-asset', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Project.findById(id, (err, found_project) => {
              if (err) {
                logs.server.error('/project/create-asset > Project.findById', [String(id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                let newAsset = new Asset({
                  name: req.body.a_name,
                  project_id: found_project._id,
                  type: req.body.type,
                  asset_description: req.body.asset_desc,
                  informations_linked: [],
                  creation: Date.now()
                });
                Asset.add(newAsset, (err, saved) => {
                  if (err) {
                    logs.server.error('/project/create-asset > Asset.add', [String(newAsset(newAsset.name))], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    Project.add_asset(found_project, saved._id, (err, saved_project) => {
                      if (err) {
                        logs.server.error('/project/create-asset > Project.add_asset', [String(found_project.name)], logs.get_ip(req), found_user);
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else {
                        logs.server.info('/project/create-asset', 'asset created {' + saved.name + '-' + saved._id + '} on project {' + saved_project.name + '-' + saved._id + '}', logs.get_ip(req), found_user);
                        let asset_id = encrypter.crypt(JSON.stringify(saved._id));
                        res.json({
                          success: true,
                          asset_id: asset_id,
                          msg: 'Asset Registered'
                        });
                      }
                    })
                  }
                })
              }
            });
          }
        });
      } else {
        logs.server.info('/project/create-asset', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/get-project-assets', passport.authenticate('jwt', { session: false }), (req, res, next) => {

  // Receiving project encrypted id
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-project-assets > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/get-project-assets > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        decrypted_id = decrypted_id.replace('"', '').replace('"', '');

        iam.has_rights_on_project(found_user._id, String(decrypted_id), null, null, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-project-assets > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(decrypted_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-project-assets > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-project-assets', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                logs.server.error('/project/get-project-assets > Project.findById', [String(decrypted_id)], err, logs.get_ip(req), found_user);
              } else {
                get_assets(found_project.assets, (found_assets) => {
                  logs.server.info('/project/get-project-assets', 'All assets sent from project {' + found_project.name + '-' + found_project._id + '}', logs.get_ip(req), found_user);
                  res.json({
                    success: true,
                    assets: found_assets
                  });
                });
              }
            });
          }
        });
      } else {
        logs.server.info('/project/get-project-assets', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });

  function get_assets(project_array, callback) {
    advance_array(project_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(asset_array, i, return_array, callback) {
      if (i >= asset_array.length) {
        callback(return_array);
      } else {
        get_asset(asset_array, i, return_array, (project_array, i, return_array) => {
          advance_array(asset_array, i+1, return_array, callback)
        });
      }
    }

    function get_asset(asset_array, i, return_array, callback) {
      // eslint-disable-next-line security/detect-object-injection
      Asset.findById(asset_array[i], (err, asset_found) => {
        if (err) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.error('/project/get-project-assets > Asset.findById', [String(asset_array[i])], err, logs.get_ip(req));
        } else if (!asset_found) {
          callback(asset_array, i, return_array);
        } else {
          register_asset(asset_found, return_array, (return_array) => {
            callback(asset_array, i, return_array);
          });
        }
      });
    }

    function register_asset(asset, return_array, callback) {
      let encrypted_id = encrypter.crypt(JSON.stringify(asset._id))
      let registering = {id: encrypted_id, name: asset.name, type: asset.type, description: asset.asset_description}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/link-asset-to-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/link-asset-to-asset > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/link-asset-to-asset > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.link_asset_id && req.body.link_to_asset_id && req.body.project_id) {
        let link_asset_id = encrypter.decrypt(req.body.link_asset_id);
        link_asset_id = link_asset_id.replace('"', '').replace('"', '');
        let asset_to_id = encrypter.decrypt(req.body.link_to_asset_id);
        asset_to_id = asset_to_id.replace('"', '').replace('"', '');
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, link_asset_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/link-asset-to-asset > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + '-' + String(link_asset_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/link-asset-to-asset > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-project-assets', 'User is not linked to project {' + String(project_id) + '}', logs.get_ip(req), found_user);
          } else {
            iam.has_rights_on_project(found_user._id, project_id, 1, asset_to_id, (is_linked, rights2, err) => {
              if (err) {
                if (err == 'No ressource with this ID') {
                  logs.server.alert('/project/link-asset-to-asset > iam.has_right_on_project (2)', 'Unlogical : No ressource with this id {' + String(project_id) + '-' + asset_to_id + '}', logs.get_ip(req));
                } else {
                  logs.server.error('/project/link-asset-to-asset > iam.has_right_on_project (2)', [String(req.body.token)], err, logs.get_ip(req));
                }
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                logs.server.alert('/project/get-project-assets (2)', 'User is not linked to project {' + String(project_id) + '}', logs.get_ip(req), found_user);
              } else {
                if (rights == 2 || rights2 == 2) {
                  Asset.findById(link_asset_id, (err, link_asset) => {
                    if (err) {
                      logs.server.error('/project/link-asset-to-asset > Asset.findById', [String(link_asset_id)], err, logs.get_ip(req), found_user);
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else if (!link_asset) {
                      logs.server.alert('/project/link-asset-to-asset > Asset.findById', 'No asset found with given ID {' + String(link_asset_id) + '}, the asset must have been deleted while processing', logs.get_ip(req), found_user);
                    } else {
                      Asset.findById(asset_to_id, (err, link_to_asset) => {
                        if (err) {
                          logs.server.error('/project/link-asset-to-asset > Asset.findById (2)', [String(asset_to_id)], err, logs.get_ip(req), found_user);
                        } else if (!link_to_asset) {
                          logs.server.alert('/project/link-asset-to-asset > Asset.findById', 'No asset found with given ID {' + String(asset_to_id) + '}, the asset must have been deleted while processing', logs.get_ip(req), found_user);
                        } else {
                          if (!link_to_asset.linked_asset) {
                            link_to_asset.linked_asset = [];
                          }
                          if (!link_asset.linked_asset) {
                            link_asset.linked_asset = [];
                          }
      
                          link_to_asset.linked_asset.push(link_asset._id);
                          link_asset.linked_asset.push(link_to_asset._id);
      
                          link_asset.save((err) => {
                            if (err) {
                              logs.server.error('/project/link-asset-to-asset > link_asset.save', [], err, logs.get_ip(req), found_user);
                              res.json({
                                success: false,
                                msg: 'Unex error'
                              });
                            } else {
                              link_to_asset.save((err) => {
                                if (err) {
                                  logs.server.error('/project/link-asset-to-asset', [], err, logs.get_ip(req), found_user);
                                  res.json({
                                    success: false,
                                    msg: 'Unex error'
                                  });
                                } else {
                                  logs.server.info('/project/link-asset-to-asset', 'Assets linked with success {' + link_asset._id + '-' + link_to_asset._id + '}', logs.get_ip(req), found_user);
                                  res.json({
                                    success: true
                                  });
                                }
                              });
                            }
                          });
                        }
                      })
                    }
                  });
                } else {
                  logs.server.alert('/project/link-asset-to-asset', 'User does not have persmission to link assets {' + link_asset_id +  '-' + asset_to_id + '}', logs.get_ip(req), found_user);
                }
              }
            });
          }
        });
      } else {
        logs.server.info('/project/create-asset', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/unlink-assets', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/unlink-assets > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/unlink-assets > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.unlink_asset_1 && req.body.unlink_asset_2 && req.body.project_id) {
        let asset_1_id = encrypter.decrypt(req.body.unlink_asset_1);
        asset_1_id = asset_1_id.replace('"', '').replace('"', '');
        let asset_2_id = encrypter.decrypt(req.body.unlink_asset_2);
        asset_2_id = asset_2_id.replace('"', '').replace('"', '');
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, asset_1_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/unlink-assets > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + '-' + String(asset_1_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/unlink-assets > iam.has_right_on_project', [String(found_user._id), String(asset_1_id), String(asset_2_id)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/unlink-assets', 'User is not linked to project {' + String(project_id) + '}', logs.get_ip(req), found_user);
          } else {
            iam.has_rights_on_project(found_user._id, project_id, 1, asset_2_id, (is_linked, rights_2, err) => {
              if (err) {
                if (err == 'No ressource with this ID') {
                  logs.server.alert('/project/unlink-assets > iam.has_right_on_project (2)', 'Unlogical : No ressource with this id {' + String(req.body.token) + '}', logs.get_ip(req));
                } else {
                  logs.server.error('/project/unlink-assets > iam.has_right_on_project (2)', [String(req.body.token)], err, logs.get_ip(req));
                }
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                logs.server.alert('/project/unlink-assets (2)', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
              } else {
                if (rights == 2 ||rights_2 == 2) {
                  Asset.unlink_asset(asset_1_id, asset_2_id, (err, worked) => {
                    if (err) {
                      logs.server.error('/project/unlink-asset > Asset.unlink_asset', [String(asset_1_id), String(asset_2_id)], err, logs.get_ip(req), found_user);
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else if (worked) {
                      Asset.unlink_asset(asset_2_id, asset_1_id, (err, worked) => {
                        if (err) {
                          logs.server.error('/project/unlink-asset > Asset.unlink_asset (2)', [String(asset_2_id), String(asset_1_id)], err, logs.get_ip(req), found_user);
                        } else if (worked) {
                          logs.server.info('/project/unlink-assets', 'Success unlinking of assets {' + String(asset_1_id) + ' - ' + String(asset_2_id) + '}', logs.get_ip(req), found_user);
                          res.json({
                            success: true
                          });
                        }
                      });
                    }
                  });
                } else {
                  logs.server.alert('/project/link-asset-to-asset', 'User does not have persmission to unlink assets {' + asset_1_id +  '-' + asset_2_id + '}', logs.get_ip(req), found_user);
                }
              }
            });
          }
        });
      } else {
        logs.server.info('/project/create-asset', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/delete-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/delete-asset > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/delete-asset > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.asset_id && req.body.project_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, asset_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/delete-asset > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(req.body.token) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/delete-asset > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/delete-asset', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            logs.server.info('/project/delete-asset', 'Asset {' + asset_id + '} deleted', logs.get_ip(req), found_user);
            delete_asset(project_id, asset_id, () => {
              res.json({
                success: true,
                msg: 'Asset deleted'
              });
            });
          }
        });
      } else {
        logs.server.info('/project/delete-asset', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/get-asset-details', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-asset-details > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/get-asset-details > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, asset_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-asset-details > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(asset_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-asset-details > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-asset-details', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                logs.server.error('/project/get-asset-details > Asset.findById', [String(asset_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                // Research all other assets:
                get_linked_assets(found_asset.linked_asset, (linked_asset_array) => {
                  let return_asset = {name: found_asset.name, description: found_asset.asset_description, type: found_asset.type, linked_asset: linked_asset_array}
                  logs.server.info('/project/get-asset-details', 'Asset details sent {' + found_asset._id + ' - ' + found_asset.name + '}');
                  res.json({
                    success:true,
                    asset: return_asset
                  });
                });
              }
            });
          }
        });
      } else {
        logs.server.info('/project/get-asset-details', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });

  function get_linked_assets(assets_array, callback) {
    advance_array(assets_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(assets_array, i, return_array, callback) {
      if (i >= assets_array.length) {
        callback(return_array);
      } else {
        get_asset(assets_array, i, return_array, (assets_array, i, return_array) => {
          advance_array(assets_array, i+1, return_array, callback)
        })
      }
    }

    function get_asset(assets_array, i, return_array, callback) {
      // eslint-disable-next-line security/detect-object-injection
      Asset.findById(assets_array[i], (err, asset_found) => {
        if (err) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.error('/project/get-asset-details > get_linked_asset > get_asset > Asset.findById', [String(assets_array[i])], err, logs.get_ip(req));
        } else if (!asset_found) {
          callback(assets_array, i, return_array);
        } else {
          register_asset(asset_found, return_array, (return_array) => {
            callback(assets_array, i, return_array);
          });
        }
      });
    }

    function register_asset(asset, return_array, callback) {
      let encrypted_id = encrypter.crypt(JSON.stringify(asset._id))
      let registering = {id: encrypted_id, name: asset.name, 
      description: asset.asset_description, type: asset.type}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/edit-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/edit-asset > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/edit-asset > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.asset_id && req.body.project_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, asset_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/edit-asset > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(asset_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/edit-asset > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/edit-asset', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                logs.server.error('/project/edit-asset > Asset.findById', [String(asset_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                if (req.body.name) {
                  found_asset.name = req.body.name;
                }
                if (req.body.type) {
                  found_asset.type = req.body.type;
                }
                if (req.body.description) {
                  found_asset.asset_description = req.body.description;
                }

                found_asset.save((err) => {
                  if (err) {
                    logs.server.error('/project/edit-asset > found_asset.save', [], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    logs.server.info('/project/edit-asset', 'Asset {' + String(asset_id) + '} modified');
                    res.json({
                      success: true
                    });
                  }
                })
              }
            })
          }
        });
      } else {
        logs.server.info('/project/edit-asset', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/add-information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/add-information > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/add-information > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.asset_id && req.body.title && req.body.description && req.body.type) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, asset_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/add-information > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(asset_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/add-information > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/add-information', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                logs.server.error('/project/add-information > Asset.findById', [String(asset_id)], err, logs.get_ip(req), found_user);
                res.json({
                  succes: false,
                  msg: 'Unex error'
                });
              } else {
                let newInfo = new Information({
                  title: req.body.title,
                  type: req.body.type,
                  description: req.body.description,
                  tool_output: '',
                  cvss: '',
                  poc: [],
                  linked_asset: found_asset._id,
                  creation: Date.now()
                });

                if (req.body.poc) {
                  newInfo.poc = req.body.poc;
                }
                if (req.body.tool_output) {
                  newInfo.tool_output = req.body.tool_output;
                }
                if (req.body.cvss) {
                  newInfo.cvss = req.body.cvss;
                }

                Information.add(newInfo, (err, saved) => {
                  if (err) {
                    logs.server.error('/project/add-information > Information.add', [String(newInfo.title)], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    Asset.add_info(found_asset, saved._id, (err, saved_asset) => {
                      if (err) {
                        logs.server.error('/project/add-information > Asset.add_info', [String(found_asset._id), String(saved._id)], err, logs.get_ip(req), found_user);
                      } else {
                        logs.server.info('/project/add-information', 'Information created in project {' + String(project_id) + '} in asset {' + String(asset_id) + ' - ' + String(found_asset.name) + '}', logs.get_ip(req), found_user);
                        res.json({
                          success: true,
                          msg: 'Info registered'
                        })
                      }
                    })
                  }
                })
              }
            });
          }
        });
      } else {
        logs.server.info('/project/add-information', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/get-asset-informations', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-asset-informations > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/get-asset-informations > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.asset_id) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 1, asset_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-asset-informations > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(asset_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-asset-informations > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-asset-informations', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                logs.server.error('/project/get-asset-information > Asset.findById', [String(asset_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                get_informations(found_asset.informations_linked, (info_array) => {
                  info_array.sort(function(info1, info2) {
                    return info2.cvss - info1.cvss
                  });
                  logs.server.info('/project/get-asset-informations', 'All informations transfered to front', logs.get_ip(req), found_user);
                  res.json({
                    success: true,
                    infos: info_array
                  });
                })
              }
            })
          }
        });
      } else {
        logs.server.info('/project/get-asset-informations', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });


  function get_informations(information_array, callback) {
    advance_array(information_array, 0, [], (return_array) => {
      callback(return_array)
    });

    function advance_array(information_array, i, return_array, callback) {
      if (i >= information_array.length) {
        callback(return_array);
      } else {
        get_information(information_array, i, return_array, (information_array, i, return_array) => {
          advance_array(information_array, i+1, return_array, callback)
        })
      }
    }

    function get_information(information_array, i, return_array, callback) {
      // eslint-disable-next-line security/detect-object-injection
      Information.findById(information_array[i], (err, info_found) => {
        if (err) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.error('/project/get-asset-informations > get_informations > get_information > Information.findById', [String(information_array[i])], err);
        } else if (!info_found) {
          callback(information_array, i, return_array);
        } else {
          register_project(info_found, return_array, (return_array) => {
            callback(information_array, i, return_array);
          });
        }
      });
    }

    function register_project(info, return_array, callback) {
      let encrypted_id = encrypter.crypt(JSON.stringify(info._id))
      let registering = {title: info.title, type: info.type, tool_output: info.tool_output, cvss: info.cvss, description: info.description, id: encrypted_id}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/link-informations', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/link-informations > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/link-informations > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.info1_id && req.body.info2_id && req.body.relation) {
        // Info 1 decryption
        let info1_id = encrypter.decrypt(req.body.info1_id);
        info1_id = info1_id.replace('"', '').replace('"', '');
        // Info 2 decryption
        let info2_id = encrypter.decrypt(req.body.info2_id);
        info2_id = info2_id.replace('"', '').replace('"', '');
        // Project id decryption
        let pj_id = encrypter.decrypt(req.body.project_id);
        pj_id = pj_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, pj_id, 2, info1_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/link-informations > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(pj_id) + ' - ' + String(info1_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/link-informations > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/link-informations', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            iam.has_rights_on_project(found_user._id, pj_id, 2, info2_id, (is_linked, rights_2, err) => {
              if (err) {
                if (err == 'No ressource with this ID') {
                  logs.server.alert('/project/link-informations > iam.has_right_on_project (2)', 'Unlogical : No ressource with this id {' + String(pj_id) + ' - ' + String(info2_id) + '}', logs.get_ip(req));
                } else {
                  logs.server.error('/project/link-informations > iam.has_right_on_project (2)', [String(req.body.token)], err, logs.get_ip(req));
                }
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                logs.server.alert('/project/link-informations (2)', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
              } else {
                Information.findById(info1_id, (err, found_info1) => {
                  if (err) {
                    logs.server.error('/project/link-informations > Information.findById', [String(info1_id)], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    Information.findById(info2_id, (err, found_info2) => {
                      if (err) {
                        logs.server.error('/project/link-informations > Information.findById (2)', [String(info1_id)], err, logs.get_ip(req), found_user);
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else {
                        if (!found_info1.linked_informations) {
                          found_info1.linked_informations = [];
                        }
                        if (!found_info2.linked_informations) {
                          found_info2.linked_informations = [];
                        }
        
                        // Define information relations:
                        if (req.body.relation == 1) {
                          // Worsen the vulnerability
                          found_info2.linked_informations.push({id: found_info1._id, relation: 2});
                          found_info1.linked_informations.push({id: found_info2._id, relation: 1});
                        } else if (req.body.relation == 2) {
                          // Worsened by the vulnerability
                          found_info2.linked_informations.push({id: found_info1._id, relation: 1});
                          found_info1.linked_informations.push({id: found_info2._id, relation: 2});
                        } else if (req.body.relation == 3) {
                          // Serucise the vulnerability
                          found_info2.linked_informations.push({id: found_info1._id, relation: 4});
                          found_info1.linked_informations.push({id: found_info2._id, relation: 3});
                        } else if (req.body.relation == 4) {
                          // Securised by the security
                          found_info2.linked_informations.push({id: found_info1._id, relation: 3});
                          found_info1.linked_informations.push({id: found_info2._id, relation: 4});
                        } else {
                          // New information on the information
                          found_info2.linked_informations.push({id: found_info1._id, relation: 5});
                          found_info1.linked_informations.push({id: found_info2._id, relation: 5});
                        }
        
                        found_info1.save((err) => {
                          if (err) {
                            logs.server.error('/project/link-informations > found_info1.save', [], err, logs.get_ip(req), found_user);
                            res.json({
                              success: false,
                              msg: 'Unex error'
                            });
                          } else {
                            found_info2.save((err) => {
                              if (err) {
                                logs.server.error('/project/link-informations > found_info2.save', [], err, logs.get_ip(req), found_user);
                                res.json({
                                  success: false,
                                  msg: 'Unex error'
                                });
                              } else {
                                if (rights == 2 || rights_2 == 2) {
                                  logs.server.info('/project/link-informations', 'Informations linked by user {' + found_info1._id + ' - ' + found_info2._id + '} with relation : {' + String(req.body.relation) + '}');
                                  res.json({
                                    success: true
                                  });
                                } else {
                                  logs.server.info('/project/link-informations', 'User does not have rights for linking {' + found_info1._id + ' - ' + found_info2._id + '}');
                                  res.json({
                                    success: false,
                                    msg: 'Not enough privileges'
                                  });
                                }
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      } else {
        logs.server.info('/project/link-informations', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/unlink-information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/unlink-information > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/unlink-information > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.info1_id && req.body.info2_id && req.body.project_id) {
        let info1_id = encrypter.decrypt(req.body.info1_id);
        info1_id = info1_id.replace('"', '').replace('"', '');
        let info2_id = encrypter.decrypt(req.body.info2_id);
        info2_id = info2_id.replace('"', '').replace('"', '');
        let pj_id = encrypter.decrypt(req.body.project_id);
        pj_id = pj_id.replace('"', '').replace('"', '');

        iam.has_rights_on_project(found_user._id, pj_id, 2, info1_id, (is_linked, rights, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/unlink-information > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(pj_id) + ' - ' + String(info2_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/unlink-information > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/unlink-information', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            iam.has_rights_on_project(found_user._id, pj_id, 2, info2_id, (is_linked, rights2, err) => {
              if (err) {
                if (err == 'No ressource with this ID') {
                  logs.server.alert('/project/unlink-information > iam.has_right_on_project (2)', 'Unlogical : No ressource with this id {' + String(pj_id) + ' - ' + String(info2_id) + '}', logs.get_ip(req));
                } else {
                  logs.server.error('/project/unlink-information > iam.has_right_on_project (2)', [String(req.body.token)], err, logs.get_ip(req));
                }
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                logs.server.alert('/project/unlink-information (2)', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
              } else {
                if (rights == 2 || rights2 == 2) {
                  Information.unlink_informations(info1_id, info2_id, (err) => {
                    if (err) {
                      logs.server.error('/project/unlink-information > unlink_informations', [String(info1_id), String(info2_id)], err, logs.get_ip(req), found_user);
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else {
                      Information.unlink_informations(info2_id, info1_id, (err) => {
                        if (err) {
                          logs.server.error('/project/unlink-information > unlink_informations (2)', [String(info2_id), String(info1_id)], err, logs.get_ip(req), found_user);
                          res.json({
                            success: false,
                            msg: 'Unex error'
                          });
                        } else {
                          logs.server.info('/project/unlink-information', 'Sucess unlinking of informations {' + String(info1_id) + ' - ' + String(info2_id) + '}');
                          res.json({
                            success: true
                          });
                        }
                      });
                    }
                  });
                } else {
                  logs.server.info('/project/unlink-information', 'User does not have rights for linking {' + String(info1_id) + ' - ' + String(info2_id) + '}');
                  res.json({
                    success: false,
                    msg: 'Not enough privileges'
                  });
                }
              }
            });
          }
        });
      } else {
        logs.server.info('/project/unlink-information', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/delete-information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/delete-information > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/delete-information > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.info_id) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, String(project_id), 2, String(info_id), (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/delete-information > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(pj_id) + ' - ' + String(info2_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/delete-information > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/delete-information', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            if (right == 2) {
              Information.findById(String(info_id), (err, found_info) => {
                if (err) {
                  logs.server.error('/project/delete-information > Inforamtion.delete', [String(info_id)], err, logs.get_ip(req), found_user);
                  res.json({
                    success: false,
                    msg: 'Unex error'
                  });
                } else if (!found_info) {
                  logs.server.alert('/project/delete-information > Information.findById', 'Incoherence: No information found eventhough it existed before deletion', logs.get_ip(req), found_user);
                } else {
                  // Unlink from all informations
                  for (let j=0;j<found_info.linked_informations.length;j++) {
                    // eslint-disable-next-line security/detect-object-injection
                    Information.unlink_informations(found_info.linked_informations[j].id, found_info._id, (err, deleted) => {});
                  }

                  Information.delete(info_id, (err) => {
                    if (err) {
                      logs.server.error('/project/delete-information > Information.delete', [String(info_id)], err, logs.get_ip(req), found_user);
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else {
                      logs.server.info('/project/delete-information', 'Information deleted {' + found_info.title + '}', logs.get_ip(req), found_user);
                      res.json({
                        success: true,
                        msg: 'Information deleted'
                      });
                    }
                  });
                }
              });
            } else {
              logs.server.info('/project/unlink-information', 'User does not have rights for deletion of information {' + String(info_id) + '}');
              res.json({
                success: false,
                msg: 'Not enough privileges'
              });
            }
          }
        });
      } else {
        logs.server.info('/project/delete_information', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/edit-information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/edit-information > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/edit-information > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.info_id && req.body.project_id) {
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        
        iam.has_rights_on_project(found_user._id, project_id, 2, info_id, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/edit-information > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(pj_id) + ' - ' + String(info2_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/edit-information > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/edit-information', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                logs.server.error('/project/edit-information > Information.findById', [String(info_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_info) {
                logs.server.alert('/project/edit-information > Information.findById', 'Incoherence: No information found eventhough it existed before deletion', logs.get_ip(req), found_user);
              } else {
                if (right == 2) {
                  if (req.body.title) {
                    found_info.title = req.body.title;
                  }
                  if (req.body.type) {
                    found_info.type = req.body.type;
                    if (req.body.type == 1) {
                      if (req.body.cvss) {
                        found_info.cvss = req.body.cvss;
                      }
                    }
                  }
                  if (req.body.description) {
                    found_info.description = req.body.description;
                  }
                  if (req.body.tool_output) {
                    found_info.tool_output = req.body.tool_output;
                  }
  
                  found_info.save((err) => {
                    if (err) {
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else {
                      res.json({
                        success: true
                      });
                    }
                  });
                } else {
                  logs.server.info('/project/unlink-information', 'User does not have rights for deletion of information {' + String(info_id) + '}');
                  res.json({
                    success: false,
                    msg: 'Not enough privileges'
                  });
                }
              }
            });
          }
        })
      } else {
        logs.server.info('/project/edit-information', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/get-information-images', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-information-images > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/get-information-images > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.info_id) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 2, info_id, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-information-images > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(info_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-information-images > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-information-images', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                logs.server.error('/project/get-information-images > Information.findById', [String(info_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_info) {
                logs.server.alert('/project/get-information-images > Information.findById', 'Incoherence: No information found eventhough it existed before deletion', logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'No information found'
                });
              } else {
                logs.server.info('/project/get-information-images', 'Information {' + String(found_info._id) + '} images sent to user', logs.get_ip(req), found_user);
                res.json({
                  success: true,
                  poc: found_info.poc
                });
              }
            });
          }
        });
      } else {
        logs.server.info('/project/get-information-images', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/delete-information-image', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/delete-information-image > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/delete-information-image > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.info_id && req.body.image_position) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');

        iam.has_rights_on_project(found_user._id, project_id, 2, info_id, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/delete-information-image > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(info_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/delete-information-image > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/delete-information-image', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                logs.server.error('/project/delete-information-image > Information.findById', [String(info_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                if (right == 2) {
                  // Delete from information
                  found_info.poc.splice(req.body.image_position, 1);
                  found_info.save((err) => {
                    if (err) {
                      logs.server.error('/project/delete-information-image > found.save', [], err, logs.get_ip(req), found_user);
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else {
                      logs.server.info('/project/delete-information-image', 'Image from information {' + String(found_info._id) + '} deleted', logs.get_ip(req), found_user);
                      res.json({
                        success: true,
                        msg: 'Image deleted'
                      });
                    }
                  });
                } else {
                  logs.server.info('/project/delete-information-image', 'User does not have rights for deletion of information {' + String(info_id) + '}');
                  res.json({
                    success: false,
                    msg: 'Not enough privileges'
                  });
                }
              }
            });
          }
        });
      } else {
        logs.server.info('/project/delete-information-image', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/add-information-image', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/add-information-image > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/add-information-image > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.info_id && req.body.image) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, 2, info_id, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/add-information-image > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(info_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/add-information-image > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/add-information-image', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            if (right == 2) {
              Information.findById(info_id, (err, found_info) => {
                if (err) {
                  logs.server.error('/project/add-information-image > Information.findById', [String(info_id)], err, logs.get_ip(req), found_user);
                  res.json({
                    success: false,
                    msg: 'Unex error'
                  });
                } else {
                  // Add image to information
                  found_info.poc.push(req.body.image);
                  found_info.save((err) => {
                    if (err) {
                      logs.server.error('/project/add-information-image > found_info.save', [], err, logs.get_ip(req), found_user);
                      res.json({
                        success: false,
                        msg: 'Unex error'
                      });
                    } else {
                      logs.server.info('/project/add-information-image', 'User added image to information {' + String(found_info._id) + '}', logs.get_ip(req), found_user);
                      res.json({
                        success: true,
                        msg: 'Image added'
                      });
                    }
                  })
                }
              });
            } else {
              logs.server.info('/project/add-information-image', 'User does not have rights for deletion of information {' + String(info_id) + '}');
              res.json({
                success: false,
                msg: 'Not enough privileges'
              });
            }
          }
        });
      } else {
        logs.server.info('/project/add-information-image', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/get-project-informations', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-project-informations > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/get-project-informations > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, project_id, null, null, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-project-informations > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(info_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-project-informations > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-project-informations', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Project.findById(project_id, (err, found_project) => {
              if (err) {
                logs.server.error('/project/add-information-image > Information.findById', [String(info_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                asset_management(found_project.assets, (linked_assets) => {
                  res.json({
                    success: true,
                    assets: linked_assets
                  });
                });
              }
            });
          }
        });
      } else {
        logs.server.info('/project/get-project-informations', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });

  function asset_management(asset_array, callback) {

    advance_array(asset_array, 0, [], (returning_array) => {
      callback(returning_array);
    })

    function advance_array(asset_array, i, return_array, callback) {
      if (i >= asset_array.length) {
        callback(return_array);
      } else {
        get_asset(asset_array, i, return_array, (asset_array, i, return_array) => {
          advance_array(asset_array, i+1, return_array, callback);
        })
      }
    }

    function get_asset(asset_array, i, return_array, callback) {
      // eslint-disable-next-line security/detect-object-injection
      Asset.findById(asset_array[i], (err, asset_found) => {
        if (err) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.error('/project/get-project-informations > asset_management > get_asset', [String(asset_array[i])], err);
        } else if (!asset_found) {
          callback(asset_array, i, return_array);
        } else {
          get_all_informations(asset_found.informations_linked, (information_list) => {
            return_array.push({asset_name: asset_found.name, informations: information_list});
            callback(asset_array, i, return_array);
          });
        }
      });
    }


    function get_all_informations(infos_array, callback) {
      advance_info_array(infos_array, 0, [], (infos_array) => {
        callback(infos_array);
      });

      function advance_info_array(infos_array, j, return_array, callback) {
        if (j >= infos_array.length) {
          callback(return_array);
        } else {
          get_info(infos_array, j, return_array, (infos_array, j, return_array) => {
            advance_info_array(infos_array, j+1, return_array, callback)
          })
        }
      }

      function get_info(infos_array, j, return_array, callback) {
        // eslint-disable-next-line security/detect-object-injection
        Information.findById(infos_array[j], (err, info_found) => {
          if (err) {
            // eslint-disable-next-line security/detect-object-injection
            logs.server.error('/project/get-project-informations > asset_management > get_info > Information.findById', [String(infos_array[j])], err);
          } else if (!info_found) {
            logs.server.alert('/project/get-project-informations > asset_management > get_info > Information.findById', 'Incoherence information existed in array but not in DB');
            callback(infos_array, j, return_array);
          } else {
            register_info(info_found, return_array, (return_array) => {
              callback(infos_array, j, return_array);
            });
          }
        });
      }

      function register_info(info, return_array, callback) {
        let encrypted_id = encrypter.crypt(JSON.stringify(info._id));
        let registering = {id: encrypted_id, title: info.title, type: info.type, description: info.description};
        return_array.push(registering);
        callback(return_array);
      }
    }
  }
});

router.post('/get-information-details', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/get-information-details > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/get-information-details > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.info_id) {
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');

        iam.has_rights_on_project(found_user._id, project_id, 2, info_id, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/get-information-details > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(info_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/get-information-details > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/get-information-details', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                logs.server.error('/project/get-information-details > Information.findById', [String(info_id)], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                get_linked_informations(found_info.linked_informations, (linked_infos_array) => {
                  let return_info = {title: found_info.title, description: found_info.description, type: found_info.type, cvss: found_info.cvss, tool_output: found_info.tool_output, linked_infos: linked_infos_array, poc: found_info.poc};
                  res.json({
                    success:true,
                    info: return_info
                  });
                });
              }
            })
          }
        });
      } else {
        logs.server.info('/project/get-information-details', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });

  function get_linked_informations(infos_array, callback) {
    advance_array(infos_array, 0, [], (infos_array) => {
      callback(infos_array);
    })

    function advance_array(infos_array, i, return_array, callback) {
      if (i >= infos_array.length) {
        callback(return_array);
      } else {
        get_info(infos_array, i, return_array, (infos_array, i, return_array) => {
          advance_array(infos_array, i+1, return_array, callback)
        })
      }
    }

    function get_info(infos_array, i, return_array, callback) {
      // eslint-disable-next-line security/detect-object-injection
      Information.findById(infos_array[i].id, (err, info_found) => {
        if (err) {
          // eslint-disable-next-line security/detect-object-injection
          logs.server.error('/project/get-information-details > get_linked_information > get_info > Inforamtion.findById', [String(infos_array[i])], err);
        } else if (!info_found) {
          logs.server.alert('/project/get-project-informations > get_linked_information > get_info > Information.findById', 'Incoherence information existed in array but not in DB');
          callback(infos_array, i, return_array);
        } else {
          register_info(info_found, infos_array[i].relation, return_array, (return_array) => {
            callback(infos_array, i, return_array);
          });
        }
      });
    }

    function register_info(info, relation, return_array, callback) {
      let encrypted_id = encrypter.crypt(JSON.stringify(info._id));
      let encrypted_asset = encrypter.crypt(JSON.stringify(info.linked_asset));
      if (relation == 1) {
        relation = 2
      } else if (relation == 2) {
        relation = 1
      } else if (relation == 3) {
         relation = 4
      } else if (relation == 4) {
        relation = 3
      } else {
        relation = 5
      }
      // Get top asset
      Asset.findById(info.linked_asset, (err, found_asset) => {
        if (err) {
          logs.server.error('/project/get-information-details > get_linked_information > register_info > Asset.findById', [String(info.linked_asset)], err);
        } else if (!found_asset) {
          logs.server.alert('/project/get-project-informations > get_linked_information > register_info > Information.findById', 'Incoherence asset existed in array but not in DB');
        } else {
          let registering = {id: encrypted_id, title: info.title, type: info.type, description: info.description, linked_asset: encrypted_asset, relation: relation, asset_name: found_asset.name};
          return_array.push(registering);
          callback(return_array);
        }
      });
    }
  }
});

router.post('/link-user-with-project', (req, res, next) => {
  // Needs Token, new user username and project id to be linked
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
      if (err) {
        logs.server.error('/project/link-user-with-project > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
        res.json({
          success: false,
          msg: 'Unex error'
        });
      } else if (!found_user) {
        logs.server.alert('/project/link-user-with-project > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
      } else {
        if (req.body.username && req.body.project_id) {
          let project_id = encrypter.decrypt(req.body.project_id);
          project_id = project_id.replace('"', '').replace('"', '');
          iam.has_rights_on_project(found_user._id, project_id, null, null, (is_linked, right, err) => {
            if (err) {
              if (err == 'No ressource with this ID') {
                logs.server.alert('/project/link-user-with-project > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(project_id) + ' - ' + String(info_id) + '}', logs.get_ip(req));
              } else {
                logs.server.error('/project/link-user-with-project > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
              }
              res.json({
                success: false,
                msg: 'Unex error'
              });
            } else if (!is_linked) {
              logs.server.alert('/project/link-user-with-project', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
            } else {
              if (right == 2) {
                User.findByUsername(String(req.body.username), (err, found_c_user) => {
                  if (err) {
                    logs.server.error('/project/link-user-with-project > User.findByUsername', [String(req.body.username)], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else if (!found_c_user) {
                    logs.server.info('/project/link-user-with-project', 'Username entered does not exists', logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Username unknown'
                    });
                  } else {
                    Project.findById(project_id, (err, found_project) => {
                      if (err) {
                        logs.server.error('/project/link-user-with-project > Project.findById', [String(project_id)], err, logs.get_ip(req));
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else if (!found_project) {
                        logs.server.info('/project/link-user-with-project', 'Project non existent', logs.get_ip(req), found_user);
                      } else {
                        if (!(found_c_user._id in found_project.team.members)) {
                          found_project.team.members.push(found_c_user._id);
                          found_project.save((err, saved_project) => {
                            if (err) {
                              logs.server.error('/project/link-user-with-project > found_project.save', [], err, logs.get_ip(req), found_user);
                              res.json({
                                success: false,
                                msg: 'Unex error'
                              });
                            } else {
                              found_c_user.RelatedProjects.push(found_project._id);
                              found_c_user.save((err, saved) => {
                                logs.server.info('/project/link-user-with-project', 'User {' + found_c_user._id + ' - ' + found_c_user.Username + '} linked with project', logs.get_ip(req), found_user);
                                res.json({
                                  success: true,
                                  msg: 'User linked'
                                });
                              });
                            }
                          });
                        } else {
                          logs.server.info('/project/link-user-with-project', 'User already in team');
                          res.json({
                            success: false,
                            msg: 'User already in project'
                          });
                        }
                      }
                    });
                  }
                });
              } else {
                logs.server.info('/project/link-user-with-project', 'User does not have rights for deletion of information {' + String(info_id) + '}');
                res.json({
                  success: false,
                  msg: 'Not enough privileges'
                });
              }
            }
          });
        } else {
          logs.server.info('/project/link-user-with-project', 'Missing parameters', logs.get_ip(req), found_user);
        }
      }
  });
});

router.post('/unlink-user-project', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  iam.verify_encrypted_token(req.body.token, (found_user, err) => {
    if (err) {
      logs.server.error('/project/unlink-user-project > iam.verify_encrypted_token', [String(req.body.token)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!found_user) {
      logs.server.alert('/project/unlink-user-project > iam.verify_encrypted_token', 'User not found w/ token {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
    } else {
      if (req.body.project_id && req.body.username) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        decrypted_id = decrypted_id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, String(decrypted_id), null, null, (is_linked, right, err) => {
          if (err) {
            if (err == 'No ressource with this ID') {
              logs.server.alert('/project/unlink-user-project > iam.has_right_on_project', 'Unlogical : No ressource with this id {' + String(decrypted_id) + '}', logs.get_ip(req));
            } else {
              logs.server.error('/project/unlink-user-project > iam.has_right_on_project', [String(req.body.token)], err, logs.get_ip(req));
            }
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.server.alert('/project/unlink-user-project', 'User is not linked to project {' + String(req.body.token) + '}', logs.get_ip(req), found_user);
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                logs.server.error('/project/unlink-user-project > Project.findById', [decrypted_id], err, logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                logs.server.alert('/project/unlink-user-project > Project.findById', 'Unlogical: project disapeared', logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'No project...'
                });
              } else {
                found_user.RelatedProjects = found_user.RelatedProjects.filter(function(value, index, arr){
                    if (value == decrypted_id) {
                      return false
                    } else {
                      return true
                    }
                });
                found_user.save((err, saved) => {
                  if (err) {
                    logs.server.error('/project/unlink-user-project > found_user.save', [], err, logs.get_ip(req), found_user);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    if (found_project.team.leader == found_user._id) {
                      res.json({
                        success: false,
                        msg: 'You cannot unlink the leader'
                      });
                    } else {
                      found_project.team.members = found_project.linked_users.filter(function(value, index, arr){
                        if (value == found_user._id) {
                          return false
                        } else {
                          return true
                        }
                      });
                      found_project.save((err, saved) => {
                        if (err) {
                          res.json({
                            success: false,
                            msg: 'Unex error'
                          });
                          logs.server.error('/project/unlink-user-project > found_user.save', [], err, logs.get_ip(req), found_user);
                        } else {
                          logs.server.info('/project/unlink-user-project', 'User unlinked {' + String(req.body.username) + '}', logs.get_ip(req), found_user);
                          res.json({
                            success: true
                          });
                        }
                      });
                    }
                  }
                });
              }
            });
          }
        });
      } else {
        logs.server.info('/project/unlink-user-project', 'Missing parameters', logs.get_ip(req), found_user);
      }
    }
  });
});

router.post('/verify-token', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  res.json({
    success: true
  })
});

function delete_project(user_id, project_id, callback) {
  Project.findById(project_id, (err, found_project) => {
    if (err) {
      logs.server('Error during delete_project > Project.findById = ' + err, 'error');
    } else {
      // Delete all assets:
      for (i=0; i<found_project.assets.length; i++) {
        delete_asset(found_project._id, found_project.assets[i], () => {})
      }

      for (i=0;i<found_project.linked_users.length; i++) {
        User.findById(found_project.linked_users[i], (err, found_user) => {
          found_user.RelatedProjects = found_user.RelatedProjects.filter(function(value, index, arr){
              if (value.equals(project_id)) {
                return false
              } else {
                return true
              }
          });
          found_user.save((err, saved) => {
            if (err) {
              logs.server('Error during delete_project > found_user.save = ' + err, 'error');
            }
          });
        });
      }
      Project.find({_id: found_project._id}).deleteOne((err) => {
        if (err) {
          logs.server('Error during delete_project > Project.find(---).deleteOne = ' + err, 'error');
        } else {
          callback(true);
        }
      });
    }
  })
}

function delete_asset(project_id, asset_id, callback) {
  Project.findById(project_id, (err, found_project) => {
    if (err) {
      console.log(err);
    } else {
      Asset.findById(asset_id, (err, found_asset) => {
        if (err) {
          console.log(err);
        } else {
          // Remove from all linked asset
          for (j=0;j<found_asset.linked_asset.length;j++) {
            Asset.unlink_assets(found_asset._id, found_asset.linked_asset[j], (err, filtered_out) => {
              if (err) {
                console.log(err);
              } else {
                console.log('Is it true ? ' + filtered_out);
              }
            });
          }

          // Delete all informations from asset:
          for (let i=0; i<found_asset.informations_linked.length; i++) {
            // eslint-disable-next-line security/detect-object-injection
            Information.delete(found_asset.informations_linked[i], () => {})
          }

          // Delete Asset
          Asset.find({_id: found_asset._id}).remove((err) => {
            if (err) {
              console.log(err);
            } else {
              found_project.assets = found_project.assets.filter(function(value, index, arr){
                  if (value.equals(asset_id)) {
                    return false
                  } else {
                    return true
                  }
              });
              Project.updateOne({'_id': project_id}, {'$set': {'assets': found_project.assets}}, (err, updated) => {
                if (err) {
                  console.log(err);
                } else {
                  callback(true);
                }
              });
            }
          })
        }
      })
    }
  });
}

function verify_user_by_encrypted_token(encrypted_token, callback) {
  jwt.verify(encrypted_token, jwt_config.secret, (err, decoded) => {
    if (err) {
      logs.server('Error during verify_user_by_encrypted_token > jwt.verify = ' + err, 'error');
      callback(err, false);
    } else {
      User.findByUsername(decoded.username, (err, found_user) => {
        if (err) {
          console.log(err);
          logs.server('Error during verify_user_by_encrypted_token > user.findByUsername = ' + err, 'error');
          callback(err, false);
        } else if (!found_user) {
          callback('', false);
        } else {
          callback('', true, found_user);
        }
      });
    }
  });
}

function is_user_linked_to_project(user, project_id, callback) {
  Project.findById(project_id, (err, found_project) => {
    if (err) {
      logs.server('Error during is_user_linked_to_project > Project.findById = ' + err, 'error');
      callback(err, false);
    } else if (!found_project) {
      callback('', false);
    } else {
      if (found_project.linked_users.includes(user._id)) {
        if (user.RelatedProjects.includes(found_project._id)) {
          callback('', true);
        } else {
          callback('', false);
        }
      } else {
        callback('', false);
      }
    }
  })

}

function is_user_linked_to_asset(user, asset_id, callback) {
  Asset.findById(asset_id, (err, found_asset) => {
    if (err) {
      logs.server('Error during is_user_linked_to_asset > Asset.findById = ' + err, 'error');
      callback(err, false);
    } else if (!found_asset) {
      callback('', false);
    } else {
      is_user_linked_to_project(user, found_asset.project_id, (err, is_linked) => {
        if (err) {
          callback(err, false);
        } else if (!is_linked) {
          callback('', false);
        } else {
          callback('', true);
        }
      })
    }
  })
}

function is_user_linked_to_information(user, info_id, callback) {
  Information.findById(info_id, (err, found_info) => {
    if (err) {
      logs.server('Error during is_user_linked_to_information > Information.findById = ' + err, 'error');
      callback(err, false);
    } else if (!found_info) {
      callback('', false);
    } else {
      is_user_linked_to_asset(user, found_info.linked_asset, (err, is_linked) => {
        if (err) {
          console.log(err);
          callback(err, false);
        } else {
          callback('', is_linked);
        }
      })
    }
  })
}

module.exports = router;
