const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");
const logs = require("../plugins/log_manager");
const iam = require("../plugins/IAM");

const User = require("../Models/User");

router.post('/test-route', (req, res) => {
  res.json({
    success: true,
    msg: "User Route Online !"
  });
  logs.server.info('/test-route', 'access to test-route detected', logs.get_ip(req));
});

router.post('/verify-token', passport.authenticate('jwt', { session: false }), (req, res) => {
  res.json({
    success: true
  });
});

router.post('/test-email-user', (req, res) => {

  let username = req.body.username;
  let email = req.body.email;

  if (!username || !email) {
    logs.server.alert('/users/test-email-user', 'No username or email provided', logs.get_ip(req));
    res.json({
      success: false,
      msg: 'Missing parameters'
    });
  } else {
    isEmailTaken(email, (err, email_res) => {
      if (err) {
        logs.server.error('/users/test-email-user', [String(email)], err, logs.get_ip(req));
        res.json({
          success: false,
          msg: 'unex error'
        });
      } else {
        isUserTaken(username, (err, user_res) => {
          if (err) {
            res.json({
              success: false,
              msg: 'unex error'
            });
          } else {
            if (email_res || user_res) {
              logs.server.info('/users/test-email-user', 'Username or Email is taken - {' + String(email) + ', ' + String(username) + '}', logs.get_ip(req));
              res.json({
                success: false,
                email: email_res,
                user: user_res
              });
            } else {
              logs.server.info('/users/test-email-user', 'Username and Email not taken - {' + String(email) + ', ' + String(username) + '}', logs.get_ip(req));
              res.json({
                success: true
              });
            }
          }
        })
      }
    });
  }

  

  function isEmailTaken(email, callback) {
    User.findByEmail(email, (err, found_doc) => {
      if (err) {
        logs.server.error('/users/test-email-user > isEmailTaken > User.findByEmail', [String(email)], err, logs.get_ip(req));
        callback(err, null);
      } else if (found_doc) {
        callback(null, true);
      } else {
        callback(null, false);
      }
    });
  }

  function isUserTaken(username, callback) {
    User.findByUsername(username, (err, found_doc) => {
      if (err) {
        logs.server.error('/users/test-email-user > isUserTaken > User.findByUsername', [String(username)], err, logs.get_ip(req));
        callback(err, null);
      } else if (found_doc) {
        callback(null, true);
      } else {
        callback(null, false);
      }
    });
  }


});

router.post('/new', (req, res) => {

  const new_user = new User({
    Username: req.body.username,
    Password: req.body.password,
    Email: req.body.email,
    Name: req.body.name,
    Surname: req.body.surname,
    Phone: req.body.phone,
    Post: req.body.post,
    Is_admin: false,
    Await_approbation: true,
    Is_Validated: {
      state: false,
      by: null
    }
  });

  logs.server.info('/users/new', 'New user creation requested', logs.get_ip(req));

  User.user_exist(new_user, (result, ident) => {
    if (result) {
      if (ident === 'email') {
        logs.server.info('/users/new', 'User creation requested with registered email - {' + String(new_user.Email) + '}', logs.get_ip(req));
        res.json({
          success: false,
          msg: 'email_taken'
        });
      } else {
        logs.server.info('/users/new', 'User creation requested with registered username - {' + String(new_user.Username) + '}', logs.get_ip(req));
        res.json({
          success: false,
          msg: 'user_taken'
        });
      }
    } else {
      User.addUser(new_user, (err) => {
        if (err) {
          logs.server.error('/users/new > User.addUser', [String(new_user.Email), String(new_user.Username)], err, logs.get_ip(req));
          res.json({
            success: false,
            msg: 'Unex Error'
          });
        } else {
          logs.server.info('/users/new', 'New user created - {' + new_user.Username + '}', logs.get_ip(req));
          res.json({
            success: true,
            msg: 'user created'
          });
        }
      })
    }
  })
});

router.post('/login', (req, res) => {

  const username = req.body.username;
  const password = req.body.password;
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress || req.socket.remoteAddress || (req.connection.socket ? req.connection.socket.remoteAddress : null);

  User.findByUsername(username, (err, found_user) => {
    if (err) {
      logs.server.error('/users/login > User.findByUsername', [String(username)], err, logs.get_ip(req));
      res.json({
        success: false,
        msg: 'Unex Error'
      });
    } else if (!found_user) {
      logs.server.info('/users/login', 'Login failed: bad username {' + String(username) + '}', logs.get_ip(req));
      res.json({
        success: false,
        msg: 'bad cred'
      });
    } else {
      User.comparePassword(found_user, password, (result) => {
        if (result) {
          // Create new Session ID (IP + ID)
          const sessionId = encrypter.crypt(ip + '+///+' + found_user._id);
          User.addSession(found_user, sessionId, (err, new_user) => {
            if (err) {
              logs.server.error('/users/login > User.addSession', [String(found_user.Username), String(sessionId)], err, logs.get_ip(req));
              res.json({
                success: false,
                msg: 'Unex Error'
              });
            } else {
              // Create JWT Payload

              if (found_user.Await_approbation) {
                logs.server.info('/users/login', 'User tried to connect while awaiting admin approbation', logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'User not validated'
                });
              } else if (!found_user.Is_Validated.state) {
                logs.server.info('/users/login', 'User invalidated by admin tried to connect', logs.get_ip(req), found_user);
                res.json({
                  success: false,
                  msg: 'User refused by admin'
                })
              } else {
                createToken(sessionId, new_user.Username, (token) => {
                  logs.server.info('/users/login', 'User loggin success {' + String(req.body.username) + '}', logs.get_ip(req));
                  res.json({
                    success: true,
                    token: token,
                    msg: 'Access Granted'
                  });
                });
              }
            }
          })
        } else {
          logs.server.alert('/users/login', 'Login failed: bad password {' + String(username) + '}', logs.get_ip(req));
          res.json({
            success: false,
            msg: 'bad cred'
          });
        }
      })
    }
  });

  function createToken(session, username, callback) {
    const payload = {
      session: session,
      username: username
    };
    const token = jwt.sign(payload, jwt_config.secret, {
      expiresIn: 345600 // Expires in 4 days
    });

    callback(token);
  }
});


module.exports = router;
