const express = require("express");
const router = express.Router();
const jwt = require("jsonwebtoken");
const passport = require("passport");

const jwt_config = require("../configs/jwt");
const encrypter = require("../plugins/encrypter");
const logs = require('../plugins/log_manager');
const iam = require("../plugins/IAM");

const User = require("../Models/User");
const Project = require("../Models/Project");
const Asset = require("../Models/Asset");
const Information = require("../Models/Information");

router.post('/test-route', (req, res) => {
  res.json({
    success: true,
    msg: "Project Route Online !"
  });
  logs.server('Access to PROJECT TEST ROUTE detected');
});

router.post('/create', passport.authenticate('jwt', {session: false}), (req, res) => {

  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex Error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad User...'
      });
      logs.server('/project/create -- Could not verify user', 'alert');
    } else {
      logs.user('New project creation -- user:{' + found_user.Username + '}');
      logs.server('New project creation -- user:{' + found_user.Username + '}');
      const newProject = new Project({
        team: {
          leader: found_user._id,
          members: []
        },
        name: req.body.name,
        assets: [],
        description: req.body.description,
        linked_users: [found_user._id],
        creation: Date.now()
      });

      Project.add(newProject, (err, saved) => {
        if (err) {
          logs.server('/project/create -- Error during Project.add = ' + err, 'error');
          res.json({
            success: false,
            msg: 'Unex error'
          });
        } else {
          User.addProject(found_user, saved._id, (err) => {
            if (err) {
              logs.server('/project/create -- Error during User.addProject = ' + err, 'error');
              res.json({
                success: false,
                msg: 'Unex error'
              })
            } else {
              logs.user('User {' + found_user.Username + '} created project {' + saved.name + '}');
              logs.server('User {' + found_user.Username + '} created project {' + saved.name + '}');
              res.json({
                success: true,
                msg: 'Project Created'
              });
            }
          })
        }
      })
    }
  });
});

router.post('/get-user-projects', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      logs.user('/project/get-user-project -- Could not verify user', 'alert');
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      logs.server('List user projects for {' + found_user.Username + '}');
      get_projects(found_user.RelatedProjects, (found_projects) => {
        res.json({
          success: true,
          projects: found_projects
        });
      })
    }
  })

  function get_projects(project_array, callback) {
    let project_done = 0;
    let return_array = [];
    advance_array(project_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(project_array, i, return_array, callback) {
      if (i >= project_array.length) {
        callback(return_array);
      } else {
        get_project(project_array, i, return_array, (project_array, i, return_array) => {
          advance_array(project_array, i+1, return_array, callback)
        })
      }
    }

    function get_project(project_array, i, return_array, callback) {
      Project.findById(project_array[i], (err, project_found) => {
        if (err) {
          logs.server('Error during get_project > advance_array > get_project = ' + err, 'error');
        } else if (!project_found) {
          logs.server('Incoherence in get_project > advance_array > get_project -- Project ID referenced in user but no project exists with this id', 'alert');
          callback(project_array, i, return_array);
        } else {
          register_project(project_found, return_array, (return_array) => {
            callback(project_array, i, return_array);
          });
        }
      });
    }

    function register_project(project, return_array, callback) {
      let encrypted_id = encrypter.crypt(JSON.stringify(project._id))
      let registering = {name: project.name, description: project.description, id: encrypted_id}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/get-project-details', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      logs.user('/project/get-user-project -- Could not verify user', 'alert');
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        logs.server('Getting project details {' + String(decrypted_id) + '}');
        decrypted_id = decrypted_id.replace('"', '').replace('"', '')
        is_user_linked_to_project(found_user, decrypted_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
            logs.user('User does not have rights on this project {' + found_user.Username + '}', 'alert');
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                logs.server('/project/get-project-details > Project.findById = ' + err, 'error');
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                logs.server('Incoherence in /project/get-project-detail -- No project found with the given ID, the project must have been deleted while processing.', 'alert');
                res.json({
                  success: false,
                  msg: 'No project'
                });
              } else {
                let return_project = {name: found_project.name, description: found_project.description};
                res.json({
                  success: true,
                  project: return_project
                });
              }
            })
          }
        })
      } else {
        logs.user('/project/get-user-project -- Route invoked with no "project_id" parameter', 'alert');
        res.json({
          success: false,
          msg: 'No project id'
        });
      }
    }
  })
});

router.post('/edit_project', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      logs.user('/project/edit_project -- Could not verify user', 'alert');
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        logs.server('Project edition process launched {' + String(decrypted_id) + '}');
        decrypted_id = decrypted_id.replace('"', '').replace('"', '');
        is_user_linked_to_project(found_user, decrypted_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            logs.user('User does not have rights on this project {' + found_user.Username + '}', 'alert');
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                logs.server('/project/edit_project > Project.findById = ' + err, 'error');
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                logs.server('Incoherence in /project/get-project-detail -- No project found with the given ID, the project must have been deleted while processing.', 'alert');
                res.json({
                  success: false,
                  msg: 'No project'
                });
              } else {
                if (req.body.name) {
                  found_project.name = req.body.name;
                }
                if (req.body.description) {
                  found_project.description = req.body.description;
                }

                found_project.save((err, saved) => {
                  if (err) {
                    logs.server('/project/edit_project > found_project.save = ' + err, 'error');
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    res.json({
                      success: true
                    });
                  }
                })
              }
            })
          }
        })
      } else {
        logs.user('/project/edit_project -- Route invoked with no "project_id" parameter', 'alert');
        res.json({
          success: false,
          msg: 'No project id'
        })
      }
    }
  })
});

router.post('/delete-project', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  if (req.body.project_id) {
    let project_id = encrypter.decrypt(req.body.project_id);
    logs.server('Project deletion in action + {' + project_id + '}');
    project_id = project_id.replace('"', '').replace('"', '');

    verify_user_by_encrypted_token(req.body.token, (err, user_present, found_user) => {
      if (err) {
        res.json({
          success: false,
          msg: 'Unex error'
        });
      } else if (!user_present) {
        logs.user('/project/delete_project -- Could not verify user', 'alert');
        res.json({
          success: false,
          msg: 'Bad user'
        });
      } else {
        is_user_linked_to_project(found_user, project_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            })
          } else if (!is_linked) {
            logs.user('User does not have rights on this project {' + found_user.Username + '}', 'alert');
            res.json({
              success: false,
              msg: 'No rights for this project'
            });
          } else {
            delete_project(found_user._id, project_id, () => {
              res.json({
                success: true,
                msg: 'Project deleted'
              });
            })
          }
        })
      }
    });
  } else {
    logs.server('/project/delete_project -- Route invoked with no "project_id" parameter', 'alert');
    res.json({
      success: false,
      msg: 'No project id'
    })
  }
});

router.post('/create-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  // Find relative Project:
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      logs.server('error occured on /project/create-asset > verify_user_by_encrypted_token with params {' + String(req.body.token) + '} - err : ' + err, 'error');
      res.json({
        success: false,
        msg: 'Unex Error'
      });
    } else if (!is_user) {
      logs.user('/project/create-asset -- Could not verify user', 'alert');
      res.json({
        success: false,
        msg: 'Bad User...'
      });
    } else {
      if (req.body.project_id) {
        let id = encrypter.decrypt(req.body.project_id);
        id = id.replace('"', '').replace('"', '');
        iam.has_rights_on_project(found_user._id, id, null, null, (is_linked) => {
          if (!is_linked) {
            logs.user('/project/create-asset user {' + found_user.Username + '} tried to acces asset without the rights', 'alert');
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            Project.findById(id, (err, found_project) => {
              if (err) {
                logs.server('error occured on /project/create-asset > project.findById with params {' + id + '} - err : ' + err, 'error');
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                let newAsset = new Asset({
                  name: req.body.a_name,
                  project_id: found_project._id,
                  type: req.body.type,
                  asset_description: req.body.asset_desc,
                  informations_linked: [],
                  creation: Date.now()
                });
                Asset.add(newAsset, (err, saved) => {
                  if (err) {
                    logs.server('error occured on /project/create-asset > Asset.add with params {' + newAsset.Name + '(AssetObject)} - err : ' + err, 'error');
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    Project.add_asset(found_project, saved._id, (err, saved_project) => {
                      if (err) {
                        logs.server('error occured on /project/create-asset > Project.add_asset with params {' + found_project._id + '(ProjectObject) , ' + saved._id + '} - err : ' + err, 'error');
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else {
                        let asset_id = encrypter.crypt(JSON.stringify(saved._id));
                        logs.server('{' + found_user.username + '} created asset {' + saved.name + '}')
                        res.json({
                          success: true,
                          asset_id: asset_id,
                          msg: 'Asset Registered'
                        });
                      }
                    })
                  }
                })
              }
            })
          }
        });
      } else {
        logs.server('/project/create-asset - Attempt on creating asset without projectId');
        res.json({
          success: false,
          msg: 'No project id'
        })
      }
    }
  });
});

router.post('/get-project-assets', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  console.log('Getting all assets');

  // Receiving project encrypted id
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user'
      });
      console.log('No user found.');
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        decrypted_id = decrypted_id.replace('"', '').replace('"', '');

        iam.is_user_linked_to_project(found_user, decrypted_id, (is_linked) => {

        });

        is_user_linked_to_project(found_user, decrypted_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            console.log('User not linked');
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                console.log(err);
              } else {
                console.log('Get all assets linked to ' + found_project._id);
                get_assets(found_project.assets, (found_assets) => {
                  res.json({
                    success: true,
                    assets: found_assets
                  });
                });
              }
            });
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No project id'
        })
      }
    }
  });

  function get_assets(project_array, callback) {
    let project_done = 0;
    let return_array = [];
    advance_array(project_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(asset_array, i, return_array, callback) {
      if (i >= asset_array.length) {
        console.log('End of Assets Management');
        callback(return_array);
      } else {
        console.log('Next Asset');
        get_asset(asset_array, i, return_array, (project_array, i, return_array) => {
          advance_array(asset_array, i+1, return_array, callback)
        })
      }
    }

    function get_asset(asset_array, i, return_array, callback) {
      Asset.findById(asset_array[i], (err, asset_found) => {
        if (err) {
          console.log(err);
        } else if (!asset_found) {
          console.log('Not existent in Assets')
          callback(asset_array, i, return_array);
        } else {
          console.log('Present in Assets')
          register_asset(asset_found, return_array, (return_array) => {
            callback(asset_array, i, return_array);
          });
        }
      });
    }

    function register_asset(asset, return_array, callback) {
      console.log('Registering Asset')
      let encrypted_id = encrypter.crypt(JSON.stringify(asset._id))
      let registering = {id: encrypted_id, name: asset.name, type: asset.type, description: asset.asset_description}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/link-asset-to-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (!req.body.link_asset_id || !req.body.link_to_asset_id) {
        res.json({
          success: false,
          msg: 'Needing ids for assets'
        });
      } else {
        let link_asset_id = encrypter.decrypt(req.body.link_asset_id);
        link_asset_id = link_asset_id.replace('"', '').replace('"', '');
        let asset_to_id = encrypter.decrypt(req.body.link_to_asset_id);
        asset_to_id = asset_to_id.replace('"', '').replace('"', '');

        is_user_linked_to_asset(found_user, link_asset_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this asset'
            });
          } else {
            is_user_linked_to_asset(found_user, asset_to_id, (err, is_linked) => {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                res.json({
                  success: false,
                  msg: 'You have no right on this asset'
                });
              } else {
                Asset.findById(link_asset_id, (err, link_asset) => {
                  if (err) {
                    console.log(err);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else if (!link_asset) {
                    res.json({
                      success: false,
                      msg: 'No asset for this id'
                    });
                  } else {
                    Asset.findById(asset_to_id, (err, link_to_asset) => {
                      if (err) {
                        console.log(err);
                        res.json({
                          success: false,
                          msg: 'No asset for this id'
                        });
                      } else if (!link_to_asset) {
                        res.json({
                          success: false,
                          msg: 'No asset 2 for this id'
                        });
                      } else {
                        if (!link_to_asset.linked_asset) {
                          link_to_asset.linked_asset = [];
                        }
                        if (!link_asset.linked_asset) {
                          link_asset.linked_asset = [];
                        }

                        link_to_asset.linked_asset.push(link_asset._id);
                        link_asset.linked_asset.push(link_to_asset._id);

                        link_asset.save((err, saved) => {
                          if (err) {
                            console.log(err);
                            res.json({
                              success: false,
                              msg: 'Unex error'
                            });
                          } else {
                            link_to_asset.save((err, saved2) => {
                              if (err) {
                                console.log(err);
                                res.json({
                                  success: false,
                                  msg: 'Unex error'
                                });
                              } else {
                                res.json({
                                  success: true
                                });
                              }
                            });
                          }
                        });
                      }
                    })
                  }
                });
              }
            });
          }
        });
      }
    }
  });
});

router.post('/unlink-assets', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (!req.body.unlink_asset_1 || !req.body.unlink_asset_2) {
        res.json({
          success: false,
          msg: 'Needing ids'
        })
      } else {
        let asset_1_id = encrypter.decrypt(req.body.unlink_asset_1);
        asset_1_id = asset_1_id.replace('"', '').replace('"', '');
        let asset_2_id = encrypter.decrypt(req.body.unlink_asset_2);
        asset_2_id = asset_2_id.replace('"', '').replace('"', '');


        is_user_linked_to_asset(found_user, asset_1_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            is_user_linked_to_asset(found_user, asset_2_id, (err, is_linked) => {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                res.json({
                  success: false,
                  msg: 'You have no right on this project'
                });
              } else {
                unlink_assets(asset_1_id, asset_2_id, (err, result) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else if (result) {
                    unlink_assets(asset_2_id, asset_1_id, (err, result2) => {
                      if (err) {
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else if (result2) {
                        res.json({
                          success: true
                        });
                      } else {
                        res.json({
                          success: false,
                          msg: 'Error in unlinking'
                        });
                      }
                    });
                  } else {
                    res.json({
                      success: false,
                      msg: 'Error in unlinking'
                    })
                  }
                });
              }
            });
          }
        });
      }
    }
  });
});

router.post('/delete_asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        succes: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        is_user_linked_to_asset(found_user, asset_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this asset'
            });
          } else {
            console.log('Deleting Asset');
            if (req.body.project_id) {
              let project_id = encrypter.decrypt(req.body.project_id);
              project_id = project_id.replace('"', '').replace('"', '');

              delete_asset(project_id, asset_id, () => {
                res.json({
                  success: true,
                  msg: 'Asset deleted'
                })
              })
            } else {
              res.json({
                success: true,
                msg: 'No project id'
              })
            }
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No asset id'
        })
      }
    }
  });
});

router.post('/add-information', passport.authenticate('jwt', { session: false }), (req, res, next) => {

  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');

        is_user_linked_to_asset(found_user, asset_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no rights on this project'
            });
          } else {
            // Begin creation
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                console.log(err);
              } else {
                // Create Information
                console.log('Information assembly');
                let newInfo = new Information({
                  title: req.body.title,
                  type: req.body.type,
                  description: req.body.description,
                  tool_output: req.body.tool,
                  cvss: req.body.cvss,
                  poc: [],
                  linked_asset: found_asset._id,
                  creation: Date.now()
                });

                if (req.body.poc) {
                  console.log('POC Present');
                  newInfo.poc = req.body.poc;
                }

                console.log('Information register')
                Information.add(newInfo, (err, saved) => {
                  if (err) {
                    console.log(err);
                  } else {
                    console.log('Adding to asset')
                    Asset.add_info(found_asset, saved._id, (err, saved_asset) => {
                      if (err) {
                        console.log(err);
                      } else {
                        console.log('Registered');
                        res.json({
                          success: true,
                          msg: 'Info registered'
                        })
                      }
                    })
                  }
                })
              }
            });
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No asset id'
        })
      }
    }
  });
});

router.post('/get-asset-details', passport.authenticate('jwt', { session: false }), (req, res, next) => {

  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      console.log('Retreiving details of asset')
      if (req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');

        is_user_linked_to_asset(found_user, asset_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no rights on this project'
            });
          } else {
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                console.log(err);
              } else {
                // Research all other assets:
                get_linked_assets(found_asset.linked_asset, (linked_asset_array) => {
                  let return_asset = {name: found_asset.name, description: found_asset.asset_description, type: found_asset.type, linked_asset: linked_asset_array}
                  res.json({
                    success:true,
                    asset: return_asset
                  });
                });
              }
            })
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No asset id'
        });
      }
    }
  })

  function get_linked_assets(assets_array, callback) {
    let project_done = 0;
    let return_array = [];
    advance_array(assets_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(assets_array, i, return_array, callback) {
      if (i >= assets_array.length) {
        console.log('End of linked assets Management');
        callback(return_array);
      } else {
        console.log('Next Info');
        get_asset(assets_array, i, return_array, (assets_array, i, return_array) => {
          advance_array(assets_array, i+1, return_array, callback)
        })
      }
    }

    function get_asset(assets_array, i, return_array, callback) {
      Asset.findById(assets_array[i], (err, asset_found) => {
        if (err) {
          console.log(err);
        } else if (!asset_found) {
          console.log('Not existent in assets')
          callback(information_array, i, return_array);
        } else {
          console.log('Present in informations')
          register_asset(asset_found, return_array, (return_array) => {
            callback(assets_array, i, return_array);
          });
        }
      });
    }

    function register_asset(asset, return_array, callback) {
      console.log('Registering Information')
      let encrypted_id = encrypter.crypt(JSON.stringify(asset._id))
      let registering = {id: encrypted_id, name: asset.name, description: asset.asset_description, type: asset.type}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/edit-asset', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        is_user_linked_to_asset(found_user, asset_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_asset) {
                res.json({
                  success: false,
                  msg: 'No asset'
                });
              } else {
                if (req.body.name) {
                  found_asset.name = req.body.name;
                }
                if (req.body.type) {
                  found_asset.type = req.body.type;
                }
                if (req.body.description) {
                  found_asset.asset_description = req.body.description;
                }

                found_asset.save((err, saved) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    res.json({
                      success: true
                    })
                  }
                })
              }
            })
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No asset id'
        });
      }
    }
  })
});

router.post('/get-asset-informations', passport.authenticate('jwt', { session: false }), (req, res, next) => {

  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        is_user_linked_to_asset(found_user, asset_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no rights on this project'
            });
          } else {
            console.log('Retreiving information for asset');

            Asset.findById(asset_id, (err, found_asset) => {
              if (err) {
                console.log(err);
              } else {
                get_informations(found_asset.informations_linked, (info_array) => {
                  info_array.sort(function(info1, info2) {
                    return info2.cvss - info1.cvss
                  })
                  res.json({
                    success: true,
                    infos: info_array
                  })
                })
              }
            })
          }
        });
      } else {
        res.json({
          success: false,
          msg: 'No asset id'
        })
      }
    }
  });


  function get_informations(information_array, callback) {
    let project_done = 0;
    let return_array = [];
    advance_array(information_array, 0, [], (return_array) => {
      callback(return_array)
    })

    function advance_array(information_array, i, return_array, callback) {
      if (i >= information_array.length) {
        console.log('End of Informations Management');
        callback(return_array);
      } else {
        console.log('Next Info');
        get_information(information_array, i, return_array, (information_array, i, return_array) => {
          advance_array(information_array, i+1, return_array, callback)
        })
      }
    }

    function get_information(information_array, i, return_array, callback) {
      Information.findById(information_array[i], (err, info_found) => {
        if (err) {
          console.log(err);
        } else if (!info_found) {
          console.log('Not existent in informations')
          callback(information_array, i, return_array);
        } else {
          console.log('Present in informations')
          register_project(info_found, return_array, (return_array) => {
            callback(information_array, i, return_array);
          });
        }
      });
    }

    function register_project(info, return_array, callback) {
      console.log('Registering Information')
      let encrypted_id = encrypter.crypt(JSON.stringify(info._id))
      let registering = {title: info.title, type: info.type, tool_output: info.tool_output, cvss: info.cvss, description: info.description, id: encrypted_id}
      return_array.push(registering)
      callback(return_array);
    }
  }
});

router.post('/link-informations', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      // Link informations:
      if (req.body.information1_id && req.body.information2_id && req.body.information_link) {
        // Info 1 decryption
        info1_id = encrypter.decrypt(req.body.information1_id);
        info1_id = info1_id.replace('"', '').replace('"', '');
        // Info 2 decryption
        info2_id = encrypter.decrypt(req.body.information2_id);
        info2_id = info2_id.replace('"', '').replace('"', '');

        Information.findById(info1_id, (err, found_info1) => {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!found_info1) {
            res.json({
              success: false,
              msg: 'Bad Info Id'
            });
          } else {
            Information.findById(info2_id, (err, found_info2) => {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_info2) {
                res.json({
                  success: false,
                  msg: 'Bad Info Id'
                });
              } else {
                if (!found_info1.linked_informations) {
                  found_info1.linked_informations = [];
                }
                if (!found_info2.linked_informations) {
                  found_info2.linked_informations = [];
                }

                // Define information relations:
                if (req.body.information_link == 1) {
                  // Worsen the vulnerability
                  found_info2.linked_informations.push({id: found_info1._id, relation: 2});
                  found_info1.linked_informations.push({id: found_info2._id, relation: 1});
                } else if (req.body.information_link == 2) {
                  // Worsened by the vulnerability
                  found_info2.linked_informations.push({id: found_info1._id, relation: 1});
                  found_info1.linked_informations.push({id: found_info2._id, relation: 2});
                } else if (req.body.information_link == 3) {
                  // Serucise the vulnerability
                  found_info2.linked_informations.push({id: found_info1._id, relation: 4});
                  found_info1.linked_informations.push({id: found_info2._id, relation: 3});
                } else if (req.body.information_link == 4) {
                  // Securised by the security
                  found_info2.linked_informations.push({id: found_info1._id, relation: 3});
                  found_info1.linked_informations.push({id: found_info2._id, relation: 4});
                } else {
                  // New information on the information
                  found_info2.linked_informations.push({id: found_info1._id, relation: 5});
                  found_info1.linked_informations.push({id: found_info2._id, relation: 5});
                }

                found_info1.save((err, saved1) => {
                  if (err) {
                    console.log(err);
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    found_info2.save((err, saved2) => {
                      if (err) {
                        console.log(err);
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else {
                        res.json({
                          success: true
                        })
                      }
                    })
                  }
                })
              }
            })
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'Need ids'
        })
      }
    }
  });
});

router.post('/unlink-information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (!req.body.info1_id || !req.body.info2_id) {
        res.json({
          success: false,
          msg: 'Need ids'
        })
      } else {
        let info1_id = encrypter.decrypt(req.body.info1_id);
        info1_id = info1_id.replace('"', '').replace('"', '');
        let info2_id = encrypter.decrypt(req.body.info2_id);
        info2_id = info2_id.replace('"', '').replace('"', '');

        is_user_linked_to_information(found_user, info1_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            is_user_linked_to_information(found_user, info2_id, (err, is_linked) => {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!is_linked) {
                res.json({
                  success: false,
                  msg: 'You have no right on this project'
                });
              } else {
                unlink_informations(info1_id, info2_id, (err, is_deleted) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    unlink_informations(info2_id, info1_id, (err, is_deleted) => {
                      if (err) {
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else {
                        res.json({
                          success: true
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        })
      }
    }
  })
});

router.post('/delete_information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      console.log('Removing Information');

      // Need Asset  & Information
      if (req.body.asset_id) {
        let asset_id = encrypter.decrypt(req.body.asset_id);
        asset_id = asset_id.replace('"', '').replace('"', '');
        if (req.body.info_id) {
          let info_id = encrypter.decrypt(req.body.info_id);
          info_id = info_id.replace('"', '').replace('"', '');

          console.log('+------ DEBUG ------+');
          console.log(asset_id);
          console.log(info_id);

          is_user_linked_to_information(found_user, info_id, (err, is_linked) => {
            if (err) {
              res.json({
                success: false,
                msg: 'Unex error'
              });
            } else if (!is_linked) {
              res.json({
                success: false,
                msg: 'You have no right on this project'
              });
            } else {
              Information.findById(info_id, (err, found_info) => {
                if (err) {
                  console.log(err);
                  res.json({
                    success: false,
                    msg: 'Unex error'
                  });
                } else if (!found_info) {
                  res.json({
                    success: false,
                    msg: 'No info found'
                  });
                } else {
                  // Unlink from all informations
                  for (j=0;j<found_info.linked_informations.length;j++) {
                    unlink_informations(found_info.linked_informations[j].id, found_info._id, (err, deleted) => {});
                  }

                  delete_information(asset_id, info_id, (deleted) => {
                    res.json({
                      success: true,
                      msg: 'Info removed from asset'
                    })
                  });
                }
              });
            }
          })
        } else {
          res.json({
            success: false,
            msg: 'No info id'
          })
        }
      } else {
        res.json({
          success: false,
          msg: 'No asset id'
        });
      }
    }
  });
});

router.post('/edit_information', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      })
    } else {
      if (req.body.info_id) {
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        is_user_linked_to_information(found_user, info_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right over this project'
            });
          } else {
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_info) {
                res.json({
                  success: false,
                  msg: 'No information'
                });
              } else {
                if (req.body.title) {
                  found_info.title = req.body.title;
                }
                if (req.body.type) {
                  found_info.type = req.body.type;
                  if (req.body.type == 1) {
                    if (req.body.cvss) {
                      found_info.cvss = req.body.cvss;
                    }
                  }
                }
                if (req.body.description) {
                  found_info.description = req.body.description;
                }
                if (req.body.tool_output) {
                  found_info.tool_output = req.body.tool_output;
                }

                found_info.save((err, saved) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    res.json({
                      success: true
                    });
                  }
                })
              }
            })
          }
        });
      } else {
        res.json({
          success: false,
          msg: 'No info id'
        });
      }
    }
  })
});

router.post('/get-information-images', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.info_id) {
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        // Verify if user has rights:
        is_user_linked_to_information(found_user, info_id, (err, is_linked) => {
          if (err) {
            console.log(err);
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            // Get information:
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_info) {
                res.json({
                  success: false,
                  msg: 'No information found'
                });
              } else {
                res.json({
                  success: true,
                  poc: found_info.poc
                });
              }
            })
          }
        });
      } else {
        res.json({
          success: false,
          msg: 'Need info id'
        });
      }
    }
  });
});

router.post('/delete-information-image', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.info_id && req.body.image_position) {
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        // Verify if user has rights:
        is_user_linked_to_information(found_user, info_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            // Find information
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                // Delete from information
                found_info.poc.splice(req.body.image_position, 1);
                found_info.save((err, saved_info) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    res.json({
                      success: true,
                      msg: 'Image deleted'
                    });
                  }
                });
              }
            })
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'Need info id and image position'
        });
      }
    }
  });
});

router.post('/add-information-image', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.info_id && req.body.image) {
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');
        // Is info linked
        is_user_linked_to_information(found_user, info_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            // Find information
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else {
                // Add image to information
                found_info.poc.push(req.body.image);
                found_info.save((err, saved_info) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else {
                    res.json({
                      success: true,
                      msg: 'Image added'
                    });
                  }
                })
              }
            })
          }
        });
      } else {
        res.json({
          success: false,
          msg: 'Need info id and image'
        });
      }
    }
  });
});

router.post('/get-project-informations', passport.authenticate('jwt', {session: false}), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.project_id) {
        // Get all project assets:
        let project_id = encrypter.decrypt(req.body.project_id);
        project_id = project_id.replace('"', '').replace('"', '');
        Project.findById(project_id, (err, found_project) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!found_project) {
            res.json({
              success: false,
              msg: 'No project for this id...'
            });
          } else {
            is_user_linked_to_project(found_user, project_id, (err, is_linked) => {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Unex error'
                })
              } else if (!is_linked) {
                res.json({
                  success: false,
                  msg: 'You have no rights on this project'
                })
              } else {
                asset_management(found_project.assets, (linked_assets) => {
                  res.json({
                    success: true,
                    assets: linked_assets
                  });
                });
              }
            });
          }
        });
      } else {
        res.json({
          success: false,
          msg: 'Need project id'
        });
      }
    }
  });

  function asset_management(asset_array, callback) {

    advance_array(asset_array, 0, [], (returning_array) => {
      callback(returning_array);
    })

    function advance_array(asset_array, i, return_array, callback) {
      if (i >= asset_array.length) {
        callback(return_array);
      } else {
        get_asset(asset_array, i, return_array, (asset_array, i, return_array) => {
          advance_array(asset_array, i+1, return_array, callback);
        })
      }
    }

    function get_asset(asset_array, i, return_array, callback) {
      console.log(asset_array[i]);
      Asset.findById(asset_array[i], (err, asset_found) => {
        if (err) {
          console.log(err);
        } else if (!asset_found) {
          console.log('Not existent in project');
          callback(asset_array, i, return_array);
        } else {
          console.log('Getting all infos');
          console.log(asset_found.informations_linked);
          get_all_informations(asset_found.informations_linked, (information_list) => {
            return_array.push({asset_name: asset_found.name, informations: information_list});
            callback(asset_array, i, return_array);
          });
        }
      });
    }


    function get_all_informations(infos_array, callback) {
      advance_info_array(infos_array, 0, [], (infos_array) => {
        callback(infos_array);
      });

      function advance_info_array(infos_array, j, return_array, callback) {
        console.log('J ' + j);
        if (j >= infos_array.length) {
          callback(return_array);
        } else {
          get_info(infos_array, j, return_array, (infos_array, j, return_array) => {
            advance_info_array(infos_array, j+1, return_array, callback)
          })
        }
      }

      function get_info(infos_array, j, return_array, callback) {
        console.log('Information ' + (j+1) + '/' + infos_array.length + ' : ' + infos_array[j]);
        Information.findById(infos_array[j], (err, info_found) => {
          if (err) {
            console.log(err);
          } else if (!info_found) {
            console.log('Not existent in assets');
            callback(infos_array, j, return_array);
          } else {
            console.log('Present in asset');
            register_info(info_found, return_array, (return_array) => {
              callback(infos_array, j, return_array);
            });
          }
        });
      }

      function register_info(info, return_array, callback) {
        console.log('Registering Information');
        let encrypted_id = encrypter.crypt(JSON.stringify(info._id));
        let registering = {id: encrypted_id, title: info.title, type: info.type, description: info.description};
        return_array.push(registering);
        callback(return_array);
      }
    }
  }
});

router.post('/get-information-details', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      console.log('Retreiving details of information')
      if (req.body.info_id) {
        let info_id = encrypter.decrypt(req.body.info_id);
        info_id = info_id.replace('"', '').replace('"', '');

        is_user_linked_to_information(found_user, info_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no rights on this project'
            });
          } else {
            Information.findById(info_id, (err, found_info) => {
              if (err) {
                console.log(err);
              } else {
                get_linked_informations(found_info.linked_informations, (linked_infos_array) => {
                  let return_info = {title: found_info.title, description: found_info.description, type: found_info.type, cvss: found_info.cvss, tool_output: found_info.tool_output, linked_infos: linked_infos_array, poc: found_info.poc};
                  res.json({
                    success:true,
                    info: return_info
                  });
                });
              }
            })
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No info id'
        });
      }
    }
  });

  function get_linked_informations(infos_array, callback) {
    let project_done = 0;
    let return_array = [];
    advance_array(infos_array, 0, [], (infos_array) => {
      callback(infos_array);
    })

    function advance_array(infos_array, i, return_array, callback) {
      if (i >= infos_array.length) {
        callback(return_array);
      } else {
        get_info(infos_array, i, return_array, (infos_array, i, return_array) => {
          advance_array(infos_array, i+1, return_array, callback)
        })
      }
    }

    function get_info(infos_array, i, return_array, callback) {
      Information.findById(infos_array[i].id, (err, info_found) => {
        if (err) {
          console.log(err);
        } else if (!info_found) {
          console.log('Not existent in assets');
          callback(information_array, i, return_array);
        } else {
          console.log('Present in informations');
          register_info(info_found, infos_array[i].relation, return_array, (return_array) => {
            callback(infos_array, i, return_array);
          });
        }
      });
    }

    function register_info(info, relation, return_array, callback) {
      console.log('Registering Information');
      let encrypted_id = encrypter.crypt(JSON.stringify(info._id));
      let encrypted_asset = encrypter.crypt(JSON.stringify(info.linked_asset));
      if (relation == 1) {
        relation = 2
      } else if (relation == 2) {
        relation = 1
      } else if (relation == 3) {
         relation = 4
      } else if (relation == 4) {
        relation = 3
      } else {
        relation = 5
      }
      // Get top asset
      Asset.findById(info.linked_asset, (err, found_asset) => {
        if (err) {
          console.log(err);
        } else if (!found_asset) {
          console.log('No asset found with this info');
        } else {
          let registering = {id: encrypted_id, title: info.title, type: info.type, description: info.description, linked_asset: encrypted_asset, relation: relation, asset_name: found_asset.name};
          return_array.push(registering);
          callback(return_array);
        }
      });
    }
  }
});

router.post('/link_user_with_project', (req, res, next) => {
  // Needs Token, new user username and project id to be linked
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      User.findByUsername(req.body.username, (err, found_sec_user) => {
        if (err) {
          console.log(err);
          res.json({
            success: false,
            msg: 'Unex error'
          });
        } else if (!found_sec_user) {
          res.json({
            success: false,
            msg: 'No user'
          });
        } else {
          if (req.body.project_id) {
            let decrypted_id = encrypter.decrypt(req.body.project_id);
            decrypted_id = decrypted_id.replace('"', '').replace('"', '')
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                res.json({
                  success: false,
                  msg: 'No project found'
                });
              } else {
                is_user_linked_to_project(found_user, found_project._id, (err, is_linked) => {
                  if (err) {
                    res.json({
                      success: false,
                      msg: 'Unex error'
                    });
                  } else if (!is_linked) {
                    res.json({
                      success: false,
                      msg: 'You have no rigths on this project'
                    });
                  } else {
                    is_user_linked_to_project(found_sec_user, found_project._id, (err, is_linked) => {
                      if (err) {
                        res.json({
                          success: false,
                          msg: 'Unex error'
                        });
                      } else if (is_linked) {
                        console.log('User in project');
                        res.json({
                          success: false,
                          msg: 'User already in project'
                        });
                      } else {
                        found_sec_user.RelatedProjects.push(found_project._id);
                        found_sec_user.save((err, saved_user) => {
                          if (err) {
                            console.log(err);
                            res.json({
                              success: false,
                              msg: 'Unex error'
                            });
                          } else {
                            found_project.linked_users.push(found_sec_user._id);
                            found_project.save((err, saved_err) => {
                              if (err) {
                                res.json({
                                  success: false,
                                  msg: 'Unex error'
                                });
                              } else {
                                res.json({
                                  success: true
                                });
                              }
                            })
                          }
                        });
                      }
                    });
                  }
                });
              }
            })
          } else {
            res.json({
              success: false,
              msg: 'No project id'
            })
          }
        }
      })
    }
  });
});

router.post('/unlink_user_project', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  verify_user_by_encrypted_token(req.body.token, (err, is_user, found_user) => {
    if (err) {
      res.json({
        success: false,
        msg: 'Unex error'
      });
    } else if (!is_user) {
      res.json({
        success: false,
        msg: 'Bad user...'
      });
    } else {
      if (req.body.project_id) {
        let decrypted_id = encrypter.decrypt(req.body.project_id);
        decrypted_id = decrypted_id.replace('"', '').replace('"', '')
        is_user_linked_to_project(found_user, decrypted_id, (err, is_linked) => {
          if (err) {
            res.json({
              success: false,
              msg: 'Unex error'
            });
          } else if (!is_linked) {
            res.json({
              success: false,
              msg: 'You have no right on this project'
            });
          } else {
            Project.findById(decrypted_id, (err, found_project) => {
              if (err) {
                console.log(err);
                res.json({
                  success: false,
                  msg: 'Unex error'
                });
              } else if (!found_project) {
                res.json({
                  success: false,
                  msg: 'No project...'
                });
              } else {
                found_user.RelatedProjects = found_user.RelatedProjects.filter(function(value, index, arr){
                    if (value == decrypted_id) {
                      return false
                    } else {
                      return true
                    }
                });
                found_user.save((err, saved) => {
                  if (err) {
                    console.log(err);
                  } else {
                    found_project.linked_users = found_project.linked_users.filter(function(value, index, arr){
                        if (value == found_user._id) {
                          return false
                        } else {
                          return true
                        }
                    });
                    found_project.save((err, saved) => {
                      if (err) {
                        console.log(err);
                      } else {
                        res.json({
                          success: true
                        })
                      }
                    });
                  }
                });
              }
            })
          }
        })
      } else {
        res.json({
          success: false,
          msg: 'No project id'
        })
      }
    }
  });
});

router.post('/verify-token', passport.authenticate('jwt', { session: false }), (req, res, next) => {
  res.json({
    success: true
  })
});

function delete_project(user_id, project_id, callback) {
  Project.findById(project_id, (err, found_project) => {
    if (err) {
      logs.server('Error during delete_project > Project.findById = ' + err, 'error');
    } else {
      // Delete all assets:
      for (i=0; i<found_project.assets.length; i++) {
        delete_asset(found_project._id, found_project.assets[i], () => {})
      }

      for (i=0;i<found_project.linked_users.length; i++) {
        User.findById(found_project.linked_users[i], (err, found_user) => {
          found_user.RelatedProjects = found_user.RelatedProjects.filter(function(value, index, arr){
              if (value.equals(project_id)) {
                return false
              } else {
                return true
              }
          });
          found_user.save((err, saved) => {
            if (err) {
              logs.server('Error during delete_project > found_user.save = ' + err, 'error');
            }
          });
        });
      }
      Project.find({_id: found_project._id}).deleteOne((err) => {
        if (err) {
          logs.server('Error during delete_project > Project.find(---).deleteOne = ' + err, 'error');
        } else {
          callback(true);
        }
      });
    }
  })
}

function unlink_assets(asset_to_unlink_id, asset_to_be_unlinked_id, callback) {
  Asset.findById(asset_to_be_unlinked_id, (err, found_asset) => {
    if (err) {
      console.log(err);
      callback(err, false);
    } else if (!found_asset) {
      callback('', false);
    } else {
      let new_arrray = found_asset.linked_asset.filter(function(value, index, arr){
        if (value.equals(asset_to_unlink_id)) {
          return false
        } else {
          return true
        }
      });
      found_asset.linked_asset = new_arrray;
      found_asset.save((err, saved) => {
        callback('', true);
      });
    }
  });
}

function unlink_informations(info1_id, info2_id, callback) {
  Information.findById(info1_id, (err, found_info1) => {
    if (err) {
      console.log(err);
      callback(err, false);
    } else if (!found_info1) {
      callback('', false);
    } else {
      let new_arrray = found_info1.linked_informations.filter(function(value, index, arr){
        if (value.id.equals(info2_id)) {
          return false;
        } else {
          return true;
        }
      });
      found_info1.linked_informations = new_arrray;
      found_info1.save((err, saved) => {
        callback('', true);
      });
    }
  });
}

function delete_asset(project_id, asset_id, callback) {
  Project.findById(project_id, (err, found_project) => {
    if (err) {
      console.log(err);
    } else {
      Asset.findById(asset_id, (err, found_asset) => {
        if (err) {
          console.log(err);
        } else {
          // Remove from all linked asset
          for (j=0;j<found_asset.linked_asset.length;j++) {
            unlink_assets(found_asset._id, found_asset.linked_asset[j], (err, filtered_out) => {
              if (err) {
                console.log(err);
              } else {
                console.log('Is it true ? ' + filtered_out);
              }
            });
          }

          // Delete all informations from asset:
          for (i=0; i<found_asset.informations_linked.length; i++) {
            delete_information(found_asset._id, found_asset.informations_linked[i], () => {})
          }

          // Delete Asset
          Asset.find({_id: found_asset._id}).remove((err) => {
            if (err) {
              console.log(err);
            } else {
              found_project.assets = found_project.assets.filter(function(value, index, arr){
                  if (value.equals(asset_id)) {
                    return false
                  } else {
                    return true
                  }
              });
              found_project.save((err, saved) => {
                if (err) {
                  console.log(err);
                } else {
                  callback(true)
                }
              })
            }
          })
        }
      })
    }
  });
}

function delete_information(asset_id, info_id, callback) {
  Asset.findById(asset_id, (err, found_asset) => {
    if (err) {
      console.log(err);
    } else {
      // Delete Asset from DB
      Information.find({_id: info_id}).remove((err) => {
        if (err) {
          console.log(err);
        } else {
          // Remove from asset DB
          // info_id = 'ObjectId("' + info_id + '")'
          let new_arrray = found_asset.informations_linked.filter(function(value, index, arr){
              if (value.equals(info_id)) {
                return false
              } else {
                return true
              }
          });
          found_asset.informations_linked = new_arrray;
          found_asset.save((err, saved) => {
            callback(true)
          });
        }
      });
    }
  })
}

function verify_user_by_encrypted_token(encrypted_token, callback) {
  jwt.verify(encrypted_token, jwt_config.secret, (err, decoded) => {
    if (err) {
      logs.server('Error during verify_user_by_encrypted_token > jwt.verify = ' + err, 'error');
      callback(err, false);
    } else {
      User.findByUsername(decoded.username, (err, found_user) => {
        if (err) {
          console.log(err);
          logs.server('Error during verify_user_by_encrypted_token > user.findByUsername = ' + err, 'error');
          callback(err, false);
        } else if (!found_user) {
          callback('', false);
        } else {
          callback('', true, found_user);
        }
      });
    }
  });
}

function is_user_linked_to_project(user, project_id, callback) {
  Project.findById(project_id, (err, found_project) => {
    if (err) {
      logs.server('Error during is_user_linked_to_project > Project.findById = ' + err, 'error');
      callback(err, false);
    } else if (!found_project) {
      callback('', false);
    } else {
      if (found_project.linked_users.includes(user._id)) {
        if (user.RelatedProjects.includes(found_project._id)) {
          callback('', true);
        } else {
          callback('', false);
        }
      } else {
        callback('', false);
      }
    }
  })

}

function is_user_linked_to_asset(user, asset_id, callback) {
  Asset.findById(asset_id, (err, found_asset) => {
    if (err) {
      logs.server('Error during is_user_linked_to_asset > Asset.findById = ' + err, 'error');
      callback(err, false);
    } else if (!found_asset) {
      callback('', false);
    } else {
      is_user_linked_to_project(user, found_asset.project_id, (err, is_linked) => {
        if (err) {
          callback(err, false);
        } else if (!is_linked) {
          callback('', false);
        } else {
          callback('', true);
        }
      })
    }
  })
}

function is_user_linked_to_information(user, info_id, callback) {
  Information.findById(info_id, (err, found_info) => {
    if (err) {
      logs.server('Error during is_user_linked_to_information > Information.findById = ' + err, 'error');
      callback(err, false);
    } else if (!found_info) {
      callback('', false);
    } else {
      is_user_linked_to_asset(user, found_info.linked_asset, (err, is_linked) => {
        if (err) {
          console.log(err);
          callback(err, false);
        } else {
          callback('', is_linked);
        }
      })
    }
  })
}

module.exports = router;
